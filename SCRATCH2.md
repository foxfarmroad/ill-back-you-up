// Get the song here
divText = ((TextNode)node).text();
divText = StringUtils.remove(divText, endChar);
// Split the song from the number
String[] songs = divText.split("\\d+[\\.]");
// If a song is found
if (songs.length > 1) {
    hasSong = true;
    // Add the song
    currSong = StringUtils.replaceChars(
            songs[1], badChar, apos);
    currSong = StringUtils.strip(StringUtils.replaceChars(
            currSong, badTranslation, "'"));
    if (currSong.equalsIgnoreCase("holloween")) {
        currSong = "Halloween";
    }
    if (!(currSong.compareToIgnoreCase("Shake Me Like A Monkey") == 0 &&
            lastSong.compareToIgnoreCase("Shake Me Like A Monkey") == 0)) {
        setList.add(currSong);
        logger.info(currSong);
        lastSong = currSong;
    }
    // Reset break tracking
    breaks = 0;
}
else {
    // No Song
    if (!StringUtils.isBlank(divText)) {
        // Look for encore
        if (divText.toLowerCase().contains("encore")) {
            currSong = "Encore:";
            setList.add(currSong);
            logger.info(currSong);
            lastSong = currSong;
            breaks = 0;
        }
        else if (divText.toLowerCase().contains(
                "set break")) {
            currSong = "Set Break";
            setList.add(currSong);
            logger.info(currSong);
            lastSong = currSong;
            breaks = 0;
        }
        // We're in the show notes
        else {
            String nodeText = StringUtils.remove(divText,
                    endChar);
            // Create the notes
            if (!StringUtils.isBlank(nodeText)) {
                if (noteList.isEmpty()) {
                    noteList.add("Notes:");
                    logger.info("Notes:");
                }
                // If a img tag is found within the notes,
                // a -> is added so this node text should
                // be appended to that last note item in
                // the list
                if (hasSegue)
                    noteList.set(noteList.size()-1,
                            noteList.get(noteList.size()-1)
                                    .concat(StringUtils.strip(
                                            nodeText)));
                else {
                    // If a guest has been found via the
                    // font tag, the symbol is added to the
                    // notes list, so this text now needs to
                    // be appended to that symbol
                    if (hasGuest)
                        noteList.set(noteList.size()-1,
                                noteList.get(noteList.size()-1)
                                        .concat(StringUtils.strip(
                                                nodeText)));
                        // Everything else is just added as a
                        // new item in the list
                        // Sometimes there is a double break
                        // between notes, so reset breaks value
                    else {
                        noteList.add(StringUtils.strip(
                                nodeText));
                        logger.info(StringUtils.strip(
                                nodeText));
                        breaks = 0;
                    }
                }
                // Has guest gets reset for the next item
                // in the show notes
                hasGuest = false;
            }
        }
    }
}
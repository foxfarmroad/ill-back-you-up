<div style="font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15px;"> 
  <div style="padding-bottom:12px;padding-left:3px;color:#3995aa;">
   Jun 28 2014 
   <br />First Niagara Pavilion
   <br />Burgettstown, PA
  </div> 
  <div>
   <div style="Color:#000000;Position:Absolute;Top:470;">
    10.&nbsp;&nbsp;Warehouse&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:504;">
    12.&nbsp;&nbsp;Crush&nbsp;
   </div> 
   <div style="Position:Absolute;Top:633;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
    SHOW NOTES:
    <br />&nbsp;
    <img src="images/setlists/NewArrow.png" width="16" height="9" />&nbsp;indicates a segue into next song
   </div>
   <br />
   <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">
    Granny
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:589;">
    17.&nbsp;&nbsp;Digging a Ditch&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">
    Dreaming Tree
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:538;">
    14.&nbsp;&nbsp;If Only&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">
    Dreaming Tree
   </div> 
   <div style="Color:#3f94aa;Position:Absolute;Top:728;">
    <font style="font-size:19px;">&Auml;</font>&nbsp;Bela Fleck
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">
    Dreaming Tree
   </div> 
   <div style="Color:#3f94aa;Position:Absolute;Top:711;">
    <font style="font-size:19px;">~</font>&nbsp;Carter, Dave and Tim
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:426;">
    -------- SET BREAK -------- &nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">
    Dreaming Tree
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:606;">
    18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:375;">
    &nbsp;6.&nbsp;&nbsp;Sweet&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">
    Dreaming Tree
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:392;">
    &nbsp;7.&nbsp;&nbsp;Lie In Our Graves&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:521;">
    13.&nbsp;&nbsp;Seven&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">
    Granny
   </div> 
   <div style="Color:#3f94aa;Position:Absolute;Top:677;">
    <font style="font-size:19px;">*</font>&nbsp;Dave Solo
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">
    Dreaming Tree
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:487;">
    11.&nbsp;&nbsp;Belly Belly Nice&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">
    Dreaming Tree
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:572;">
    16.&nbsp;&nbsp;The Riff&nbsp;
    <img src="images/setlists/NewArrow.png" hspace="4" />
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:324;">
    &nbsp;3.&nbsp;&nbsp;What Would You Say&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">
    Dreaming Tree
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:358;">
    &nbsp;5.&nbsp;&nbsp;Christmas Song&nbsp;~
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:290;">
    &nbsp;1.&nbsp;&nbsp;Beach Ball&nbsp;*
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:307;">
    &nbsp;2.&nbsp;&nbsp;Minarets&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">
    Granny
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:453;">
    &nbsp;9.&nbsp;&nbsp;Save Me&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">
    Granny
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:555;">
    15.&nbsp;&nbsp;Jimi Thing&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">
    Granny
   </div> 
   <div style="Color:#3f94aa;Position:Absolute;Top:694;">
    <font style="font-size:19px;">+</font>&nbsp;Carter and Dave
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">
    Granny
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:409;">
    &nbsp;8.&nbsp;&nbsp;Two Step&nbsp;
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">
    Granny
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:341;">
    &nbsp;4.&nbsp;&nbsp;Little Red Bird&nbsp;+
   </div> 
   <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">
    Granny
   </div> 
  </div> 
  <br /> 
  <p></p>
  <br />&nbsp;
  <br /> 
 </div> 
 <div class="clear"></div>   
</div> 
package com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.journal;

import lombok.Data;

@Data
public class JournalBean {
    private RecordBean[] records;
}

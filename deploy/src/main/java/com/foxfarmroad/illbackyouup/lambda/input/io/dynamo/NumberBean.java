package com.foxfarmroad.illbackyouup.lambda.input.io.dynamo;

import lombok.Data;

@Data
public class NumberBean {
    private String n;
}

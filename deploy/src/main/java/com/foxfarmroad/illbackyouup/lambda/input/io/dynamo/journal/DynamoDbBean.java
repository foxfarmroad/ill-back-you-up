package com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.journal;

import lombok.Data;

@Data
public class DynamoDbBean {
    private Integer approximateCreationDateTime;
    private KeysBean keys;
    private NewImageBean newImage;
    private String sequenceNumber;
    private Integer sizeBytes;
    private String streamViewType;
}

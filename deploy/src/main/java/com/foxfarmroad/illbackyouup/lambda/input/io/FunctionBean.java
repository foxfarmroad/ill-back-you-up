package com.foxfarmroad.illbackyouup.lambda.input.io;

import lombok.Data;

@Data
public class FunctionBean {
    private FunctionType function;
}

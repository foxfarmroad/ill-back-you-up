package com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.journal;

import lombok.Data;

@Data
public class RecordBean {
    private String eventId;
    private String eventName;
    private String eventVersion;
    private String eventSource;
    private String awsRegion;
    private DynamoDbBean dynamodb;
    private String eventSourceArn;
}

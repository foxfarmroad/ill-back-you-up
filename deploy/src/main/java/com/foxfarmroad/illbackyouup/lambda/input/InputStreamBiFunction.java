package com.foxfarmroad.illbackyouup.lambda.input;

import com.google.gson.Gson;
import io.reactivex.functions.BiFunction;

import javax.inject.Inject;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public class InputStreamBiFunction implements BiFunction<InputStream, Class<? extends Object>, Object> {
    @Inject
    private Gson gson;
    @Inject
    @Charset
    private String charset;

    @Override
    public Object apply(InputStream inputStream, Class<? extends Object> type) throws Exception {
        Reader reader = readStream(inputStream);
        return gson.fromJson(reader, type);
    }

    private Reader readStream(InputStream inputStream) {
        try {
            return new InputStreamReader(inputStream, charset);
        } catch (UnsupportedEncodingException | NullPointerException e) {
            throw new RuntimeException(e);
        }
    }
}

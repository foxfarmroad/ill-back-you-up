package com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.journal;

import com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.StringBean;
import lombok.Data;

@Data
public class KeysBean {
    private StringBean correlationId;
    private StringBean uuid;
}

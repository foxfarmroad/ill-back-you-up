package com.foxfarmroad.illbackyouup.lambda.input.io.dynamo;

import lombok.Data;

@Data
public class StringBean {
    private String s;
}

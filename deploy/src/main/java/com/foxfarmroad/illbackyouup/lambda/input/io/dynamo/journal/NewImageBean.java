package com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.journal;

import com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.NumberBean;
import com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.StringBean;
import lombok.Data;

@Data
public class NewImageBean {
    private StringBean createdAt;
    private StringBean correlationId;
    private StringBean uuid;
    private NumberBean timestamp;
}

package com.foxfarmroad.illbackyouup.lambda.input;

import com.google.gson.Gson;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

public class InputModule extends AbstractModule {
    @Override
    protected void configure() {
        requireBinding(Gson.class);
        requireBinding(String.class);

        bindConstant().annotatedWith(Charset.class).to("UTF-8");
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }
}

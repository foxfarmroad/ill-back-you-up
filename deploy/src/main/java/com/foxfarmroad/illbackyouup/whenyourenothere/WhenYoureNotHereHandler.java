package com.foxfarmroad.illbackyouup.whenyourenothere;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.foxfarmroad.illbackyouup.lambda.input.io.FunctionBean;
import com.foxfarmroad.illbackyouup.lambda.input.io.FunctionType;
import com.foxfarmroad.illbackyouup.lambda.input.io.dynamo.journal.JournalBean;
import com.google.inject.Guice;
import com.google.inject.Module;
import hu.akarnokd.rxjava2.expr.StatementObservable;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import lombok.experimental.var;
import org.javatuples.Pair;

import javax.inject.Inject;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.concurrent.Callable;

/**
 * <p>
 *     Entry point for all functions types.
 *
 *     This container routes correctly based on the incoming data, including contents and sources.
 * </p>
 */
public class WhenYoureNotHereHandler implements RequestStreamHandler {
    @Inject
    private BiFunction<InputStream, Class<? extends Object>, Object> inputStreamBiFunction;
    @Inject
    private Callable<Observable<Pair<String, Long>>> whenCallable;

    public WhenYoureNotHereHandler() {
        this(new WhenYoureNotHereModule());
    }

    public WhenYoureNotHereHandler(Module module) {
        Guice.createInjector(module).injectMembers(this);
    }

    @Override
    public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException {
        // Input will be either
        // {"function": "SCHEDULE|CHECK"}
        // OR
        // { ... Dynamo Stream Event ...}

        BufferedInputStream bufferedInputStream = new BufferedInputStream(input);

        if (bufferedInputStream.markSupported()) {
            System.out.println("MARK SUPPORTED!");
            bufferedInputStream.mark(0);
        }
        /* Respond to CloudWatch Rule/Event */
        Observable<Object> functionBeanObservable = Observable.just(bufferedInputStream)
                .zipWith(Observable.just(FunctionBean.class), inputStreamBiFunction)
                .share();

        var map = new HashMap<FunctionType, Observable<Pair<String, Long>>>();
        map.put(FunctionType.SCHEDULE, Observable.fromCallable(whenCallable).blockingSingle());

        StatementObservable.switchCase(
                () -> ((FunctionBean) functionBeanObservable.blockingSingle()).getFunction(),
                map,
                Observable.empty()
        ).blockingSingle();

        bufferedInputStream.mark(0);
        bufferedInputStream.reset();
        /* Respond to DynamoDB Stream Event */
        Observable<Object> journalBeanObservable = Observable.just(bufferedInputStream)
                .zipWith(Observable.just(JournalBean.class), inputStreamBiFunction)
                .share();

//        var pair = StatementObservable.ifThen(
//                    () -> functionBean.getFunction() != null,
//                    Observable.defer(whenCallable),
//                    // TODO Handle SCHEDULE and CHECK
//                    Observable.fromCallable(yourStepsCallable)
//                            .blockingSingle()
//                            .flatMap(sometimesFunction)
//                            .map(epoch -> Pair.with(dateTimeFormatter.print(new DateTime()), epoch))
//                            .doOnNext(journalEntryConsumer),
//                    // TODO Handle Dynamo Stream Event
//                    // Set schedule for the CHECK event
//                    // Create snapshot record
//                    Observable.just(Pair.with("Dynamo", -1L))
//            )
//            .blockingSingle();
    }
}

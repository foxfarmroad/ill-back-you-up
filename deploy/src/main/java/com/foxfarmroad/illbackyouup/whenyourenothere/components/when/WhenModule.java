package com.foxfarmroad.illbackyouup.whenyourenothere.components.when;

import com.foxfarmroad.illbackyouup.components.sometimes.SometimesFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.SometimesModule;
import com.foxfarmroad.illbackyouup.components.yoursteps.Steps;
import com.foxfarmroad.illbackyouup.components.yoursteps.Your;
import com.foxfarmroad.illbackyouup.components.yoursteps.YourStepsCallable;
import com.foxfarmroad.illbackyouup.components.yoursteps.YourStepsModule;
import com.foxfarmroad.illbackyouup.dynamo.DynamoModule;
import com.foxfarmroad.illbackyouup.dynamo.components.journal.JournalEntryConsumer;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.foxfarmroad.illbackyouup.whenyourenothere.Correlation;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import org.javatuples.Pair;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.concurrent.Callable;

public class WhenModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new YourStepsModule());
        install(new SometimesModule());
        install(new DynamoModule());

        bindConstant().annotatedWith(Your.class).to("http://www.dmbalmanac.com");
        // TODO Make the year dynamic in the path
        bindConstant().annotatedWith(Steps.class).to("TourShow.aspx?where=2018");

        bind(new TypeLiteral<Callable<Observable<YourStepsBean>>>() { }).to(YourStepsCallable.class);
        bind(new TypeLiteral<Function<YourStepsBean, Observable<Long>>>() { }).to(SometimesFunction.class);
        bind(new TypeLiteral<Consumer<Pair<String, Long>>>() { }).to(JournalEntryConsumer.class);
    }

    @Provides
    @Singleton
    @Correlation
    DateTimeFormatter provideDateTimeFormatter() {
        return ISODateTimeFormat.yearMonthDay().withZoneUTC();
    }
}

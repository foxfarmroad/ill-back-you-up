package com.foxfarmroad.illbackyouup.whenyourenothere;

import com.foxfarmroad.illbackyouup.components.sometimes.SometimesFunction;
import com.foxfarmroad.illbackyouup.components.yoursteps.Steps;
import com.foxfarmroad.illbackyouup.components.yoursteps.Your;
import com.foxfarmroad.illbackyouup.components.yoursteps.YourStepsCallable;
import com.foxfarmroad.illbackyouup.dynamo.components.journal.JournalEntryConsumer;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.foxfarmroad.illbackyouup.lambda.input.InputModule;
import com.foxfarmroad.illbackyouup.lambda.input.InputStreamBiFunction;
import com.foxfarmroad.illbackyouup.whenyourenothere.components.when.WhenCallable;
import com.foxfarmroad.illbackyouup.whenyourenothere.components.when.WhenModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import org.javatuples.Pair;

import java.io.InputStream;
import java.util.concurrent.Callable;

public class WhenYoureNotHereModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new InputModule());
        install(new WhenModule());

        bindConstant().annotatedWith(Your.class).to("http://www.dmbalmanac.com");
        // TODO Make the year dynamic in the path
        bindConstant().annotatedWith(Steps.class).to("TourShow.aspx?where=2018");

        bind(new TypeLiteral<BiFunction<InputStream, Class<? extends Object>, Object>>() { }).to(InputStreamBiFunction.class);
        bind(new TypeLiteral<Callable<Observable<YourStepsBean>>>() { }).to(YourStepsCallable.class);
        bind(new TypeLiteral<Function<YourStepsBean, Observable<Long>>>() { }).to(SometimesFunction.class);
        bind(new TypeLiteral<Consumer<Pair<String, Long>>>() { }).to(JournalEntryConsumer.class);
        bind(new TypeLiteral<Callable<Observable<Pair<String, Long>>>>() { }).to(WhenCallable.class);
    }
}

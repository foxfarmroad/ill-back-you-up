package com.foxfarmroad.illbackyouup.whenyourenothere.components.when;

import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.foxfarmroad.illbackyouup.whenyourenothere.Correlation;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import org.javatuples.Pair;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import javax.inject.Inject;
import java.util.concurrent.Callable;

public class WhenCallable implements Callable<Observable<Pair<String, Long>>> {
    @Inject
    private Callable<Observable<YourStepsBean>> yourStepsCallable;
    @Inject
    private Function<YourStepsBean, Observable<Long>> sometimesFunction;
    @Inject
    private Consumer<Pair<String, Long>> journalEntryConsumer;
    @Inject
    @Correlation
    private DateTimeFormatter dateTimeFormatter;

    @Override
    public Observable<Pair<String, Long>> call() throws Exception {
        return Observable.fromCallable(yourStepsCallable)
                .blockingSingle()
                .flatMap(sometimesFunction)
                .map(epoch -> Pair.with(dateTimeFormatter.print(new DateTime()), epoch))
                .doOnNext(journalEntryConsumer);
    }
}
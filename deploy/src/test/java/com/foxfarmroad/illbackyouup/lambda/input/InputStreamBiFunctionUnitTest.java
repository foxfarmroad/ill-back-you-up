package com.foxfarmroad.illbackyouup.lambda.input;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.lambda.input.io.FunctionBean;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import lombok.Data;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

@Category(UnitTest.class)
public class InputStreamBiFunctionUnitTest {
    @Inject
    private InputStreamBiFunction inputStreamBiFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new InputModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("{\"testKey\":\"testValue\"}".getBytes());

        TestObserver<Object> testObserver = Observable.zip(
                    Observable.just(inputStream),
                    Observable.just(TestBean.class),
                    inputStreamBiFunction
            )
            .test();

        testObserver.assertValueCount(1);
        testObserver.assertNoErrors();
        testObserver.assertComplete();

        Assert.assertEquals("testValue", ((TestBean) testObserver.values().get(0)).getTestKey());
    }

    @Test
    public void verifyRight2() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("{\"function\":\"SCHEDULE\"}".getBytes());

        TestObserver<Object> testObserver = Observable.zip(
                Observable.just(inputStream),
                Observable.just(FunctionBean.class),
                inputStreamBiFunction
        )
                .test();

        testObserver.assertValueCount(1);
        testObserver.assertNoErrors();
        testObserver.assertComplete();

        Assert.assertEquals("SCHEDULE", ((FunctionBean) testObserver.values().get(0)).getFunction().name());
    }

    @Test
    public void verifyB() throws Exception {
        InputStream inputStream = new ByteArrayInputStream("{\"test\":\"test\"}".getBytes());

        TestObserver<Object> testObserver = Observable.zip(
                    Observable.just(inputStream),
                    Observable.just(TestBean.class),
                    inputStreamBiFunction
            )
            .test();

        testObserver.assertValueCount(1);
        testObserver.assertNoErrors();
        testObserver.assertComplete();

        Assert.assertNull(((TestBean) testObserver.values().get(0)).getTestKey());
    }

    @Data
    private class TestBean {
        private String testKey;
    }
}

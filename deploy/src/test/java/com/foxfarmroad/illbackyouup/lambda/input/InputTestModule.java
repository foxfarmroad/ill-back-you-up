package com.foxfarmroad.illbackyouup.lambda.input;

import com.foxfarmroad.illbackyouup.components.sometimes.Sometimes;
import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.times.TimeOffset;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class InputTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(TimeStart.class).to(0);
        bindConstant().annotatedWith(TimeOffset.class).to(6);
    }

    @Provides
    @Sometimes
    DateTime provideNow() {
        return new DateTime()
                .withZone(DateTimeZone.UTC)
                .withMonthOfYear(5)
                .withDayOfMonth(18)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(1);
    }

}

package com.foxfarmroad.illbackyouup.whenyourenothere;

import com.amazonaws.services.lambda.runtime.Context;
import com.foxfarmroad.ManualTest;
import com.google.inject.util.Modules;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;

@Category(ManualTest.class)
public class WhenYoureNotHereManualTest {
    private WhenYoureNotHereHandler whenYoureNotHereHandler;

    @Mock
    private Context context;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        whenYoureNotHereHandler = new WhenYoureNotHereHandler(Modules.override(new WhenYoureNotHereModule()).with(new WhenYoureNotHereTestModule()));
    }

    @Test
    public void verifyRight() throws Exception {
        whenYoureNotHereHandler.handleRequest(new ByteArrayInputStream("{\"test\":\"test\"}".getBytes()), null, context);
    }
}

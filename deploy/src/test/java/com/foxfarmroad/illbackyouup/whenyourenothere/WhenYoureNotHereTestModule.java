package com.foxfarmroad.illbackyouup.whenyourenothere;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.foxfarmroad.illbackyouup.components.sometimes.Sometimes;
import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.times.TimeOffset;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.mockito.Mockito;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class WhenYoureNotHereTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(TimeStart.class).to(0);
        bindConstant().annotatedWith(TimeOffset.class).to(6);
    }

    @Provides
    @Sometimes
    DateTime provideNow() {
        return new DateTime()
                .withZone(DateTimeZone.UTC)
                .withMonthOfYear(5)
                .withDayOfMonth(18)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(1);
    }

    @Provides
    @Singleton
    DynamoDB provideDynamo() {
        DynamoDB dynamoDB = Mockito.mock(DynamoDB.class);
        Table table = Mockito.mock(Table.class);
        when(table.putItem(any(Item.class))).thenReturn(Mockito.mock(PutItemOutcome.class));
        when(dynamoDB.getTable(any())).thenReturn(table);
        return dynamoDB;
    }
}

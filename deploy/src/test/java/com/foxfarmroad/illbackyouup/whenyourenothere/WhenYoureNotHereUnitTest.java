package com.foxfarmroad.illbackyouup.whenyourenothere;

import com.amazonaws.services.lambda.runtime.Context;
import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.foxfarmroad.illbackyouup.lambda.input.io.FunctionBean;
import com.foxfarmroad.illbackyouup.lambda.input.io.FunctionType;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import lombok.Data;
import org.javatuples.Pair;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class WhenYoureNotHereUnitTest {
    private WhenYoureNotHereHandler whenYoureNotHereHandler;
    @Mock
    private Context context;
    @Mock
    @Bind
    private BiFunction<InputStream, Class<? extends Object>, Object> inputStreamBiFunction;
    @Mock
    @Bind
    private Callable<Observable<YourStepsBean>> yourStepsCallable;
    @Mock
    @Bind
    private Function<YourStepsBean, Observable<Long>> sometimesFunction;
    @Mock
    @Bind
    private Consumer<Pair<String, Long>> journalEntryFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        whenYoureNotHereHandler = new WhenYoureNotHereHandler(Modules.override(new WhenYoureNotHereModule()).with(new WhenYoureNotHereTestModule(), BoundFieldModule.of(this)));
    }

    @Test
    public void verifyRight1() throws Exception {
        FunctionBean functionBean = new FunctionBean();
        functionBean.setFunction(FunctionType.SCHEDULE);
        YourStepsBean yourStepsBean = new YourStepsBean(new Document(""));

        when(inputStreamBiFunction.apply(any(), any())).thenReturn(functionBean);
        when(yourStepsCallable.call()).thenReturn(Observable.just(yourStepsBean));
        when(sometimesFunction.apply(any())).thenReturn(Observable.just(-1L));
        doNothing().when(journalEntryFunction).accept(any());

        InputStream inputStream = new ByteArrayInputStream("".getBytes());
        OutputStream outputStream = new ByteArrayOutputStream(0);

        whenYoureNotHereHandler.handleRequest(inputStream, outputStream, context);

        verify(inputStreamBiFunction, times(1)).apply(any(), any());
        verify(yourStepsCallable, times(1)).call();
        verify(sometimesFunction, times(1)).apply(any());
        verify(journalEntryFunction, times(1)).accept(any());
        verifyNoMoreInteractions(inputStreamBiFunction, yourStepsCallable, sometimesFunction, journalEntryFunction);
    }

    @Test
    public void verifyRight2() throws Exception {
        FunctionBean functionBean = new FunctionBean();
        YourStepsBean yourStepsBean = new YourStepsBean(new Document(""));

        when(inputStreamBiFunction.apply(any(), any())).thenReturn(functionBean);
        when(yourStepsCallable.call()).thenReturn(Observable.just(yourStepsBean));
        when(sometimesFunction.apply(any())).thenReturn(Observable.just(-1L));
        doNothing().when(journalEntryFunction).accept(any());

        InputStream inputStream = new ByteArrayInputStream("".getBytes());
        OutputStream outputStream = new ByteArrayOutputStream(0);

        whenYoureNotHereHandler.handleRequest(inputStream, outputStream, context);

        verify(inputStreamBiFunction, times(1)).apply(any(), any());
        verifyZeroInteractions(sometimesFunction, journalEntryFunction);
        verify(yourStepsCallable, times(1)).call();
        verifyNoMoreInteractions(inputStreamBiFunction, yourStepsCallable);
    }

    @Data
    private class TestBean {
        private String testKey;
    }
}

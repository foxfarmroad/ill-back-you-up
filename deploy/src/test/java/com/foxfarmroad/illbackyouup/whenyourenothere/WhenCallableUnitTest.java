package com.foxfarmroad.illbackyouup.whenyourenothere;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.foxfarmroad.illbackyouup.whenyourenothere.components.when.WhenCallable;
import com.foxfarmroad.illbackyouup.whenyourenothere.components.when.WhenModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.javatuples.Pair;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.concurrent.Callable;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class WhenCallableUnitTest {
    @Mock
    @Bind
    private Callable<Observable<YourStepsBean>> yourStepsCallable;
    @Mock
    @Bind
    private Function<YourStepsBean, Observable<Long>> sometimesFunction;
    @Mock
    @Bind
    private Consumer<Pair<String, Long>> journalEntryConsumer;
    @Inject
    @Correlation
    private DateTimeFormatter dateTimeFormatter;
    @Inject
    private WhenCallable whenCallable;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new WhenModule()).with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        YourStepsBean yourStepsBean = new YourStepsBean(new Document(""));

        when(yourStepsCallable.call()).thenReturn(Observable.just(yourStepsBean));
        when(sometimesFunction.apply(any())).thenReturn(Observable.just(-1L));
        doNothing().when(journalEntryConsumer).accept(any());

        TestObserver<Pair<String, Long>> testObserver = Observable.fromCallable(whenCallable)
                .blockingSingle()
                .test();

        testObserver.assertSubscribed()
                .assertValueCount(1)
                .assertNoErrors()
                .assertComplete();

        verify(yourStepsCallable, times(1)).call();
        verify(sometimesFunction, times(1)).apply(any());
        verify(journalEntryConsumer, times(1)).accept(any());
        verifyNoMoreInteractions(yourStepsCallable, sometimesFunction, journalEntryConsumer);
    }
}
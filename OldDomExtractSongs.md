Document
->
First node with SETLIST_STYLE:

<div style="font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15px;">
   <div style="padding-bottom:12px;padding-left:3px;color:#3995aa;">
      Jun 28 2014
      <br />
      First Niagara Pavilion
      <br />
      Burgettstown, PA
   </div>
   <div>
      <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
      <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
      <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#3f94aa;Position:Absolute;Top:806;">
         <font style="font-size:19px;">*</font>
         &amp;nbsp;Dave Solo
      </div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#3f94aa;Position:Absolute;Top:840;">
         <font style="font-size:19px;">~</font>
         &amp;nbsp;Carter, Dave and Tim
      </div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
      <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
         SHOW NOTES:
         <br />
         &amp;nbsp;
         <img src="images/setlists/NewArrow.png" width="16" height="9" />
         &amp;nbsp;indicates a segue into next song
      </div>
      <br />
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
      <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#3f94aa;Position:Absolute;Top:823;">
         <font style="font-size:19px;">+</font>
         &amp;nbsp;Carter and Dave
      </div>
      <div style="Color:#000000;Position:Absolute;Top:606;">
         18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
         <img src="images/setlists/NewArrow.png" hspace="4" />
      </div>
      <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#3f94aa;Position:Absolute;Top:857;">
         <font style="font-size:19px;">&amp;Auml;</font>
         &amp;nbsp;Bela Fleck
      </div>
      <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:572;">
         16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
         <img src="images/setlists/NewArrow.png" hspace="4" />
      </div>
      <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
   </div>
   <br />
   <p />
   <br />
   &amp;nbsp;
   <br />
</div>

->
All child nodes:

<div style="padding-bottom:12px;padding-left:3px;color:#3995aa;">
  Jun 28 2014
  <br />
  First Niagara Pavilion
  <br />
  Burgettstown, PA
</div>
<div>
  <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
  <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:806;">
     <font style="font-size:19px;">*</font>
     &amp;nbsp;Dave Solo
  </div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:840;">
     <font style="font-size:19px;">~</font>
     &amp;nbsp;Carter, Dave and Tim
  </div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
  <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
     SHOW NOTES:
     <br />
     &amp;nbsp;
     <img src="images/setlists/NewArrow.png" width="16" height="9" />
     &amp;nbsp;indicates a segue into next song
  </div>
  <br />
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
  <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:823;">
     <font style="font-size:19px;">+</font>
     &amp;nbsp;Carter and Dave
  </div>
  <div style="Color:#000000;Position:Absolute;Top:606;">
     18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:857;">
     <font style="font-size:19px;">&amp;Auml;</font>
     &amp;nbsp;Bela Fleck
  </div>
  <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:572;">
     16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
</div>
<br />
<p />
<br />
&amp;nbsp;
<br />

->
All div nodes:

<div style="padding-bottom:12px;padding-left:3px;color:#3995aa;">
  Jun 28 2014
  <br />
  First Niagara Pavilion
  <br />
  Burgettstown, PA
</div>
<div>
  <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
  <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:806;">
     <font style="font-size:19px;">*</font>
     &amp;nbsp;Dave Solo
  </div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:840;">
     <font style="font-size:19px;">~</font>
     &amp;nbsp;Carter, Dave and Tim
  </div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
  <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
     SHOW NOTES:
     <br />
     &amp;nbsp;
     <img src="images/setlists/NewArrow.png" width="16" height="9" />
     &amp;nbsp;indicates a segue into next song
  </div>
  <br />
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
  <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:823;">
     <font style="font-size:19px;">+</font>
     &amp;nbsp;Carter and Dave
  </div>
  <div style="Color:#000000;Position:Absolute;Top:606;">
     18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:857;">
     <font style="font-size:19px;">&amp;Auml;</font>
     &amp;nbsp;Bela Fleck
  </div>
  <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:572;">
     16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
</div>

->
without LOC_STYLE:

<div>
  <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
  <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:806;">
     <font style="font-size:19px;">*</font>
     &amp;nbsp;Dave Solo
  </div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:840;">
     <font style="font-size:19px;">~</font>
     &amp;nbsp;Carter, Dave and Tim
  </div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
  <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
     SHOW NOTES:
     <br />
     &amp;nbsp;
     <img src="images/setlists/NewArrow.png" width="16" height="9" />
     &amp;nbsp;indicates a segue into next song
  </div>
  <br />
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
  <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:823;">
     <font style="font-size:19px;">+</font>
     &amp;nbsp;Carter and Dave
  </div>
  <div style="Color:#000000;Position:Absolute;Top:606;">
     18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:857;">
     <font style="font-size:19px;">&amp;Auml;</font>
     &amp;nbsp;Bela Fleck
  </div>
  <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:572;">
     16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
</div>

->
All child div nodes:

<div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
<div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#3f94aa;Position:Absolute;Top:806;">
 <font style="font-size:19px;">*</font>
 &amp;nbsp;Dave Solo
</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#3f94aa;Position:Absolute;Top:840;">
 <font style="font-size:19px;">~</font>
 &amp;nbsp;Carter, Dave and Tim
</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
<div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
 SHOW NOTES:
 <br />
 &amp;nbsp;
 <img src="images/setlists/NewArrow.png" width="16" height="9" />
 &amp;nbsp;indicates a segue into next song
</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
<div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#3f94aa;Position:Absolute;Top:823;">
 <font style="font-size:19px;">+</font>
 &amp;nbsp;Carter and Dave
</div>
<div style="Color:#000000;Position:Absolute;Top:606;">
 18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
 <img src="images/setlists/NewArrow.png" hspace="4" />
</div>
<div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#3f94aa;Position:Absolute;Top:857;">
 <font style="font-size:19px;">&amp;Auml;</font>
 &amp;nbsp;Bela Fleck
</div>
<div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:572;">
 16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
 <img src="images/setlists/NewArrow.png" hspace="4" />
</div>
<div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>

->
Get location from style, only for those with Top: in style:

470
426
6014
674
...
324
6014

->
All nodes starting with SONG_STYLE:

<div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
<div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#3f94aa;Position:Absolute;Top:806;">
 <font style="font-size:19px;">*</font>
 &amp;nbsp;Dave Solo
</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
<div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:606;">
 18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
 <img src="images/setlists/NewArrow.png" hspace="4" />
</div>
<div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:572;">
 16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
 <img src="images/setlists/NewArrow.png" hspace="4" />
</div>
<div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>

->
Get all div 

<div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
<div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#3f94aa;Position:Absolute;Top:806;">
 <font style="font-size:19px;">*</font>
 &amp;nbsp;Dave Solo
</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
<div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
<div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:606;">
 18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
 <img src="images/setlists/NewArrow.png" hspace="4" />
</div>
<div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
<div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
<div style="Color:#000000;Position:Absolute;Top:572;">
 16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
 <img src="images/setlists/NewArrow.png" hspace="4" />
</div>
<div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
<div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>

->
Div own text:

10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;
-------- SET BREAK -------- &amp;nbsp;
So Much To Say
22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;
&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~
19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;
Spoon
-------- ENCORE -------- &amp;nbsp;
So Much To Say
20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;
So Much To Say

Spoon
So Much To Say
&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;
So Much To Say
&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*
15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;
Spoon
24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;
Spoon
17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;
So Much To Say
12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;
11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;
So Much To Say
14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;
So Much To Say
&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;
So Much To Say
&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;
Spoon
23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;
&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;
Spoon
13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;
Spoon

&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;
Spoon
21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;
Spoon
&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+
Spoon

&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;
So Much To Say

->
Remove nbsp;

10.&amp;&amp;Warehouse&amp;
-------- SET BREAK -------- &amp;
So Much To Say
22.&amp;&amp;Grey Street&amp;&amp;Auml;
&amp;5.&amp;&amp;Christmas Song&amp;~
19.&amp;&amp;Typical Situation&amp;&amp;Auml;
Spoon
-------- ENCORE -------- &amp;
So Much To Say
20.&amp;&amp;Drunken Soldier&amp;&amp;Auml;
So Much To Say

Spoon
So Much To Say
&amp;7.&amp;&amp;Lie In Our Graves&amp;
So Much To Say
&amp;1.&amp;&amp;Beach Ball&amp;*
15.&amp;&amp;Jimi Thing&amp;
Spoon
24.&amp;&amp;Ants Marching&amp;
Spoon
17.&amp;&amp;Digging a Ditch&amp;
So Much To Say
12.&amp;&amp;Crush&amp;
11.&amp;&amp;Belly Belly Nice&amp;
So Much To Say
14.&amp;&amp;If Only&amp;
So Much To Say
&amp;6.&amp;&amp;Sweet&amp;
So Much To Say
&amp;2.&amp;&amp;Minarets&amp;
Spoon
23.&amp;&amp;Granny&amp;
&amp;9.&amp;&amp;Save Me&amp;
Spoon
13.&amp;&amp;Seven&amp;
Spoon

&amp;8.&amp;&amp;Two Step&amp;
Spoon
21.&amp;&amp;Corn Bread&amp;&amp;Auml;
Spoon
&amp;4.&amp;&amp;Little Red Bird&amp;+
Spoon

&amp;3.&amp;&amp;What Would You Say&amp;
So Much To Say

->
Split only numbered songs

10. | &amp;&amp;Warehouse&amp;
22. | &amp;&amp;Grey Street&amp;&amp;Auml;
&amp;5. | &amp;&amp;Christmas Song&amp;~
19. | &amp;&amp;Typical Situation&amp;&amp;Auml;
20. | &amp;&amp;Drunken Soldier&amp;&amp;Auml;
&amp;7. | &amp;&amp;Lie In Our Graves&amp;
&amp;1. | &amp;&amp;Beach Ball&amp;*
15. | &amp;&amp;Jimi Thing&amp;
24. | &amp;&amp;Ants Marching&amp;
17. | &amp;&amp;Digging a Ditch&amp;
12. | &amp;&amp;Crush&amp;
11. | &amp;&amp;Belly Belly Nice&amp;
14. | &amp;&amp;If Only&amp;
&amp;6. | &amp;&amp;Sweet&amp;
&amp;2. | &amp;&amp;Minarets&amp;
23. | &amp;&amp;Granny&amp;
&amp;9. | &amp;&amp;Save Me&amp;
13. | &amp;&amp;Seven&amp;
&amp;8. | &amp;&amp;Two Step&amp;
21. | &amp;&amp;Corn Bread&amp;&amp;Auml;
&amp;4. | &amp;&amp;Little Red Bird&amp;+
&amp;3. | &amp;&amp;What Would You Say&amp;

->
Get only song titles

&amp;&amp;Warehouse&amp;
&amp;&amp;Grey Street&amp;&amp;Auml;
&amp;&amp;Christmas Song&amp;~
&amp;&amp;Typical Situation&amp;&amp;Auml;
&amp;&amp;Drunken Soldier&amp;&amp;Auml;
&amp;&amp;Lie In Our Graves&amp;
&amp;&amp;Beach Ball&amp;*
&amp;&amp;Jimi Thing&amp;
&amp;&amp;Ants Marching&amp;
&amp;&amp;Digging a Ditch&amp;
&amp;&amp;Crush&amp;
&amp;&amp;Belly Belly Nice&amp;
&amp;&amp;If Only&amp;
&amp;&amp;Sweet&amp;
&amp;&amp;Minarets&amp;
&amp;&amp;Granny&amp;
&amp;&amp;Save Me&amp;
&amp;&amp;Seven&amp;
&amp;&amp;Two Step&amp;
&amp;&amp;Corn Bread&amp;&amp;Auml;
&amp;&amp;Little Red Bird&amp;+
&amp;&amp;What Would You Say&amp;

->
Add them to tree map with the appropriate location from above

470 | &amp;&amp;Warehouse&amp;
674 | &amp;&amp;Grey Street&amp;&amp;Auml;
358 | &amp;&amp;Christmas Song&amp;~
623 | &amp;&amp;Typical Situation&amp;&amp;Auml;
640 | &amp;&amp;Drunken Soldier&amp;&amp;Auml;
392 | &amp;&amp;Lie In Our Graves&amp;
290 | &amp;&amp;Beach Ball&amp;*
555 | &amp;&amp;Jimi Thing&amp;
735 | &amp;&amp;Ants Marching&amp;
589 | &amp;&amp;Digging a Ditch&amp;
504 | &amp;&amp;Crush&amp;
487 | &amp;&amp;Belly Belly Nice&amp;
538 | &amp;&amp;If Only&amp;
375 | &amp;&amp;Sweet&amp;
307 | &amp;&amp;Minarets&amp;
718 | &amp;&amp;Granny&amp;
453 | &amp;&amp;Save Me&amp;
521 | &amp;&amp;Seven&amp;
409 | &amp;&amp;Two Step&amp;
657 | &amp;&amp;Corn Bread&amp;&amp;Auml;
341 | &amp;&amp;Little Red Bird&amp;+
324 | &amp;&amp;What Would You Say&amp;

Which is sorted

290 | &amp;&amp;Beach Ball&amp;*
307 | &amp;&amp;Minarets&amp;
324 | &amp;&amp;What Would You Say&amp;
341 | &amp;&amp;Little Red Bird&amp;+
358 | &amp;&amp;Christmas Song&amp;~
375 | &amp;&amp;Sweet&amp;
392 | &amp;&amp;Lie In Our Graves&amp;
409 | &amp;&amp;Two Step&amp;
453 | &amp;&amp;Save Me&amp;
470 | &amp;&amp;Warehouse&amp;
487 | &amp;&amp;Belly Belly Nice&amp;
504 | &amp;&amp;Crush&amp;
521 | &amp;&amp;Seven&amp;
538 | &amp;&amp;If Only&amp;
555 | &amp;&amp;Jimi Thing&amp;
589 | &amp;&amp;Digging a Ditch&amp;
623 | &amp;&amp;Typical Situation&amp;&amp;Auml;
640 | &amp;&amp;Drunken Soldier&amp;&amp;Auml;
657 | &amp;&amp;Corn Bread&amp;&amp;Auml;
674 | &amp;&amp;Grey Street&amp;&amp;Auml;
718 | &amp;&amp;Granny&amp;
735 | &amp;&amp;Ants Marching&amp;

->
Do the same for the other bits (non-songs)

290 | &amp;&amp;Beach Ball&amp;*
307 | &amp;&amp;Minarets&amp;
324 | &amp;&amp;What Would You Say&amp;
341 | &amp;&amp;Little Red Bird&amp;+
358 | &amp;&amp;Christmas Song&amp;~
375 | &amp;&amp;Sweet&amp;
392 | &amp;&amp;Lie In Our Graves&amp;
409 | &amp;&amp;Two Step&amp;
426 | -------- SET BREAK -------- &amp;
453 | &amp;&amp;Save Me&amp;
470 | &amp;&amp;Warehouse&amp;
487 | &amp;&amp;Belly Belly Nice&amp;
504 | &amp;&amp;Crush&amp;
521 | &amp;&amp;Seven&amp;
538 | &amp;&amp;If Only&amp;
555 | &amp;&amp;Jimi Thing&amp;
589 | &amp;&amp;Digging a Ditch&amp;
623 | &amp;&amp;Typical Situation&amp;&amp;Auml;
640 | &amp;&amp;Drunken Soldier&amp;&amp;Auml;
657 | &amp;&amp;Corn Bread&amp;&amp;Auml;
674 | &amp;&amp;Grey Street&amp;&amp;Auml;
691 | -------- ENCORE -------- &amp;
718 | &amp;&amp;Granny&amp;
735 | &amp;&amp;Ants Marching&amp;
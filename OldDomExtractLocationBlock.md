Document
->
First node with SETLIST_STYLE:

<div style="font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15px;">
   <div style="padding-bottom:12px;padding-left:3px;color:#3995aa;">
      Jun 28 2014
      <br />
      First Niagara Pavilion
      <br />
      Burgettstown, PA
   </div>
   <div>
      <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
      <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
      <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#3f94aa;Position:Absolute;Top:806;">
         <font style="font-size:19px;">*</font>
         &amp;nbsp;Dave Solo
      </div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#3f94aa;Position:Absolute;Top:840;">
         <font style="font-size:19px;">~</font>
         &amp;nbsp;Carter, Dave and Tim
      </div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
      <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
         SHOW NOTES:
         <br />
         &amp;nbsp;
         <img src="images/setlists/NewArrow.png" width="16" height="9" />
         &amp;nbsp;indicates a segue into next song
      </div>
      <br />
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
      <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
      <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#3f94aa;Position:Absolute;Top:823;">
         <font style="font-size:19px;">+</font>
         &amp;nbsp;Carter and Dave
      </div>
      <div style="Color:#000000;Position:Absolute;Top:606;">
         18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
         <img src="images/setlists/NewArrow.png" hspace="4" />
      </div>
      <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#3f94aa;Position:Absolute;Top:857;">
         <font style="font-size:19px;">&amp;Auml;</font>
         &amp;nbsp;Bela Fleck
      </div>
      <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
      <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
      <div style="Color:#000000;Position:Absolute;Top:572;">
         16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
         <img src="images/setlists/NewArrow.png" hspace="4" />
      </div>
      <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
      <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
   </div>
   <br />
   <p />
   <br />
   &amp;nbsp;
   <br />
</div>

->
All child nodes:

<div style="padding-bottom:12px;padding-left:3px;color:#3995aa;">
  Jun 28 2014
  <br />
  First Niagara Pavilion
  <br />
  Burgettstown, PA
</div>
<div>
  <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
  <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:806;">
     <font style="font-size:19px;">*</font>
     &amp;nbsp;Dave Solo
  </div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:840;">
     <font style="font-size:19px;">~</font>
     &amp;nbsp;Carter, Dave and Tim
  </div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
  <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
     SHOW NOTES:
     <br />
     &amp;nbsp;
     <img src="images/setlists/NewArrow.png" width="16" height="9" />
     &amp;nbsp;indicates a segue into next song
  </div>
  <br />
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
  <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:823;">
     <font style="font-size:19px;">+</font>
     &amp;nbsp;Carter and Dave
  </div>
  <div style="Color:#000000;Position:Absolute;Top:606;">
     18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:857;">
     <font style="font-size:19px;">&amp;Auml;</font>
     &amp;nbsp;Bela Fleck
  </div>
  <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:572;">
     16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
</div>
<br />
<p />
<br />
&amp;nbsp;
<br />

->
All div nodes:

<div style="padding-bottom:12px;padding-left:3px;color:#3995aa;">
  Jun 28 2014
  <br />
  First Niagara Pavilion
  <br />
  Burgettstown, PA
</div>
<div>
  <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
  <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:806;">
     <font style="font-size:19px;">*</font>
     &amp;nbsp;Dave Solo
  </div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:840;">
     <font style="font-size:19px;">~</font>
     &amp;nbsp;Carter, Dave and Tim
  </div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
  <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
     SHOW NOTES:
     <br />
     &amp;nbsp;
     <img src="images/setlists/NewArrow.png" width="16" height="9" />
     &amp;nbsp;indicates a segue into next song
  </div>
  <br />
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
  <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:823;">
     <font style="font-size:19px;">+</font>
     &amp;nbsp;Carter and Dave
  </div>
  <div style="Color:#000000;Position:Absolute;Top:606;">
     18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:857;">
     <font style="font-size:19px;">&amp;Auml;</font>
     &amp;nbsp;Bela Fleck
  </div>
  <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:572;">
     16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
</div>

->
with LOC_STYLE:

<div style="padding-bottom:12px;padding-left:3px;color:#3995aa;">
  Jun 28 2014
  <br />
  First Niagara Pavilion
  <br />
  Burgettstown, PA
</div>

->
All child nodes:

Jun 28 2014
<br />
First Niagara Pavilion
<br />
Burgettstown, PA

->
Not Comment nodes:

Jun 28 2014
<br />
First Niagara Pavilion
<br />
Burgettstown, PA

->
Text nodes:

Jun 28 2014
First Niagara Pavilion
Burgettstown, PA

->
Add as the location block

























  <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
  <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:806;">
     <font style="font-size:19px;">*</font>
     &amp;nbsp;Dave Solo
  </div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:840;">
     <font style="font-size:19px;">~</font>
     &amp;nbsp;Carter, Dave and Tim
  </div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
  <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
     SHOW NOTES:
     <br />
     &amp;nbsp;
     <img src="images/setlists/NewArrow.png" width="16" height="9" />
     &amp;nbsp;indicates a segue into next song
  </div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
  <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:823;">
     <font style="font-size:19px;">+</font>
     &amp;nbsp;Carter and Dave
  </div>
  <div style="Color:#000000;Position:Absolute;Top:606;">
     18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#3f94aa;Position:Absolute;Top:857;">
     <font style="font-size:19px;">&amp;Auml;</font>
     &amp;nbsp;Bela Fleck
  </div>
  <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:572;">
     16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  
  
  <div style="Color:#000000;Position:Absolute;Top:470;">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:426;">-------- SET BREAK -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:674;">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:358;">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>
  <div style="Color:#000000;Position:Absolute;Top:623;">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:691;">-------- ENCORE -------- &amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:640;">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:392;">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:290;">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>
  <div style="Color:#000000;Position:Absolute;Top:555;">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:735;">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:589;">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:504;">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:487;">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:538;">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:375;">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  <div style="Color:#000000;Position:Absolute;Top:307;">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:718;">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:453;">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:521;">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:606;">
     18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:409;">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:657;">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:341;">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>
  <div style="Color:#000000;Position:Absolute;Top:5863;font-size:1px;">Spoon</div>
  <div style="Color:#000000;Position:Absolute;Top:572;">
     16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;
     <img src="images/setlists/NewArrow.png" hspace="4" />
  </div>
  <div style="Color:#000000;Position:Absolute;Top:324;">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>
  <div style="Color:#000000;Position:Absolute;Top:6014;font-size:1px;">So Much To Say</div>
  
  
  
  <div style="Color:#3f94aa;Position:Absolute;Top:806;">
     <font style="font-size:19px;">*</font>
     &amp;nbsp;Dave Solo
  </div>
  <div style="Color:#3f94aa;Position:Absolute;Top:840;">
     <font style="font-size:19px;">~</font>
     &amp;nbsp;Carter, Dave and Tim
  </div>
  <div style="Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;">
     SHOW NOTES:
     <br />
     &amp;nbsp;
     <img src="images/setlists/NewArrow.png" width="16" height="9" />
     &amp;nbsp;indicates a segue into next song
  </div>
  <div style="Color:#3f94aa;Position:Absolute;Top:823;">
     <font style="font-size:19px;">+</font>
     &amp;nbsp;Carter and Dave
  </div>
  <div style="Color:#3f94aa;Position:Absolute;Top:857;">
     <font style="font-size:19px;">&amp;Auml;</font>
     &amp;nbsp;Bela Fleck
  </div>
package com.foxfarmroad;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import software.amazon.awssdk.services.ssm.SSMClient;

import static org.mockito.Mockito.mock;

public class ComponentConfigOverridesModule extends AbstractModule {

    @Override
    public void configure() {
    }

    @Provides
    SSMClient provideAWSSimpleSystemsManagement() {
        return mock(SSMClient.class);
    }

}

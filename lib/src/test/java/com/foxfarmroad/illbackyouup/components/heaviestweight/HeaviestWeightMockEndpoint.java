package com.foxfarmroad.illbackyouup.components.heaviestweight;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Url;
import retrofit2.mock.BehaviorDelegate;

public class HeaviestWeightMockEndpoint implements HeaviestWeightEndpoint {

    private final BehaviorDelegate<HeaviestWeightEndpoint> delegate;
    private final Response<ResponseBody> responseBodyResponse;

    public HeaviestWeightMockEndpoint(BehaviorDelegate<HeaviestWeightEndpoint> service,
                                      Response<ResponseBody> responseBodyResponse) {
        this.delegate = service;
        this.responseBodyResponse = responseBodyResponse;
    }

    @Override
    public Observable<Response<ResponseBody>> login(@Url String url, @Field("Username") String username,
                                                    @Field("Password") String password,
                                                    @Field("form_action") String formAction) {
        return delegate.returningResponse(responseBodyResponse).login(url, username, password, formAction);
    }

}

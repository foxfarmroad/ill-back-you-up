package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

@Category(ComponentTest.class)
public class WeHaveDancedComponentTest {

    @Inject
    private WeHaveDancedFunction weHaveDancedFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new WeHaveDancedModule(), new WouldYouLikeModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parse("<div style=\"Color:#3f94aa;Position:Absolute;Top:806;\"><font style=\"font-size:19px;\">*</font>&nbsp;Dave Solo</div>").body().child(0))
                .flatMap(weHaveDancedFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals((Integer) 806, testObserver.values().get(0).getLeft());
        assertEquals("* Dave Solo", testObserver.values().get(0).getRight());
    }

    @Test
    public void verifyRight2() throws Exception {
        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parse("<div style=\"Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;\">SHOW NOTES:<br />&nbsp;<img src=\"images/setlists/NewArrow.png\" width=\"16\" height=\"9\" />&nbsp;indicates a segue into next song</div>").body().child(0))
                .flatMap(weHaveDancedFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals((Integer) 762, testObserver.values().get(0).getLeft());
        assertEquals("-> indicates a segue into next song", testObserver.values().get(0).getRight());
    }

}

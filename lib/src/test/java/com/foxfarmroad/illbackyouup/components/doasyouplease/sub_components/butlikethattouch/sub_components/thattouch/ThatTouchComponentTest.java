package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.LocStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.ArrayList;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(ComponentTest.class)
public class ThatTouchComponentTest {

    @Inject
    @LocStyle
    private String locStyle;

    @Inject
    private ThatTouchFunction thatTouchFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(new ThatTouchModule(), new WouldYouLikeModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Node node1 = mock(Node.class);
        when(node1.attr(anyString())).thenReturn(locStyle);
        TextNode node2 = mock(TextNode.class);
        when(node2.attr(anyString())).thenReturn("");
        when(node2.nodeName()).thenReturn("#text");
        when(node2.text()).thenReturn("1.test");
        Node node3 = mock(Node.class);
        when(node3.attr(anyString())).thenReturn("");
        when(node3.nodeName()).thenReturn("img");
        Node node4 = mock(Node.class);
        when(node4.attr(anyString())).thenReturn("");
        when(node4.nodeName()).thenReturn("div");
        Node node5 = mock(Node.class);
        when(node5.attr(anyString())).thenReturn("");
        when(node5.nodeName()).thenReturn("test");

        ArrayList<String> list = new ArrayList<>();
        list.add("test ->");

        TestObserver<Node> testObserver = Observable.fromArray(node1, node2, node3, node4, node5)
                .flatMap(thatTouchFunction)
                .takeLast(1)
                .test();

        testObserver.awaitTerminalEvent();
        // TODO: testObserver.assertResult(list);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.io.Do;
import com.foxfarmroad.illbackyouup.io.DoAsYouPlease;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class DoAsYouPleaseFunctionUnitTest {

    @Mock
    @Bind
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Inject
    @StyleAttr
    private String attrName;

    @Inject
    @LocStyle
    private String locStyle;

    @Mock
    @Bind
    private Function<Node, Observable<Do>> iveKnownFunction;

    @Mock
    @Bind
    @ButLikeThatTouch
    private Function<YourStepsBean, Observable<List<String>>> butLikeThatTouchFunction;

    @Mock
    @Bind
    @IllGoOn
    private Function<YourStepsBean, Observable<List<String>>> illGoOnFunction;

    @Inject
    private DoAsYouPleaseFunction doAsYouPleaseFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new DoAsYouPleaseModule()).with(BoundFieldModule.of(this)))
                .injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Node node = mock(Node.class);
        YourStepsBean yourStepsBean = mock(YourStepsBean.class);
        Document document = mock(Document.class);
        Do d0 = new Do("", "", "", "");

        when(yourStepsBean.getBody()).thenReturn(document);
        when(butIKnowFunction.apply(any())).thenReturn(Observable.just(node));
//        doAnswer(invocation -> invocation.getArguments()[0] = butLikeThatTouchBean)
//                .when(butLikeThatTouchBiConsumer).accept(any(), any());
        when(node.nodeName()).thenReturn("div");
        when(node.attr(attrName)).thenReturn(locStyle);
        when(iveKnownFunction.apply(any())).thenReturn(Observable.just(d0));
        when(butLikeThatTouchFunction.apply(any())).thenReturn(Observable.just(new ArrayList<>()));
        when(illGoOnFunction.apply(any())).thenReturn(Observable.just(new ArrayList<>()));

        TestObserver<DoAsYouPlease> testObserver = Observable.just(yourStepsBean)
                .flatMap(doAsYouPleaseFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.awaitCount(1);
    }

//    @Test
//    public void verifyRight2() throws Exception {
//        Node node = mock(Node.class);
//        ButLikeThatTouchBean butLikeThatTouchBean = mock(ButLikeThatTouchBean.class);
//        YourStepsBean yourStepsBean = mock(YourStepsBean.class);
//        Document document = mock(Document.class);
//        Do d0 = mock(Do.class);
//
//        when(yourStepsBean.getBody()).thenReturn(document);
//        when(butIKnowFunction.apply(any())).thenReturn(Observable.just(node));
//        doAnswer(invocation -> invocation.getArguments()[0] = butLikeThatTouchBean)
//                .when(illGoOnBiConsumer).accept(any(), any());
//        when(node.nodeName()).thenReturn("div", "#text");
//        when(node.attr(attrName)).thenReturn(locStyle);
//        when(iveKnownFunction.apply(any())).thenReturn(Observable.just(d0));
//        when(foreverOnlyKnowingFunction.apply(any())).thenReturn(Observable.just(Boolean.FALSE));
//
//        TestObserver<DoAsYouPlease> testObserver = Observable.just(yourStepsBean)
//                .map(doAsYouPleaseFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertEquals(d0, testObserver.values().get(0).getDo());
//    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.AndForSure;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class ButLikeFunctionUnitTest {

    @Mock
    @Bind
    @AndForSure
    private Function<Node, Observable<Pair<Integer, String>>> andForSureFunction;

    @Inject
    private ButLikeFunction butLikeFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new ButLikeModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Element node = new Element("div");
        Node node2 = new Element("div");
        node2.attr("style", "");
        Node node1 = new Element("div");
        node1.attr("style", "");
        node.appendChild(node2);
        node.appendChild(node1);
        when(andForSureFunction.apply(any())).thenReturn(Observable.just(ImmutablePair.of(2, "Test2")),
                Observable.just(ImmutablePair.of(1, "Test1")));

        ArrayList<String> list = new ArrayList<>();
        list.add("Test1");
        list.add("Test2");

        TestObserver<List<String>> testObserver = Observable.just(node)
                .flatMap(butLikeFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(list);
    }

    @Test
    public void verifyRight2() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("div");
        Node node2 = mock(Node.class);
        when(node2.nodeName()).thenReturn("div");
        when(node2.hasAttr(anyString())).thenReturn(Boolean.TRUE);
        Node node1 = mock(Node.class);
        when(node1.nodeName()).thenReturn("div");
        when(node1.hasAttr(anyString())).thenReturn(Boolean.TRUE);
        List<Node> nodeList = new ArrayList<>();
        nodeList.add(node2);
        nodeList.add(node1);
        when(node.childNodes()).thenReturn(nodeList);
        Observable<Node> nodeObservable = Observable.just(node);
        when(andForSureFunction.apply(any())).thenReturn(Observable.just(ImmutablePair.of(2, "Test2")),
                Observable.just(ImmutablePair.of(1, "DUMMY")));

        ArrayList<String> list = new ArrayList<>();
        list.add("Test2");

        TestObserver<List<String>> testObserver = nodeObservable
                .flatMap(butLikeFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(list);
    }

    @Test
    public void verifyB1() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("div");
        Node node1 = mock(Node.class);
        when(node1.nodeName()).thenReturn("div");
        when(node1.hasAttr(anyString())).thenReturn(Boolean.TRUE);
        List<Node> nodeList = new ArrayList<>();
        nodeList.add(node1);
        when(node.childNodes()).thenReturn(nodeList);
        Observable<Node> nodeObservable = Observable.just(node);
        when(andForSureFunction.apply(any())).thenReturn(Observable.just(ImmutablePair.of(1, "Test1")));

        ArrayList<String> list = new ArrayList<>();
        list.add("Test1");

        TestObserver<List<String>> testObserver = nodeObservable
                .flatMap(butLikeFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(list);
    }

    @Test
    public void verifyB2() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("div");
        List<Node> nodeList = new ArrayList<>();
        when(node.childNodes()).thenReturn(nodeList);
        when(andForSureFunction.apply(any())).thenReturn(Observable.empty());

        TestObserver<List<String>> testObserver = Observable.just(node)
                .flatMap(butLikeFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

    @Test
    public void verifyB3() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("");
        Observable<Node> nodeObservable = Observable.just(node);

        TestObserver<List<String>> testObserver = nodeObservable
                .flatMap(butLikeFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

    @Test
    public void verifyB4() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("div");
        Node node2 = mock(Node.class);
        when(node2.nodeName()).thenReturn("");
        Node node1 = mock(Node.class);
        when(node1.nodeName()).thenReturn("");
        List<Node> nodeList = new ArrayList<>();
        nodeList.add(node2);
        nodeList.add(node1);
        when(node.childNodes()).thenReturn(nodeList);
        Observable<Node> nodeObservable = Observable.just(node);

        TestObserver<List<String>> testObserver = nodeObservable
                .flatMap(butLikeFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

    @Test
    public void verifyB5() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("div");
        Node node2 = mock(Node.class);
        when(node2.nodeName()).thenReturn("div");
        when(node2.hasAttr(anyString())).thenReturn(Boolean.TRUE);
        Node node1 = mock(Node.class);
        when(node1.nodeName()).thenReturn("div");
        when(node1.hasAttr(anyString())).thenReturn(Boolean.FALSE);
        List<Node> nodeList = new ArrayList<>();
        nodeList.add(node2);
        nodeList.add(node1);
        when(node.childNodes()).thenReturn(nodeList);
        Observable<Node> nodeObservable = Observable.just(node);
        when(andForSureFunction.apply(node2)).thenReturn(Observable.just(ImmutablePair.of(2, "Test2")));

        ArrayList<String> list = new ArrayList<>();
        list.add("Test2");

        TestObserver<List<String>> testObserver = nodeObservable
                .flatMap(butLikeFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(list);
    }

    @Test
    public void verifyB6() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("div");
        Node node2 = mock(Node.class);
        when(node2.nodeName()).thenReturn("div");
        when(node2.hasAttr(anyString())).thenReturn(Boolean.TRUE);
        Node node1 = mock(Node.class);
        when(node1.nodeName()).thenReturn("div");
        when(node1.hasAttr(anyString())).thenReturn(Boolean.TRUE);
        List<Node> nodeList = new ArrayList<>();
        nodeList.add(node2);
        nodeList.add(node1);
        when(node.childNodes()).thenReturn(nodeList);
        Observable<Node> nodeObservable = Observable.just(node);
        when(andForSureFunction.apply(node2)).thenReturn(Observable.just(ImmutablePair.of(2, "DUMMY")));
        when(andForSureFunction.apply(node1)).thenReturn(Observable.just(ImmutablePair.of(1, "DUMMY")));

        TestObserver<List<String>> testObserver = nodeObservable
                .flatMap(butLikeFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

}

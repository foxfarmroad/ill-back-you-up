package com.foxfarmroad.illbackyouup.components.sometimes;

import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.TestObserver;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SometimesFunctionUnitTest {
    @Mock
    @Bind
    private Function<YourStepsBean, Observable<WalkBean>> weWalkFunction;
    @Mock
    @Bind
    private Predicate<WalkBean> somePredicate;
    @Mock
    @Bind
    private Function<WalkBean, Observable<Integer>> weRunAwayFunction;
    @Mock
    @Bind
    private DateTimeFormatter dateTimeFormatter;
    @Mock
    @Bind
    private Function3<DateTime, DateTime, Integer, Observable<Long>> timesFunction3;
    @Inject
    @Times
    private Long times;
    @Inject
    private SometimesFunction sometimesFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new SometimesModule()).with(BoundFieldModule.of(this)))
                .injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getDate()).thenReturn("");

        when(weWalkFunction.apply(any())).thenReturn(Observable.just(walkBean));
        when(somePredicate.test(any())).thenReturn(Boolean.TRUE);
        when(weRunAwayFunction.apply(any())).thenReturn(Observable.just(0));
        when(dateTimeFormatter.parseDateTime(any())).thenReturn(new DateTime().withZone(DateTimeZone.UTC));
        when(timesFunction3.apply(any(), any(), any())).thenReturn(Observable.just(Long.MAX_VALUE));

        TestObserver<Long> testObserver = Observable.just(mock(YourStepsBean.class))
                .flatMap(sometimesFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(Long.MAX_VALUE);
    }

    @Test
    public void verifyB1() throws Exception {
        when(weWalkFunction.apply(any())).thenReturn(Observable.empty());
        when(somePredicate.test(any())).thenReturn(Boolean.TRUE);
        when(weRunAwayFunction.apply(any())).thenReturn(Observable.just(0));
        when(dateTimeFormatter.parseDateTime(any())).thenReturn(new DateTime().withZone(DateTimeZone.UTC));
        when(timesFunction3.apply(any(), any(), any())).thenReturn(Observable.just(Long.MAX_VALUE));

        TestObserver<Long> testObserver = Observable.just(mock(YourStepsBean.class))
                .flatMap(sometimesFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(times);
    }

    @Test
    public void verifyB2() throws Exception {
        when(weWalkFunction.apply(any())).thenReturn(Observable.empty());
        when(somePredicate.test(any())).thenReturn(Boolean.FALSE);

        TestObserver<Long> testObserver = Observable.just(mock(YourStepsBean.class))
                .flatMap(sometimesFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(times);
    }

    @Test
    public void verifyE1() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getDate()).thenReturn("");

        when(weWalkFunction.apply(any())).thenReturn(Observable.just(walkBean));
        when(somePredicate.test(any())).thenReturn(Boolean.TRUE);
        when(weRunAwayFunction.apply(any())).thenReturn(Observable.just(0));
        when(dateTimeFormatter.parseDateTime(any())).thenThrow(UnsupportedOperationException.class);
        when(timesFunction3.apply(any(), any(), any())).thenReturn(Observable.just(Long.MAX_VALUE));

        TestObserver<Long> testObserver = Observable.just(mock(YourStepsBean.class))
                .flatMap(sometimesFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(UnsupportedOperationException.class);
    }

    @Test
    public void verifyE2() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getDate()).thenReturn("");

        when(weWalkFunction.apply(any())).thenReturn(Observable.just(walkBean));
        when(somePredicate.test(any())).thenReturn(Boolean.TRUE);
        when(weRunAwayFunction.apply(any())).thenReturn(Observable.just(0));
        when(dateTimeFormatter.parseDateTime(any())).thenThrow(IllegalArgumentException.class);
        when(timesFunction3.apply(any(), any(), any())).thenReturn(Observable.just(Long.MAX_VALUE));

        TestObserver<Long> testObserver = Observable.just(mock(YourStepsBean.class))
                .flatMap(sometimesFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(IllegalArgumentException.class);
    }
}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.ConfigOverridesModule;
import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ImmutableConfiguration;
import org.apache.commons.configuration2.MapConfiguration;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import software.amazon.awssdk.services.ssm.SSMClient;
import software.amazon.awssdk.services.ssm.model.DescribeParametersRequest;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;
import software.amazon.awssdk.services.ssm.model.GetParametersRequest;
import software.amazon.awssdk.services.ssm.model.GetParametersResponse;
import software.amazon.awssdk.services.ssm.model.ParametersFilter;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

@Category(UnitTest.class)
public class HardToRememberModuleUnitTest {

    @Inject
    private SSMClient ssmClient;

    @Inject
    private Function<Collection<String>, GetParametersRequest> collectionToGetParametersRequestFunction;

    @Inject
    private Function<Collection<String>, ParametersFilter> collectionToParametersFilterFunction;

    @Inject
    private Function<DescribeParametersRequest, Observable<DescribeParametersResponse>> describeParametersRequestToDescribeParametersResponseFunction;

    @Inject
    private Function<DescribeParametersResponse, Observable<List<String>>> describeParametersResponseToNameListObservableFunction;

    @Inject
    private Predicate<DescribeParametersResponse> filterDescribeParametersResponseFunction;

    @Inject
    private Function<GetParametersRequest, Observable<GetParametersResponse>> getParametersRequestToGetParametersResponseFunction;

    @Inject
    private Function<GetParametersResponse, Observable<MapConfiguration>> getParametersResponseToMapConfigurationObservableFunction;

    @Inject
    private Function<Configuration, Observable<ImmutableConfiguration>> configurationToImmutableConfigurationObservableFunction;

    @Inject
    private Function<Collection<ParametersFilter>, DescribeParametersRequest> parametersFilterListToDescribeParametersRequestFunction;

    @Inject
    private ImmutableConfiguration immutableConfiguration;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(Modules.override(new HardToRememberModule()).with(new ConfigOverridesModule())).injectMembers(this);
    }

}

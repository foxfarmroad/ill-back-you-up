package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.we;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class WeFunctionUnitTest {

    @Mock
    @Bind
    private Map<String, String> countryCodeMap;

    @Inject
    private WeFunction weFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new WeModule()).with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        when(countryCodeMap.containsKey(any())).thenReturn(Boolean.TRUE);
        when(countryCodeMap.get(any())).thenReturn("testy");

        TestObserver<String> testObserver = Observable.just("test")
                .flatMap(weFunction)
                .test();

        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        testObserver.assertValue("testy");
        verify(countryCodeMap, times(1)).containsKey(any());
        verify(countryCodeMap, times(1)).get(any());
        verifyNoMoreInteractions(countryCodeMap);
    }

    @Test
    public void verifyB() throws Exception {
        when(countryCodeMap.containsKey(any())).thenReturn(Boolean.FALSE);

        TestObserver<String> testObserver = Observable.just("test")
                .flatMap(weFunction)
                .test();

        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(0);
        verify(countryCodeMap, times(1)).containsKey(any());
        verifyNoMoreInteractions(countryCodeMap);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.sub_components.intherisk;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.IRememberThinking;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class InTheRiskConsumerUnitTest {

    @Mock
    @Bind
    @IRememberThinking
    private Function<String, Observable<String>> iRememberThinkingFunction;

    @Inject
    private InTheRiskConsumer inTheRiskConsumer;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new InTheRiskModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        when(iRememberThinkingFunction.apply(any())).thenReturn(Observable.just("Dave Solo"));

        TestObserver<StringBuilder> testObserver = Observable.fromArray(
                        Jsoup.parseBodyFragment("<font style=\"font-size:19px;\">*</font>").body().child(0),
                        Jsoup.parse("&nbsp;Dave Solo").body().textNodes().get(0)
                )
                .collectInto(new StringBuilder(), inTheRiskConsumer)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("* Dave Solo", testObserver.values().get(0).toString());
        verify(iRememberThinkingFunction, times(1)).apply(any());
        verifyNoMoreInteractions(iRememberThinkingFunction);
    }

    @Test
    public void verifyRight2() throws Exception {
        Node showNotesNode = Jsoup.parse("SHOW NOTES:").body().textNodes().get(0);
        Node blankNode = Jsoup.parse("&nbsp;").body().textNodes().get(0);
        Node segueNode = Jsoup.parse("&nbsp;indicates a segue into next song").body().textNodes().get(0);

        when(iRememberThinkingFunction.apply(((TextNode) showNotesNode).text())).thenReturn(Observable.empty());
        when(iRememberThinkingFunction.apply(((TextNode) blankNode).text())).thenReturn(Observable.just(""));
        when(iRememberThinkingFunction.apply(((TextNode) segueNode).text())).thenReturn(Observable.just("indicates a segue into next song"));

        TestObserver<StringBuilder> testObserver = Observable.fromArray(
                        showNotesNode,
                        Jsoup.parse("<br />").body().child(0),
                        blankNode,
                        Jsoup.parse("<img src=\"images/setlists/NewArrow.png\" width=\"16\" height=\"9\" />").body().child(0),
                        segueNode
                )
                .collectInto(new StringBuilder(), inTheRiskConsumer)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("-> indicates a segue into next song", testObserver.values().get(0).toString());
        verify(iRememberThinkingFunction, times(3)).apply(any());
        verifyNoMoreInteractions(iRememberThinkingFunction);
    }

}

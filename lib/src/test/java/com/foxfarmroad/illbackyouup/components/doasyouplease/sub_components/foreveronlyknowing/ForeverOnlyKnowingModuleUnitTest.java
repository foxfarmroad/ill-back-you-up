package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.List;

@Category(UnitTest.class)
public class ForeverOnlyKnowingModuleUnitTest {

    @Inject
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Inject
    private BiFunction<Boolean, List<Node>, Boolean> foreverBiFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new ForeverOnlyKnowingModule()).injectMembers(this);
    }
}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.LocStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class ThatTouchFunctionUnitTest {

    @Inject
    @LocStyle
    private String locStyle;

    @Mock
    @Bind
    private Predicate<Node> thatPredicate;

    @Mock
    @Bind
    private BiFunction<List<String>, Node, List<String>> touchBiFunction;

    @Inject
    private ThatTouchFunction thatTouchFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new ThatTouchModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Node node = mock(Node.class);
        when(node.attr(anyString())).thenReturn("");

        ArrayList<String> list = new ArrayList<>();
        list.add("Test1");
        list.add("Test2");

        when(thatPredicate.test(any())).thenReturn(Boolean.TRUE);
        when(touchBiFunction.apply(any(), any())).thenReturn(list);

        TestObserver<Node> testObserver = Observable.just(node)
                .flatMap(thatTouchFunction)
                .test();

        testObserver.awaitTerminalEvent();
        // TODO: testObserver.assertResult(new ArrayList<>(), list);
    }

    @Test
    public void verifyRight2() throws Exception {
        Node node = mock(Node.class);
        when(node.attr(anyString())).thenReturn(locStyle);

        TestObserver<Node> testObserver = Observable.just(node)
                .flatMap(thatTouchFunction)
                .test();

        testObserver.awaitTerminalEvent();
        // TODO: testObserver.assertResult(new ArrayList<>());
    }

    @Test
    public void verifyRight3() throws Exception {
        Node node = mock(Node.class);
        when(node.attr(anyString())).thenReturn("");

        ArrayList<String> list = new ArrayList<>();
        list.add("Test1");
        list.add("Test2");

        when(thatPredicate.test(any())).thenReturn(Boolean.FALSE);
        when(thatPredicate.test(any())).thenReturn(Boolean.TRUE);
        when(touchBiFunction.apply(any(), any())).thenReturn(list);

        TestObserver<Node> testObserver = Observable.just(node)
                .flatMap(thatTouchFunction)
                .test();

        testObserver.awaitTerminalEvent();
        // TODO: testObserver.assertResult(new ArrayList<>(), list);
    }

}

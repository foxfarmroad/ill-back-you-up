package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class WouldYouLikeModuleUnitTest {

    @Inject
    @NonBreakingSpace
    private char nonBreakingSpace;

    @Inject
    @ReplacementCharacter
    private char replacementCharacter;

    @Inject
    @Apostrophe
    private char apostrophe;

    @Inject
    @BadTranslation
    private String badTranslation;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new WouldYouLikeModule()).injectMembers(this);
    }

}

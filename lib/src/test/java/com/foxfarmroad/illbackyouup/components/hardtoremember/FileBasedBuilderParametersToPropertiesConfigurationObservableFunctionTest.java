package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.FileBasedBuilderParameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static org.mockito.Mockito.mock;

@Category(UnitTest.class)
public class FileBasedBuilderParametersToPropertiesConfigurationObservableFunctionTest {

    @Inject
    private FileBasedBuilderParametersToPropertiesConfigurationObservableFunction fileBasedBuilderParametersToPropertiesConfigurationObservableFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.EMPTY_MODULE).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        FileBasedBuilderParameters fileBasedBuilderParameters = mock(FileBasedBuilderParameters.class);

        TestObserver<PropertiesConfiguration> testObserver = Observable.just(fileBasedBuilderParameters)
                .flatMap(fileBasedBuilderParametersToPropertiesConfigurationObservableFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
    }

}

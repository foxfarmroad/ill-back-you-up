package com.foxfarmroad.illbackyouup.components.heaviestweight;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class HeaviestWeightTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(Heaviest.class).to("http://localhost/");
        bindConstant().annotatedWith(Weight.class).to("test");
        bindConstant().annotatedWith(Username.class).to("test");
        bindConstant().annotatedWith(Password.class).to("test");
        bindConstant().annotatedWith(FormAction.class).to("test");
    }

    @Provides
    MockRetrofit provideMockRetrofit(@Heaviest Retrofit retrofit, NetworkBehavior networkBehavior) {
        return new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();
    }

    @Provides
    HeaviestWeightEndpoint provideHeaviestWeightEndpoint(MockRetrofit mockRetrofit,
                                                         Response<ResponseBody> responseBodyResponse) {
        BehaviorDelegate<HeaviestWeightEndpoint> delegate = mockRetrofit.create(HeaviestWeightEndpoint.class);
        return new HeaviestWeightMockEndpoint(delegate, responseBodyResponse);
    }

    @Provides
    Response<ResponseBody> provideResponseBodyResponse() {
        return Response.success(ResponseBody.create(MediaType.parse("text/plain"), ""));
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run;

import com.foxfarmroad.illbackyouup.io.RunBean;
import io.reactivex.Observable;
import retrofit2.http.Query;
import retrofit2.http.Url;
import retrofit2.mock.BehaviorDelegate;

public class RunMockEndpoint implements RunEndpoint {

    private final BehaviorDelegate<RunEndpoint> delegate;
    private final RunBean runBean;

    public RunMockEndpoint(BehaviorDelegate<RunEndpoint> service, RunBean runBean) {
        this.delegate = service;
        this.runBean = runBean;
    }

    @Override
    public Observable<RunBean> getGeocodeJson(@Url String path, @Query("address") String address) {
        return delegate.returningResponse(runBean).getGeocodeJson(path, address);
    }

}

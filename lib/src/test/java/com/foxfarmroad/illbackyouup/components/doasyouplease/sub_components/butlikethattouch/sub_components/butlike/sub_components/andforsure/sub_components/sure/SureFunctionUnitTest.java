package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.sure;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class SureFunctionUnitTest {

    @Mock
    @Bind
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Inject
    private SureFunction sureFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new TheTouchOfYouModule(), new SureModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        when(theTouchOfYouFunction.apply(any())).thenReturn(Observable.just("Don't Drink the WaterÄ"));

        TestObserver<String> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:606;\">18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;\n<img src=\"images/setlists/NewArrow.png\" hspace=\"4\" /></div>").body().childNode(0))
                .flatMap(sureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("Don't Drink the WaterÄ", testObserver.values().get(0));
        verify(theTouchOfYouFunction, times(1)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

    @Test
    public void verifyRight2() throws Exception {
        when(theTouchOfYouFunction.apply(any())).thenReturn(Observable.just("Encore:"));

        TestObserver<String> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:691;\">-------- ENCORE -------- &nbsp;</div>").body().childNode(0))
                .flatMap(sureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("Encore:", testObserver.values().get(0));
        verify(theTouchOfYouFunction, times(1)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

    @Test
    public void verifyRight3() throws Exception {
        when(theTouchOfYouFunction.apply(any())).thenReturn(Observable.just("Set Break"));

        TestObserver<String> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:426;\">-------- SET BREAK -------- &nbsp;</div>").body().childNode(0))
                .flatMap(sureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("Set Break", testObserver.values().get(0));
        verify(theTouchOfYouFunction, times(1)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

    @Test
    public void verifyB() throws Exception {
        when(theTouchOfYouFunction.apply(any())).thenReturn(Observable.empty());

        Element element = mock(Element.class);
        when(element.text()).thenReturn("test");

        TestObserver<String> testObserver = Observable.just(element)
                .flatMap(sureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("DUMMY", testObserver.values().get(0));
        verify(theTouchOfYouFunction, times(1)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

}

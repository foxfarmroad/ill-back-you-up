package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.some;

import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * <p>
 * </p>
 */
public class SomePredicateUnitTest {

    @Mock
    @Bind
    private DateTimeFormatter dateTimeFormatter;

    @Inject
    private DateTime now;

    @Inject
    private SomePredicate somePredicate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.combine(new SomeTestModule(), BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getDate()).thenReturn("");

        when(dateTimeFormatter.parseDateTime(any())).thenReturn(now.minusHours(10));

        TestObserver<WalkBean> testObserver = Observable.just(walkBean)
                .filter(somePredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(walkBean);
    }

    @Test
    public void verifyRight2() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getDate()).thenReturn("");

        when(dateTimeFormatter.parseDateTime(any())).thenReturn(now.plusHours(8));

        TestObserver<WalkBean> testObserver = Observable.just(walkBean)
                .filter(somePredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(walkBean);
    }

    @Test
    public void verifyB1() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getDate()).thenReturn("");

        when(dateTimeFormatter.parseDateTime(any())).thenReturn(now.minusDays(1));

        TestObserver<WalkBean> testObserver = Observable.just(walkBean)
                .filter(somePredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

    @Test
    public void verifyB2() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getDate()).thenReturn("");

        when(dateTimeFormatter.parseDateTime(any())).thenReturn(now.plusHours(23));

        TestObserver<WalkBean> testObserver = Observable.just(walkBean)
                .filter(somePredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(walkBean);
    }

    @Test
    public void verifyB3() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getDate()).thenReturn("");

        when(dateTimeFormatter.parseDateTime(any())).thenReturn(now.plusHours(25));

        TestObserver<WalkBean> testObserver = Observable.just(walkBean)
                .filter(somePredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

    @Test
    public void verifyE1() throws Exception {
        when(dateTimeFormatter.parseDateTime(any())).thenThrow(UnsupportedOperationException.class);

        TestObserver<WalkBean> testObserver = Observable.just(mock(WalkBean.class))
                .filter(somePredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(UnsupportedOperationException.class);
    }

    @Test
    public void verifyE2() throws Exception {
        when(dateTimeFormatter.parseDateTime(any())).thenThrow(IllegalArgumentException.class);

        TestObserver<WalkBean> testObserver = Observable.just(mock(WalkBean.class))
                .filter(somePredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(IllegalArgumentException.class);
    }

}

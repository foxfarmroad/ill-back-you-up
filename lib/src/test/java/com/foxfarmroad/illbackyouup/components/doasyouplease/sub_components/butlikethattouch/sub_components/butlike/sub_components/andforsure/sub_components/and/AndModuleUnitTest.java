package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.and;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class AndModuleUnitTest {

    @Inject
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(Modules.combine(new AndModule(), new TheTouchOfYouModule(), new WouldYouLikeModule()))
                .injectMembers(this);
    }

}

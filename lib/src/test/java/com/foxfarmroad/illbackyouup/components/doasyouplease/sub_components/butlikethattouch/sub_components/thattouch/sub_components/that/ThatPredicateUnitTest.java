package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.sub_components.that;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class ThatPredicateUnitTest {

    @Inject
    private ThatPredicate thatPredicate;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.EMPTY_MODULE).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("div");

        TestObserver<Node> testObserver = Observable.just(node)
                .filter(thatPredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(node);
    }

    @Test
    public void verifyRight2() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("img");

        TestObserver<Node> testObserver = Observable.just(node)
                .filter(thatPredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

}

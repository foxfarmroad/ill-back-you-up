package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.sub_components.touch;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.TextNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class TouchBiFunctionUnitTest {

    @Mock
    @Bind
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Inject
    private TouchBiFunction touchBiFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new TouchModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        TextNode node = mock(TextNode.class);
        when(node.nodeName()).thenReturn("#text");
        when(node.text()).thenReturn("");

        when(theTouchOfYouFunction.apply(anyString())).thenReturn(Observable.just("test"));

        ArrayList<String> list = new ArrayList<>();
        list.add("test");

        TestObserver<List<String>> testObserver = Observable.just(node)
                .scan(new ArrayList<>(), touchBiFunction)
                .takeLast(1)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(list);
    }

    @Test
    public void verifyRight2() throws Exception {
        TextNode node = mock(TextNode.class);
        when(node.nodeName()).thenReturn("img");

        ArrayList<String> preList = new ArrayList<>();
        preList.add("test");

        TestObserver<List<String>> testObserver = Observable.just(node)
                .scan(preList, touchBiFunction)
                .takeLast(1)
                .test();

        ArrayList<String> postList = new ArrayList<>();
        postList.add("test ->");

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(postList);
    }

    @Test
    public void verifyB() throws Exception {
        TextNode node = mock(TextNode.class);
        when(node.nodeName()).thenReturn("img");

        TestObserver<List<String>> testObserver = Observable.just(node)
                .scan(new ArrayList<>(), touchBiFunction)
                .takeLast(1)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(new ArrayList<>());
    }

}

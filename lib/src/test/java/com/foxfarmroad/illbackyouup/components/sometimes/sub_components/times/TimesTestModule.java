package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.times;

import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.google.inject.AbstractModule;

/**
 * <p>
 * </p>
 */
public class TimesTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(TimeStart.class).to(20);
        bindConstant().annotatedWith(TimeOffset.class).to(13);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run;

import com.foxfarmroad.RetrofitTestModule;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.junit.Test;
import retrofit2.Retrofit;

import javax.inject.Inject;

public class RunModuleUnitTest {

    @Inject
    @Run
    private Retrofit retrofit;

    @Inject
    private RunEndpoint runEndpoint;

    @Inject
    private HttpLoggingInterceptor httpLoggingInterceptor;

    @Inject
    private OkHttpClient okHttpClient;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new RunModule(), new RetrofitTestModule(),
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bindConstant().annotatedWith(Run.class).to("http://localhost");
                    }
                }).injectMembers(this);
    }
}

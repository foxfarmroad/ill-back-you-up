package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import software.amazon.awssdk.services.ssm.model.GetParametersRequest;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category(UnitTest.class)
public class CollectionToGetParametersRequestFunctionTest {

    @Inject
    private CollectionToGetParametersRequestFunction collectionToGetParametersRequestFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.EMPTY_MODULE).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        Collection<String> stringCollection = new ArrayList<>();
        stringCollection.add("test");

        TestObserver<GetParametersRequest> testObserver = Observable.just(stringCollection)
                .map(collectionToGetParametersRequestFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        assertTrue(testObserver.values().get(0).withDecryption());
        assertEquals(stringCollection, testObserver.values().get(0).names());
    }

    @Test
    public void verifyB() throws Exception {
        Collection<String> stringCollection = new ArrayList<>();

        TestObserver<GetParametersRequest> testObserver = Observable.just(stringCollection)
                .map(collectionToGetParametersRequestFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        assertTrue(testObserver.values().get(0).withDecryption());
        assertEquals(stringCollection, testObserver.values().get(0).names());
    }

}

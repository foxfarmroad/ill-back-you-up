package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run;

import com.foxfarmroad.illbackyouup.io.GeometryBean;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.foxfarmroad.illbackyouup.io.RunBean;
import com.foxfarmroad.illbackyouup.io.RunResultsBean;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import java.util.Collections;

public class RunTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(Run.class).to("http://localhost");
        bindConstant().annotatedWith(Path.class).to("test");
    }

    @Provides
    MockRetrofit provideMockRetrofit(@Run Retrofit retrofit, NetworkBehavior networkBehavior) {
        return new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();
    }

    @Provides
    RunEndpoint provideRunEndpoint(MockRetrofit mockRetrofit, RunBean runBean) {
        BehaviorDelegate<RunEndpoint> delegate = mockRetrofit.create(RunEndpoint.class);
        return new RunMockEndpoint(delegate, runBean);
    }

    @Provides
    RunBean provideRunBean() {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(1.0f);
        latLngBean.setLng(1.0f);
        GeometryBean geometryBean = new GeometryBean();
        geometryBean.setLocation(latLngBean);
        RunResultsBean runResultsBean = new RunResultsBean();
        runResultsBean.setGeometry(geometryBean);
        RunBean runBean = new RunBean();
        runBean.setResults(Collections.singletonList(runResultsBean));

        return runBean;
    }

}

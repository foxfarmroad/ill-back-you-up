package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.sub_components.intherisk;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

@Category(ComponentTest.class)
public class InTheRiskComponentTest {

    @Inject
    private InTheRiskConsumer inTheRiskConsumer;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new InTheRiskModule(), new WouldYouLikeModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        TestObserver<StringBuilder> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;\">SHOW NOTES:<br />&nbsp;<img src=\"images/setlists/NewArrow.png\" width=\"16\" height=\"9\" />&nbsp;indicates a segue into next song</div>").body().childNode(0))
                .flatMapIterable(Node::childNodes)
                .collectInto(new StringBuilder(), inTheRiskConsumer)
                .toObservable()
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("-> indicates a segue into next song", testObserver.values().get(0).toString());
    }

    @Test
    public void verifyRight2() throws Exception {
        TestObserver<StringBuilder> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#3f94aa;Position:Absolute;Top:840;\"><font style=\"font-size:19px;\">~</font>&nbsp;Carter, Dave and Tim</div>").body().childNode(0))
        .flatMapIterable(Node::childNodes)
        .collectInto(new StringBuilder(), inTheRiskConsumer)
        .toObservable()
        .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("~ Carter, Dave and Tim", testObserver.values().get(0).toString());
    }

}

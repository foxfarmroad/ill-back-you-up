package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway;

import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import javax.inject.Inject;

public class WeRunAwayModuleUnitTest {

    @Inject
    private Function<String, Observable<LatLngBean>> weRunFunction;

    @Inject
    private DateTimeFormatter dateTimeFormatter;

    @Inject
    @TimeStart
    private Integer timeStart;

    @Inject
    private BiFunction<LatLngBean, DateTime, Observable<Integer>> awayBiFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new WeRunAwayModule(), new WeRunAwayModuleTestModule()).injectMembers(this);
    }
}

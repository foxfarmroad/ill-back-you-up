package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast;

import com.foxfarmroad.illbackyouup.io.HowFastBean;
import io.reactivex.Observable;
import retrofit2.http.Query;
import retrofit2.http.Url;
import retrofit2.mock.BehaviorDelegate;

public class HowFastMockEndpoint implements HowFastEndpoint {

    private final BehaviorDelegate<HowFastEndpoint> delegate;
    private final HowFastBean howFastBean;

    public HowFastMockEndpoint(BehaviorDelegate<HowFastEndpoint> service, HowFastBean howFastBean) {
        this.delegate = service;
        this.howFastBean = howFastBean;
    }

    @Override
    public Observable<HowFastBean> getTimezoneJson(@Url String fast, @Query("location") String location,
                                                   @Query("timestamp") Long timestamp) {
        return delegate.returningResponse(howFastBean).getTimezoneJson(fast, location, timestamp);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import java.util.NoSuchElementException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class WeRunAwayFunctionUnitTest {

    @Mock
    @Bind
    private Function<String, Observable<LatLngBean>> weRunFunction;

    @Mock
    @Bind
    private DateTimeFormatter dateTimeFormatter;

    @Mock
    @Bind
    private BiFunction<LatLngBean, DateTime, Observable<Integer>> awayBiFunction;

    @Bind
    @TimeStart
    private Integer timeStart = 20;

    @Inject
    private WeRunAwayFunction weRunAwayFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new WeRunAwayModule()).with(BoundFieldModule.of(this)))
                .injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getLocation()).thenReturn("");
        when(walkBean.getDate()).thenReturn("");

        when(weRunFunction.apply(any())).thenReturn(Observable.just(mock(LatLngBean.class)));
        when(dateTimeFormatter.parseDateTime(any())).thenReturn(new DateTime());
        when(awayBiFunction.apply(any(), any())).thenReturn(Observable.just(0));

        TestObserver<Integer> testObserver = Observable.just(walkBean)
                .flatMap(weRunAwayFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(0);
        verify(weRunFunction, times(1)).apply(any());
        verifyNoMoreInteractions(weRunFunction);
        verify(dateTimeFormatter, times(1)).parseDateTime(any());
        verifyNoMoreInteractions(dateTimeFormatter);
        verify(awayBiFunction, times(1)).apply(any(), any());
        verifyNoMoreInteractions(awayBiFunction);
    }

    @Test
    public void verifyE1() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getLocation()).thenReturn(null);
        when(walkBean.getDate()).thenReturn("");

        when(weRunFunction.apply(any())).thenReturn(Observable.just(mock(LatLngBean.class)));
        when(dateTimeFormatter.parseDateTime(any())).thenReturn(new DateTime());
        when(awayBiFunction.apply(any(), any())).thenReturn(Observable.just(0));

        TestObserver<Integer> testObserver = Observable.just(walkBean)
                .flatMap(weRunAwayFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(NullPointerException.class);
        verify(weRunFunction, times(0)).apply(any());
        verifyNoMoreInteractions(weRunFunction);
        verify(dateTimeFormatter, times(0)).parseDateTime(any());
        verifyNoMoreInteractions(dateTimeFormatter);
        verify(awayBiFunction, times(0)).apply(any(), any());
        verifyNoMoreInteractions(awayBiFunction);
    }

    @Test
    public void verifyE2() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getLocation()).thenReturn("");
        when(walkBean.getDate()).thenReturn(null);

        when(weRunFunction.apply(any())).thenReturn(Observable.just(mock(LatLngBean.class)));
        when(dateTimeFormatter.parseDateTime(any())).thenReturn(new DateTime());
        when(awayBiFunction.apply(any(), any())).thenReturn(Observable.just(0));

        TestObserver<Integer> testObserver = Observable.just(walkBean)
                .flatMap(weRunAwayFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(NullPointerException.class);
        verify(weRunFunction, times(1)).apply(any());
        verifyNoMoreInteractions(weRunFunction);
        verify(dateTimeFormatter, times(0)).parseDateTime(any());
        verifyNoMoreInteractions(dateTimeFormatter);
        verify(awayBiFunction, times(0)).apply(any(), any());
        verifyNoMoreInteractions(awayBiFunction);
    }

    @Test
    public void verifyE3() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getLocation()).thenReturn("");
        when(walkBean.getDate()).thenReturn("");

        when(weRunFunction.apply(any())).thenReturn(Observable.empty());
        when(dateTimeFormatter.parseDateTime(any())).thenReturn(new DateTime());
        when(awayBiFunction.apply(any(), any())).thenReturn(Observable.just(0));

        TestObserver<Integer> testObserver = Observable.just(walkBean)
                .flatMap(weRunAwayFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(NoSuchElementException.class);
        verify(weRunFunction, times(1)).apply(any());
        verifyNoMoreInteractions(weRunFunction);
        verify(dateTimeFormatter, times(0)).parseDateTime(any());
        verifyNoMoreInteractions(dateTimeFormatter);
        verify(awayBiFunction, times(0)).apply(any(), any());
        verifyNoMoreInteractions(awayBiFunction);
    }

    @Test
    public void verifyE4() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);
        when(walkBean.getLocation()).thenReturn("");
        when(walkBean.getDate()).thenReturn("");

        when(weRunFunction.apply(any())).thenReturn(Observable.just(mock(LatLngBean.class)));
        when(dateTimeFormatter.parseDateTime(any())).thenReturn(new DateTime());
        when(awayBiFunction.apply(any(), any())).thenReturn(Observable.empty());

        TestObserver<Integer> testObserver = Observable.just(walkBean)
                .flatMap(weRunAwayFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
        verify(weRunFunction, times(1)).apply(any());
        verifyNoMoreInteractions(weRunFunction);
        verify(dateTimeFormatter, times(1)).parseDateTime(any());
        verifyNoMoreInteractions(dateTimeFormatter);
        verify(awayBiFunction, times(1)).apply(any(), any());
        verifyNoMoreInteractions(awayBiFunction);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Tag;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class IllGoOnFunctionUnitTest {

    @Mock
    @Bind
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Mock
    @Bind
    @IRememberThinking
    private Function<String, Observable<String>> iRememberThinkingFunction;

    @Inject
    private IllGoOnFunction illGoOnFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new IllGoOnModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Element node = new Element(Tag.valueOf("div"), "");

//        TestObserver<DoAsYouPlease> testObserver = Observable.just(node)
//                .collectInto((DoAsYouPlease) new ButGoOnBean(), illGoOnFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertTrue(((OnBean) testObserver.values().get(0).getYou()).getInNotes());
    }

    @Test
    public void verifyRight2() throws Exception {
        TextNode node = new TextNode("test", "");

        when(theTouchOfYouFunction.apply(any())).thenReturn(Observable.just("test"));

//        TestObserver<DoAsYouPlease> testObserver = Observable.just(node)
//                .collectInto((DoAsYouPlease) new ButGoOnBean(), illGoOnFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertFalse(((OnBean) testObserver.values().get(0).getYou()).getInNotes());
//        assertEquals(1, testObserver.values().get(0).getAs().getSongs().size());
    }

    @Test
    public void verifyRight3() throws Exception {
        Element element = new Element(Tag.valueOf("div"), "");
        TextNode node = new TextNode("test", "");

        when(iRememberThinkingFunction.apply(any())).thenReturn(Observable.just("test"));

//        TestObserver<DoAsYouPlease> testObserver = Observable.just(element, node)
//                .collectInto((DoAsYouPlease) new ButGoOnBean(), illGoOnFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertTrue(((OnBean) testObserver.values().get(0).getYou()).getInNotes());
//        assertEquals(1, testObserver.values().get(0).getYou().getNotes().size());
    }

    @Test
    public void verifyRight4() throws Exception {
        TextNode node = new TextNode("test", "");
        Element element = new Element(Tag.valueOf("img"), "");

        when(theTouchOfYouFunction.apply(any())).thenReturn(Observable.just("test"));

//        TestObserver<DoAsYouPlease> testObserver = Observable.just(node, element)
//                .collectInto((DoAsYouPlease) new ButGoOnBean(), illGoOnFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertFalse(((OnBean) testObserver.values().get(0).getYou()).getInNotes());
//        assertEquals(1, testObserver.values().get(0).getAs().getSongs().size());
//        assertEquals("test ->", testObserver.values().get(0).getAs().getSongs().get(0));
    }

    @Test
    public void verifyRight5() throws Exception {
        Element div = new Element(Tag.valueOf("div"), "");
        Element element = new Element(Tag.valueOf("img"), "");
        TextNode node = new TextNode("test", "");

        when(iRememberThinkingFunction.apply(any())).thenReturn(Observable.just("test"));

//        TestObserver<DoAsYouPlease> testObserver = Observable.just(div, element, node)
//                .collectInto((DoAsYouPlease) new ButGoOnBean(), illGoOnFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertTrue(((OnBean) testObserver.values().get(0).getYou()).getInNotes());
//        assertEquals(0, testObserver.values().get(0).getAs().getSongs().size());
//        assertEquals(1, testObserver.values().get(0).getYou().getNotes().size());
//        assertEquals("-> test", testObserver.values().get(0).getYou().getNotes().get(0));
    }

    @Test
    public void verifyRight6() throws Exception {
        Element div = new Element(Tag.valueOf("div"), "");
        Element element = Jsoup.parse("<font>*</font>").body().child(0);
        TextNode node = new TextNode("test", "");

        when(iRememberThinkingFunction.apply(any())).thenReturn(Observable.just("test"));

//        TestObserver<DoAsYouPlease> testObserver = Observable.just(div, element, node)
//                .collectInto((DoAsYouPlease) new ButGoOnBean(), illGoOnFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertTrue(((OnBean) testObserver.values().get(0).getYou()).getInNotes());
//        assertEquals(0, testObserver.values().get(0).getAs().getSongs().size());
//        assertEquals(1, testObserver.values().get(0).getYou().getNotes().size());
//        assertEquals("* test", testObserver.values().get(0).getYou().getNotes().get(0));
    }

    @Test
    public void verifyB1() throws Exception {
        Element node = new Element(Tag.valueOf("img"), "");

//        TestObserver<DoAsYouPlease> testObserver = Observable.just(node)
//                .collectInto((DoAsYouPlease) new ButGoOnBean(), illGoOnFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertFalse(((OnBean) testObserver.values().get(0).getYou()).getInNotes());
    }

}

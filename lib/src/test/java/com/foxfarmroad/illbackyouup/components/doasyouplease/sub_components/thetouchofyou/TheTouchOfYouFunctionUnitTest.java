package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class TheTouchOfYouFunctionUnitTest {

    @Mock
    @Bind
    private Function<String, Observable<String>> wouldYouLikeFunction;

    @Inject
    private TheTouchOfYouFunction theTouchOfYouFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new TheTouchOfYouModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        when(wouldYouLikeFunction.apply(any())).thenReturn(Observable.just("1.Digging a Ditch"));

        TestObserver<String> testObserver = Observable.just("")
                .flatMap(theTouchOfYouFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue("Digging a Ditch");
    }

    @Test
    public void verifyRight2() throws Exception {
        when(wouldYouLikeFunction.apply(any())).thenReturn(Observable.just("-------- SET BREAK --------"));

        TestObserver<String> testObserver = Observable.just("")
                .flatMap(theTouchOfYouFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue("Set Break");
    }

    @Test
    public void verifyRight3() throws Exception {
        when(wouldYouLikeFunction.apply(any())).thenReturn(Observable.just("-------- ENCORE --------"));

        TestObserver<String> testObserver = Observable.just("")
                .flatMap(theTouchOfYouFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
    }

    @Test
    public void verifyB() throws Exception {
        when(wouldYouLikeFunction.apply(any())).thenReturn(Observable.just("test"));

        TestObserver<String> testObserver = Observable.just("")
                .flatMap(theTouchOfYouFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

}

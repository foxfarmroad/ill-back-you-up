package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.position;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class PositionFunctionUnitTest {

    @Inject
    private PositionFunction positionFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new PositionModule()).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<Integer> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:606;\">18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;\n<img src=\"images/setlists/NewArrow.png\" hspace=\"4\" /></div>").body().childNode(0))
                .flatMap(positionFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals(new Integer(606), testObserver.values().get(0));
    }

    @Test
    public void verifyB1() throws Exception {
        Node node = mock(Node.class);
        when(node.attr("style")).thenReturn("");

        TestObserver<Integer> testObserver = Observable.just(node)
                .flatMap(positionFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

    @Test
    public void verifyB2() throws Exception {
        Node node = mock(Node.class);
        when(node.attr("style")).thenReturn("Color:#000000:Position:Absolute:Top:606:");

        TestObserver<Integer> testObserver = Observable.just(node)
                .flatMap(positionFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

    @Test
    public void verifyE() throws Exception {
        Node node = mock(Node.class);
        when(node.attr("style")).thenReturn("Color:#000000;Position:Absolute;Top:blah;");

        TestObserver<Integer> testObserver = Observable.just(node)
                .flatMap(positionFunction)
                .test();

        testObserver.assertError(NumberFormatException.class);
        testObserver.assertNotComplete();
    }

}

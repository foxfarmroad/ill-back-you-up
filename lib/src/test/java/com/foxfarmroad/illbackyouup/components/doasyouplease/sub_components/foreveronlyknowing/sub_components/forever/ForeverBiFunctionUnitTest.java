package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing.sub_components.forever;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Tag;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Category(UnitTest.class)
public class ForeverBiFunctionUnitTest {

    @Inject
    private ForeverBiFunction foreverBiFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.EMPTY_MODULE).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        List<Node> nodeList = new ArrayList<>();
        nodeList.add(new Element(Tag.valueOf("div"), ""));
        nodeList.add(new Element(Tag.valueOf("div"), ""));

        TestObserver<Boolean> testObserver = Observable.just(nodeList)
                .reduce(Boolean.FALSE, foreverBiFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(Boolean.TRUE);
    }

    @Test
    public void verifyRight2() throws Exception {
        List<Node> nodeList = new ArrayList<>();
        nodeList.add(new Element(Tag.valueOf("div"), ""));
        nodeList.add(new Element(Tag.valueOf("#text"), ""));

        TestObserver<Boolean> testObserver = Observable.just(nodeList)
                .reduce(Boolean.FALSE, foreverBiFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(Boolean.FALSE);
    }

    @Test
    public void verifyB1() throws Exception {
        List<Node> nodeList = new ArrayList<>();

        TestObserver<Boolean> testObserver = Observable.just(nodeList)
                .reduce(Boolean.FALSE, foreverBiFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(Boolean.FALSE);
    }

    @Test
    public void verifyB2() throws Exception {
        List<Node> nodeList = new ArrayList<>();

        TestObserver<Boolean> testObserver = Observable.just(nodeList)
                .filter(list -> !list.isEmpty())
                .reduce(Boolean.FALSE, foreverBiFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(Boolean.FALSE);
    }

}

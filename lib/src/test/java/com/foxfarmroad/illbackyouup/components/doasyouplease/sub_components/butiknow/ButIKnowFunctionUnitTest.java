package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class ButIKnowFunctionUnitTest {

    @Inject
    private ButIKnowFunction butIKnowFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new ButIKnowModule()).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        Document document = Jsoup.parse("<body><tag style=\"font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15px;\"><tag></tag></tag></body>");

        TestObserver<Node> testObserver = Observable.just(document)
                .flatMap(butIKnowFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
    }

    @Test
    public void verifyB1() throws Exception {
        Document document = Jsoup.parse("<body><tag style=\"font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15p;\"><tag></tag></tag></body>");

        TestObserver<Node> testObserver = Observable.just(document)
                .flatMap(butIKnowFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

    @Test
    public void verifyB2() throws Exception {
        Document document = Jsoup.parse("<body><tag style=\"font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15px;\"></tag></body>");

        TestObserver<Node> testObserver = Observable.just(document)
                .flatMap(butIKnowFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

    @Test
    public void verifyB3() throws Exception {
        Document document = Jsoup.parse("");

        TestObserver<Node> testObserver = Observable.just(document)
                .flatMap(butIKnowFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

}

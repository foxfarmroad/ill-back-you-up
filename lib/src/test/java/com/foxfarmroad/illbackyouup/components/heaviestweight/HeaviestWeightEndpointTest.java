package com.foxfarmroad.illbackyouup.components.heaviestweight;

import com.foxfarmroad.RetrofitTestModule;
import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.observers.TestObserver;
import okhttp3.ResponseBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.MockitoAnnotations;
import retrofit2.Response;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;

@Category(UnitTest.class)
public class HeaviestWeightEndpointTest {

    @Inject
    private HeaviestWeightEndpoint heaviestWeightEndpoint;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new HeaviestWeightModule()).with(new HeaviestWeightTestModule(),
                new RetrofitTestModule())).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<Response<ResponseBody>> testObserver = heaviestWeightEndpoint.login("test", "test", "test", "test")
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        assertEquals(200, testObserver.values().get(0).code());
        assertEquals("OK", testObserver.values().get(0).message());
    }

}

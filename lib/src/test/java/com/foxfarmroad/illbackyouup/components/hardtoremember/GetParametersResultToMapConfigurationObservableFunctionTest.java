package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.apache.commons.configuration2.MapConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import software.amazon.awssdk.services.ssm.model.GetParametersResponse;
import software.amazon.awssdk.services.ssm.model.Parameter;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class GetParametersResultToMapConfigurationObservableFunctionTest {

    @Mock
    private GetParametersResponse getParametersResult;

    private List<Parameter> parameterList;

    @Inject
    private GetParametersResultToMapConfigurationObservableFunction getParametersResultToMapConfigurationObservableFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.EMPTY_MODULE).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Parameter parameter = Parameter.builder()
                .name("name")
                .value("value")
                .build();
        parameterList = new ArrayList<>();
        parameterList.add(parameter);
        when(getParametersResult.parameters()).thenReturn(parameterList);

        Map<String, Object> map = new HashMap<>();
        map.put("name", "value");
        MapConfiguration mapConfiguration = new MapConfiguration(map);

        TestObserver<MapConfiguration> testObserver = Observable.just(getParametersResult)
                .flatMap(getParametersResultToMapConfigurationObservableFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        MapConfiguration actualMapConfiguration = testObserver.values().get(0);
        assertEquals(mapConfiguration.size(), actualMapConfiguration.size());
        assertEquals(mapConfiguration.getMap(), actualMapConfiguration.getMap());
    }

    @Test
    public void verifyRight2() throws Exception {
        Parameter parameter1 = Parameter.builder()
                .name("name1")
                .value("value1")
                .build();
        Parameter parameter2 = Parameter.builder()
                .name("name2")
                .value("value2")
                .build();
        parameterList = new ArrayList<>();
        parameterList.add(parameter1);
        parameterList.add(parameter2);
        when(getParametersResult.parameters()).thenReturn(parameterList);

        Map<String, Object> map = new HashMap<>();
        map.put("name1", "value1");
        map.put("name2", "value2");
        MapConfiguration mapConfiguration = new MapConfiguration(map);

        TestObserver<MapConfiguration> testObserver = Observable.just(getParametersResult)
                .flatMap(getParametersResultToMapConfigurationObservableFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        MapConfiguration actualMapConfiguration = testObserver.values().get(0);
        assertEquals(mapConfiguration.size(), actualMapConfiguration.size());
        assertEquals(mapConfiguration.getMap(), actualMapConfiguration.getMap());
    }

    @Test
    public void verifyRight3() throws Exception {
        parameterList = new ArrayList<>();
        when(getParametersResult.parameters()).thenReturn(parameterList);

        Map<String, Object> map = new HashMap<>();
        MapConfiguration mapConfiguration = new MapConfiguration(map);

        TestObserver<MapConfiguration> testObserver = Observable.just(getParametersResult)
                .flatMap(getParametersResultToMapConfigurationObservableFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        MapConfiguration actualMapConfiguration = testObserver.values().get(0);
        assertEquals(mapConfiguration.size(), actualMapConfiguration.size());
        assertEquals(mapConfiguration.getMap(), actualMapConfiguration.getMap());
    }

}

package com.foxfarmroad.illbackyouup.components.yoursteps;

import com.foxfarmroad.RetrofitTestModule;
import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.observers.TestObserver;
import okhttp3.Headers;
import okhttp3.ResponseBody;
import okhttp3.internal.http.RealResponseBody;
import okio.Buffer;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

@Category(UnitTest.class)
public class YourStepsEndpointTest {

    @Inject
    private YourStepsEndpoint yourStepsEndpoint;

    @Bind
    private ResponseBody responseBody = new RealResponseBody(Headers.of(), new Buffer());

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new YourStepsModule()).with(new YourStepsTestModule(),
                new RetrofitTestModule(), BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<Document> testObserver = yourStepsEndpoint.document("test")
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
    }

}

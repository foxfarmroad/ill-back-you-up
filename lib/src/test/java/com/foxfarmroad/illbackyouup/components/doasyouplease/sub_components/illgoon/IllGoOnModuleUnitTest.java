package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class IllGoOnModuleUnitTest {

    @Inject
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Inject
    @IRememberThinking
    private Function<String, Observable<String>> iRememberThinkingFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new IllGoOnModule(), new WouldYouLikeModule()).injectMembers(this);
    }
}

package com.foxfarmroad.illbackyouup.components.doasyouplease;

import com.foxfarmroad.illbackyouup.io.Do;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.junit.Test;

import javax.inject.Inject;
import java.util.List;

public class DoAsYouPleaseModuleUnitTest {

    @Inject
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Inject
    @StyleAttr
    private String attrName;

    @Inject
    @LocStyle
    private String locStyle;

    @Inject
    private Function<Node, Observable<Do>> iveKnownFunction;

    @Inject
    @ButLikeThatTouch
    private Function<YourStepsBean, Observable<List<String>>> butLikeThatTouchFunction;

    @Inject
    @IllGoOn
    private Function<YourStepsBean, Observable<List<String>>> illGoOnFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new DoAsYouPleaseModule()).injectMembers(this);
    }
}

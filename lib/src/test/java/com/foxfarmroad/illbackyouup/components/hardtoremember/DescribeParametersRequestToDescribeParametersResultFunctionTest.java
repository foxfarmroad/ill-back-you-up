package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import software.amazon.awssdk.services.ssm.SSMClient;
import software.amazon.awssdk.services.ssm.model.DescribeParametersRequest;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;

import javax.inject.Inject;

import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class DescribeParametersRequestToDescribeParametersResultFunctionTest {

    @Mock
    @Bind
    private SSMClient ssm;

    @Mock
    private DescribeParametersRequest describeParametersRequest;

    @Mock
    private DescribeParametersResponse describeParametersResponse;

    @Inject
    private DescribeParametersRequestToDescribeParametersResultFunction describeParametersRequestDescribeParametersResultFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new HardToRememberModule()).with(BoundFieldModule.of(this)))
                .injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        when(ssm.describeParameters(describeParametersRequest)).thenReturn(describeParametersResponse);

        TestObserver<DescribeParametersResponse> testObserver = Observable.just(describeParametersRequest)
                .flatMap(describeParametersRequestDescribeParametersResultFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(describeParametersResponse);
    }

}

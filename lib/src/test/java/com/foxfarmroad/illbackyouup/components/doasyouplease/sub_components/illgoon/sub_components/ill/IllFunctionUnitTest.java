package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.OldNoteStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.WeHaveDanced;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class IllFunctionUnitTest {

    @Inject
    @OldNoteStyle
    private String noteStyle;

    @Mock
    @Bind
    @WeHaveDanced
    private Function<Node, Observable<Pair<Integer, String>>> weHaveDancedFunction;

    @Inject
    private IllFunction illFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new IllModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        Node node = mock(Node.class);
        when(node.nodeName()).thenReturn("div");
        Node node2 = mock(Node.class);
        when(node2.nodeName()).thenReturn("div");
        when(node2.hasAttr(anyString())).thenReturn(Boolean.TRUE);
        when(node2.attr(anyString())).thenReturn(noteStyle);
        Node node1 = mock(Node.class);
        when(node1.nodeName()).thenReturn("div");
        when(node1.hasAttr(anyString())).thenReturn(Boolean.TRUE);
        when(node1.attr(anyString())).thenReturn(noteStyle);

        List<Node> nodeList = new ArrayList<>();
        nodeList.add(node2);
        nodeList.add(node1);
        when(node.childNodes()).thenReturn(nodeList);

        when(weHaveDancedFunction.apply(node2)).thenReturn(Observable.just(ImmutablePair.of(2, "Test2")));
        when(weHaveDancedFunction.apply(node1)).thenReturn(Observable.just(ImmutablePair.of(1, "Test1")));

        ArrayList<String> list = new ArrayList<>();
        list.add("Test1");
        list.add("Test2");

        TestObserver<List<String>> testObserver = Observable.just(Observable.just(node))
                .flatMap(illFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(list);
        verify(weHaveDancedFunction, times(2)).apply(any());
        verifyNoMoreInteractions(weHaveDancedFunction);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Node;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class WeHaveDancedModuleUnitTest {

    @Inject
    private Function<Node, Observable<Integer>> positionFunction;

    @Inject
    private BiConsumer<StringBuilder, Node> inTheRiskConsumer;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new WeHaveDancedModule(), new WouldYouLikeModule()).injectMembers(this);
    }

}

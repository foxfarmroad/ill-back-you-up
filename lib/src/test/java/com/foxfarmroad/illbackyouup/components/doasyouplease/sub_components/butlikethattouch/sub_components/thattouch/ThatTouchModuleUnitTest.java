package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.LocStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.StyleAttr;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Predicate;
import org.jsoup.nodes.Node;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.List;

@Category(UnitTest.class)
public class ThatTouchModuleUnitTest {

    @Inject
    @StyleAttr
    private String attrName;

    @Inject
    @LocStyle
    private String locStyle;

    @Inject
    private Predicate<Node> thatPredicate;

    @Inject
    private BiFunction<List<String>, Node, List<String>> touchBiFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new ThatTouchModule(), new WouldYouLikeModule()).injectMembers(this);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.RunEndpoint;
import com.google.inject.Guice;
import org.junit.Test;

import javax.inject.Inject;

public class WeRunModuleUnitTest {

    @Inject
    private RunEndpoint runEndpoint;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new WeRunModule()).injectMembers(this);
    }
}

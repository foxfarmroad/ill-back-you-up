package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.walk;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class WalkFunctionUnitTest {

    @Mock
    @Bind
    @Walk
    private Function<String, Observable<String>> weFunction;

    @Inject
    private WalkFunction walkFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new WalkModule()).with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Element element = new Element("tr");
        Element one = new Element("td").attr("align", "center");
        Element date = new Element("a").appendText("date");
        one.appendChild(date);
        Element two = new Element("td").attr("align", "center");
        Element three = new Element("td").attr("align", "center");
        Element four = new Element("td").attr("align", "center");
        Element five = new Element("td").attr("align", "center");
        Element six = new Element("td").attr("align", "center");
        Element seven = new Element("td").attr("align", "center");
        Element eight = new Element("td").attr("align", "left");
        Element table = new Element("table");
        Element tableBody = new Element("tbody");
        Element tableRow = new Element("tr");
        Element tableCell1 = new Element("td");
        Element a = new Element("a").appendText("venue");
        tableCell1.appendChild(a);
        tableRow.appendChild(tableCell1);
        Element tableCell2 = new Element("td").attr("align", "right");
        Element span = new Element("span").appendText("showDetail");
        tableCell2.appendChild(span);
        tableRow.appendChild(tableCell2);
        tableBody.appendChild(tableRow);
        table.appendChild(tableBody);
        eight.appendChild(table);
        Element nine = new Element("td").attr("align", "right");
        Element location = new Element("span").appendText("location");
        nine.appendChild(location);
        Element ten = new Element("td").attr("align", "right");

        element.appendChild(one);
        element.appendChild(two);
        element.appendChild(three);
        element.appendChild(four);
        element.appendChild(five);
        element.appendChild(six);
        element.appendChild(seven);
        element.appendChild(eight);
        element.appendChild(nine);
        element.appendChild(ten);

        when(weFunction.apply("date")).thenReturn(Observable.just("date"));
        when(weFunction.apply("venue")).thenReturn(Observable.just("venue"));
        when(weFunction.apply("showDetail")).thenReturn(Observable.just("showDetail"));
        when(weFunction.apply("location")).thenReturn(Observable.just("location"));

        TestObserver<WalkBean> testObserver = Observable.just(element)
                .flatMap(walkFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertSubscribed()
                .assertNoErrors()
                .assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("date", testObserver.values().get(0).getDate());
        assertEquals("venue", testObserver.values().get(0).getVenue());
        assertEquals("showDetail", testObserver.values().get(0).getShowDetail());
        assertEquals("location", testObserver.values().get(0).getLocation());
    }

    @Test
    public void verifyB1() throws Exception {
        Element element = new Element("tr");
        Element one = new Element("td").attr("align", "center");
        Element date = new Element("a").appendText("date");
        one.appendChild(date);
        Element three = new Element("td").attr("align", "center");
        Element four = new Element("td").attr("align", "center");
        Element five = new Element("td").attr("align", "center");
        Element six = new Element("td").attr("align", "center");
        Element seven = new Element("td").attr("align", "center");
        Element eight = new Element("td").attr("align", "left");
        Element table = new Element("table");
        Element tableBody = new Element("tbody");
        Element tableRow = new Element("tr");
        Element tableCell1 = new Element("td");
        Element a = new Element("a").appendText("venue");
        tableCell1.appendChild(a);
        tableRow.appendChild(tableCell1);
        Element tableCell2 = new Element("td").attr("align", "right");
        Element span = new Element("span").appendText("showDetail");
        tableCell2.appendChild(span);
        tableRow.appendChild(tableCell2);
        tableBody.appendChild(tableRow);
        table.appendChild(tableBody);
        eight.appendChild(table);
        Element nine = new Element("td").attr("align", "right");
        Element location = new Element("span").appendText("location");
        nine.appendChild(location);
        Element ten = new Element("td").attr("align", "right");

        element.appendChild(one);
        element.appendChild(three);
        element.appendChild(four);
        element.appendChild(five);
        element.appendChild(six);
        element.appendChild(seven);
        element.appendChild(eight);
        element.appendChild(nine);
        element.appendChild(ten);

        TestObserver<WalkBean> testObserver = Observable.just(element)
                .flatMap(walkFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

    @Test
    public void verifyB2() throws Exception {
        Element element = new Element("tr");
        Element one = new Element("tr");
        Element two = new Element("tr");
        Element three = new Element("tr");
        Element four = new Element("tr");
        Element five = new Element("tr");
        Element six = new Element("tr");
        Element seven = new Element("tr");
        Element eight = new Element("tr");
        Element nine = new Element("tr");
        Element ten = new Element("tr");

        element.appendChild(one);
        element.appendChild(two);
        element.appendChild(three);
        element.appendChild(four);
        element.appendChild(five);
        element.appendChild(six);
        element.appendChild(seven);
        element.appendChild(eight);
        element.appendChild(nine);
        element.appendChild(ten);

        TestObserver<WalkBean> testObserver = Observable.just(element)
                .flatMap(walkFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

    @Test
    public void verifyB3() throws Exception {
        Element element = new Element("tr");
        Element one = new Element("td").attr("align", "left");
        Element date = new Element("a").appendText("date");
        one.appendChild(date);
        Element two = new Element("td").attr("align", "left");
        Element three = new Element("td").attr("align", "left");
        Element four = new Element("td").attr("align", "left");
        Element five = new Element("td").attr("align", "left");
        Element six = new Element("td").attr("align", "left");
        Element seven = new Element("td").attr("align", "left");
        Element eight = new Element("td").attr("align", "left");
        Element table = new Element("table");
        Element tableBody = new Element("tbody");
        Element tableRow = new Element("tr");
        Element tableCell1 = new Element("td");
        Element a = new Element("a").appendText("venue");
        tableCell1.appendChild(a);
        tableRow.appendChild(tableCell1);
        Element tableCell2 = new Element("td").attr("align", "right");
        Element span = new Element("span").appendText("showDetail");
        tableCell2.appendChild(span);
        tableRow.appendChild(tableCell2);
        tableBody.appendChild(tableRow);
        table.appendChild(tableBody);
        eight.appendChild(table);
        Element nine = new Element("td").attr("align", "right");
        Element location = new Element("span").appendText("location");
        nine.appendChild(location);
        Element ten = new Element("td").attr("align", "right");

        element.appendChild(one);
        element.appendChild(two);
        element.appendChild(three);
        element.appendChild(four);
        element.appendChild(five);
        element.appendChild(six);
        element.appendChild(seven);
        element.appendChild(eight);
        element.appendChild(nine);
        element.appendChild(ten);

        when(weFunction.apply("location")).thenReturn(Observable.just("location"));

        TestObserver<WalkBean> testObserver = Observable.just(element)
                .flatMap(walkFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

}

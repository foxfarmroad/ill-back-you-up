package com.foxfarmroad.illbackyouup.components.dowhatyouwill;

import com.foxfarmroad.ManualTest;
import com.foxfarmroad.illbackyouup.components.heaviestweight.FormAction;
import com.foxfarmroad.illbackyouup.components.heaviestweight.Heaviest;
import com.foxfarmroad.illbackyouup.components.heaviestweight.Password;
import com.foxfarmroad.illbackyouup.components.heaviestweight.Username;
import com.foxfarmroad.illbackyouup.components.heaviestweight.Weight;
import com.foxfarmroad.illbackyouup.components.yoursteps.Steps;
import com.foxfarmroad.illbackyouup.components.yoursteps.Your;
import com.foxfarmroad.illbackyouup.io.DoAsYouPlease;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(ManualTest.class)
public class DoWhatYouWillManualTest {

    @Bind
    @Heaviest
    private String heaviest = "https://whsec1.davematthewsband.com";

    @Bind
    @Weight
    private String weight = "login.asp";

    @Bind
    @Username
    private String username = "fateman";

    @Bind
    @Password
    private String password = "nintendo";

    @Bind
    @FormAction
    private String formAction = "login";

    @Bind
    @Your
    private String your = "https://whsec1.davematthewsband.com";

    @Bind
    @Steps
    private String steps = "backstage.asp";

    @Inject
    private DoWhatYouWillCallable doWhatYouWillCallable;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new DoWhatYouWillModule(), BoundFieldModule.of(this)).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<DoAsYouPlease> testObserver = Observable.fromCallable(doWhatYouWillCallable)
                .blockingSingle()
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        System.out.println(testObserver.values().get(0));
    }
}

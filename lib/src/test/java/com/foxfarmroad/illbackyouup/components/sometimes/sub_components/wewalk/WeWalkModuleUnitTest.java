package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk;

import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Element;
import org.junit.Test;

import javax.inject.Inject;

public class WeWalkModuleUnitTest {

    @Inject
    private Function<Element, Observable<WalkBean>> walkFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new WeWalkModule(), new WeWalkTestModule()).injectMembers(this);
    }
}

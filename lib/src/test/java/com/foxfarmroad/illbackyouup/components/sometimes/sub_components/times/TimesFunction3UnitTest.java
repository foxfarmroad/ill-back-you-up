package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.times;

import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.google.inject.Guice;
import com.google.inject.Inject;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;

public class TimesFunction3UnitTest {

    @Inject
    @TimeStart
    private Integer showStartHour;

    @Inject
    @TimeOffset
    private Integer offsetHour;

    @Inject
    private TimesFunction3 timesFunction3;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new TimesTestModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        DateTime now = new DateTime()
                .withZone(DateTimeZone.UTC)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        DateTime showDate = now;
        now = now.withHourOfDay(showStartHour);
        now = now.minusHours(1);

        TestObserver<Long> testObserver = Observable.zip(
                        Observable.just(showDate),
                        Observable.just(now),
                        Observable.just(0),
                        timesFunction3
                ).blockingSingle(Observable.empty())
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(showDate.withHourOfDay(showStartHour).plusSeconds(0).getMillis());
    }

    @Test
    public void verifyRight2() throws Exception {
        DateTime now = new DateTime()
                .withZone(DateTimeZone.UTC)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        DateTime showDate = now;
        now = now.withHourOfDay(showStartHour);
        now = now.minusHours(offsetHour);
        now = now.plusMinutes(1);

        TestObserver<Long> testObserver = Observable.zip(
                Observable.just(showDate),
                Observable.just(now),
                Observable.just(0),
                timesFunction3
        ).blockingSingle(Observable.empty())
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(showDate.withHourOfDay(showStartHour).plusSeconds(0).getMillis());
    }

    @Test
    public void verifyB1() throws Exception {
        DateTime now = new DateTime()
                .withZone(DateTimeZone.UTC)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        DateTime showDate = now;
        now = now.withHourOfDay(showStartHour);
        now = now.minusHours(offsetHour);
        now = now.minusMinutes(1);

        TestObserver<Long> testObserver = Observable.zip(
                Observable.just(showDate),
                Observable.just(now),
                Observable.just(0),
                timesFunction3
        ).blockingSingle(Observable.empty())
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult();
    }

    @Test
    public void verifyB2() throws Exception {
        DateTime now = new DateTime()
                .withZone(DateTimeZone.UTC)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        DateTime showDate = now;
        now = now.withHourOfDay(showStartHour);
        now = now.plusHours(offsetHour);
        now = now.plusMinutes(1);

        TestObserver<Long> testObserver = Observable.zip(
                Observable.just(showDate),
                Observable.just(now),
                Observable.just(0),
                timesFunction3
        ).blockingSingle(Observable.empty())
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(showDate.withHourOfDay(showStartHour).plusSeconds(0).getMillis());
    }

}

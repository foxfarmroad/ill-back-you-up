package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.we.WeFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.we.WeModule;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * <p>
 * </p>
 */
public class WeWalkTestModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(WeFunction.class);

        install(new WeModule());

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).to(WeFunction.class);
    }

    @Provides
    Document provideDmbAlmanacDocument() {
        return Jsoup.parseBodyFragment("<html><body><table class=\"newslogcell\" border='0' bgcolor=\"#102142\" cellpadding=\"0\" cellspacing=\"0\" width=\"75%\">\n" +
                "   <tr class='darkheadercell'>\n" +
                "      <td align=\"center\"><span class=\"reportlink\">Date</span></a></td>\n" +
                "      <td align=\"center\" style='padding:0px 5px 0px 5px;'>Rarity&nbsp;<a href=\"javascript:void(0);\" onmouseover=\"return overlib('NOTE: Rarity indexes are based on shows within the same tour. Ranks within the tours are shown in parenthesis. <br><br><img src=\\'./images/showinfo.gif\\'> The Song Rarity index represents the number of shows between song appearances given the average number of tour appearances for all songs played during a show. Shows with a higher index tend to contain rarer songs. <br><br>The index &#40;X&#41; is defined as follows: <i>The average song in this set was played once every &#40;X&#41; shows in this tour</i>. <br>It is calculated by taking the average of the number of times each song was played during a tour, dividing it by the number of shows during the tour, and taking the inverse to get the 1 out of every X value.', FGCOLOR, '#808080', BGCOLOR, '#C0C0C0', FGBACKGROUND, '/images/bak.gif', TEXTCOLOR, '#FFC449', WIDTH, '200');\" onmouseout=\"return nd();\"><img src='/images/showinfo.gif'></a> </td>\n" +
                "      <td colspan='5' align=\"center\" nowrap style='padding:0px 5px 0px 5px;'>Info</td>\n" +
                "      <td align=\"left\" style='padding:0px 5px 0px 5px;'>\n" +
                "         <table border='0' cellpadding='0' cellspacing='0' width='100%'>\n" +
                "            <tr>\n" +
                "               <td valign='top' nowrap>\n" +
                "                  <div class='reportlink'>Venue</div>\n" +
                "               </td>\n" +
                "               <td align='right' valign='top' nowrap>\n" +
                "                  <div class='reportlink'>Show Detail</div>\n" +
                "               </td>\n" +
                "            </tr>\n" +
                "         </table>\n" +
                "      </td>\n" +
                "      <td align=\"right\"><span class=\"reportlink\">Location</span></td>\n" +
                "      <td align=\"right\" nowrap style='padding:0px 5px 0px 5px;'><span class=\"reportlink\">Opening Act</span></td>\n" +
                "   </tr>\n" +
                "   <tr bgcolor=\"#1A2B4C\">\n" +
                "      <td align=\"center\" class='showrowcell'><a  onmouseover=\"return overlib('Friday', FGCOLOR, '#808080', BGCOLOR, '#C0C0C0', FGBACKGROUND, '/images/bak.gif', TEXTCOLOR, '#FFC449', WIDTH, '80');\" onmouseout=\"return nd();\"  href=\"./TourShowSet.aspx?id=453058207&tid=127&where=2015\">&nbsp;01.16.15</a></td>\n" +
                "      <td align=\"center\" class='showrowcell'></td>\n" +
                "      <td align=\"center\" class='showrowcell'></td>\n" +
                "      <td align=\"center\" class='showrowcell'><a target='_blank' href=\"http://dmbalmanac.com/TourSetScan.aspx?sid=453058207&tid=127\"><img src=\"./images/setlist.gif\"></a>&nbsp;</td>\n" +
                "      <td align=\"center\" class='showrowcell'><a target='_blank' href=\"http://dmbalmanac.com/TourMemorabilia.aspx?sid=453058207&tid=127\"><img src=\"./images/ticketstub.gif\"></a>&nbsp;</td>\n" +
                "      <td align=\"center\" class='showrowcell'><a target='_blank' href=\"http://dmbalmanac.com/TourMemorabilia.aspx?sid=453058207&tid=127\"><img src=\"./images/poster.gif\"></a>&nbsp;</td>\n" +
                "      <td align=\"center\" class='showrowcell'></td>\n" +
                "      <td align=\"left\" class='showrowcell'>\n" +
                "         <table border='0' cellpadding='0' cellspacing='0' width='100%'>\n" +
                "            <tr>\n" +
                "               <td valign='top' nowrap><a class='lightorange' style='font-size:10px;' target=\"_blank\" href=\"./VenueStats.aspx?vid=2908\">Paramount Theatre</a></td>\n" +
                "               <td align='right' valign='top' nowrap><span class=\"detailgreen\">&nbsp;&nbsp;(Dave and Tim)</span></td>\n" +
                "            </tr>\n" +
                "         </table>\n" +
                "      </td>\n" +
                "      <td align=\"right\" class='showrowcell'><span class=\"detail3\">Oakland, CA</span></td>\n" +
                "      <td align=\"right\" class='showrowcell'></td>\n" +
                "   </tr>\n" +
                "   <tr bgcolor=\"#102142\">\n" +
                "      <td align=\"center\" class='showrowcell'><a  onmouseover=\"return overlib('Saturday', FGCOLOR, '#808080', BGCOLOR, '#C0C0C0', FGBACKGROUND, '/images/bak.gif', TEXTCOLOR, '#FFC449', WIDTH, '80');\" onmouseout=\"return nd();\"  href=\"./TourShowSet.aspx?id=453058208&tid=127&where=2015\">&nbsp;01.17.15</a></td>\n" +
                "      <td align=\"center\" class='showrowcell'></td>\n" +
                "      <td align=\"center\" class='showrowcell'></td>\n" +
                "      <td align=\"center\" class='showrowcell'><a target='_blank' href=\"http://dmbalmanac.com/TourSetScan.aspx?sid=453058208&tid=127\"><img src=\"./images/setlist.gif\"></a>&nbsp;</td>\n" +
                "      <td align=\"center\" class='showrowcell'><a target='_blank' href=\"http://dmbalmanac.com/TourMemorabilia.aspx?sid=453058208&tid=127\"><img src=\"./images/ticketstub.gif\"></a>&nbsp;</td>\n" +
                "      <td align=\"center\" class='showrowcell'><a target='_blank' href=\"http://dmbalmanac.com/TourMemorabilia.aspx?sid=453058208&tid=127\"><img src=\"./images/poster.gif\"></a>&nbsp;</td>\n" +
                "      <td align=\"center\" class='showrowcell'></td>\n" +
                "      <td align=\"left\" class='showrowcell'>\n" +
                "         <table border='0' cellpadding='0' cellspacing='0' width='100%'>\n" +
                "            <tr>\n" +
                "               <td valign='top' nowrap><a class='lightorange' style='font-size:10px;' target=\"_blank\" href=\"./VenueStats.aspx?vid=2908\">Paramount Theatre</a></td>\n" +
                "               <td align='right' valign='top' nowrap><span class=\"detailgreen\">&nbsp;&nbsp;(Dave and Tim)</span></td>\n" +
                "            </tr>\n" +
                "         </table>\n" +
                "      </td>\n" +
                "      <td align=\"right\" class='showrowcell'><span class=\"detail3\">Oakland, CA</span></td>\n" +
                "      <td align=\"right\" class='showrowcell'></td>\n" +
                "   </tr>" +
                "</table></body></html>");
    }
}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.RetrofitTestModule;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastModule;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastTestModule;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

/**
 * <p>
 * </p>
 */
@Category(ComponentTest.class)
public class AwayComponentTest {

    @Inject
    private AwayBiFunction awayBiFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.override(new HowFastModule()).with(new HowFastTestModule(),
                new RetrofitTestModule())).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(1.0f);
        latLngBean.setLng(1.0f);

        TestObserver<Integer> testObserver = Observable.zip(
                        Observable.just(latLngBean),
                        Observable.just(new DateTime()),
                        awayBiFunction
                )
                .blockingSingle()
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
    }

}

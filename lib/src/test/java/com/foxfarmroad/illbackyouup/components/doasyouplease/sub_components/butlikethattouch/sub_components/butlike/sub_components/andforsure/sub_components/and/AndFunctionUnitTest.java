package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.and;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class AndFunctionUnitTest {

    @Mock
    @Bind
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Inject
    private AndFunction andFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new TheTouchOfYouModule(), new AndModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        when(theTouchOfYouFunction.apply(any())).thenReturn(Observable.just("Don't Drink the Water"));

        TestObserver<String> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:606;\">18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;\n<img src=\"images/setlists/NewArrow.png\" hspace=\"4\" /></div>").body().childNode(0))
                .flatMap(andFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals("Don't Drink the Water ->", testObserver.values().get(0));
        verify(theTouchOfYouFunction, times(1)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

    @Test
    public void verifyB1() throws Exception {
        Node parentNode = mock(Node.class);
        when(parentNode.childNodes()).thenReturn(new ArrayList<>());

        TestObserver<String> testObserver = Observable.just(parentNode)
                .flatMap(andFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
        verify(theTouchOfYouFunction, times(0)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

    @Test
    public void verifyB2() throws Exception {
        Element element = mock(Element.class);
        Node parentNode = mock(Node.class);
        List<Node> children = new ArrayList<>();
        children.add(element);
        when(parentNode.childNodes()).thenReturn(children);

        TestObserver<String> testObserver = Observable.just(parentNode)
                .flatMap(andFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
        verify(theTouchOfYouFunction, times(0)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

    @Test
    public void verifyB3() throws Exception {
        TextNode element = mock(TextNode.class);
        when(element.text()).thenReturn("");
        Node parentNode = mock(Node.class);
        List<Node> children = new ArrayList<>();
        children.add(element);
        when(parentNode.childNodes()).thenReturn(children);

        TestObserver<String> testObserver = Observable.just(parentNode)
                .flatMap(andFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
        verify(theTouchOfYouFunction, times(0)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

    @Test
    public void verifyB4() throws Exception {
        TextNode element = mock(TextNode.class);
        when(element.text()).thenReturn("test");
        Node parentNode = mock(Node.class);
        List<Node> children = new ArrayList<>();
        children.add(element);
        when(parentNode.childNodes()).thenReturn(children);

        when(theTouchOfYouFunction.apply(any())).thenReturn(Observable.empty());

        TestObserver<String> testObserver = Observable.just(parentNode)
                .flatMap(andFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
        verify(theTouchOfYouFunction, times(1)).apply(any());
        verifyNoMoreInteractions(theTouchOfYouFunction);
    }

}

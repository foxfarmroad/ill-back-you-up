package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import software.amazon.awssdk.services.ssm.SSMClient;
import software.amazon.awssdk.services.ssm.model.GetParametersRequest;
import software.amazon.awssdk.services.ssm.model.GetParametersResponse;

import javax.inject.Inject;

import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class GetParametersRequestToGetParametersResultFunctionTest {

    @Mock
    @Bind
    private SSMClient ssm;

    @Mock
    private GetParametersRequest getParametersRequest;

    @Mock
    private GetParametersResponse getParametersResponse;

    @Inject
    private GetParametersRequestToGetParametersResultFunction getParametersRequestGetParametersResultFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new HardToRememberModule()).with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        when(ssm.getParameters(getParametersRequest)).thenReturn(getParametersResponse);

        TestObserver<GetParametersResponse> testObserver = Observable.just(getParametersRequest)
                .flatMap(getParametersRequestGetParametersResultFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(getParametersResponse);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast;

import com.foxfarmroad.illbackyouup.io.HowFastBean;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class HowFastTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(How.class).to("http://localhost");
        bindConstant().annotatedWith(Fast.class).to("test");
    }

    @Provides
    MockRetrofit provideMockRetrofit(@How Retrofit retrofit, NetworkBehavior networkBehavior) {
        return new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();
    }

    @Provides
    HowFastEndpoint provideHowFastEndpoint(MockRetrofit mockRetrofit, HowFastBean howFastBean) {
        BehaviorDelegate<HowFastEndpoint> delegate = mockRetrofit.create(HowFastEndpoint.class);
        return new HowFastMockEndpoint(delegate, howFastBean);
    }

    @Provides
    HowFastBean provideHowFastBean() {
        HowFastBean howFastBean = new HowFastBean();
        howFastBean.setDstOffset(1);
        howFastBean.setRawOffset(1);

        return howFastBean;
    }

}

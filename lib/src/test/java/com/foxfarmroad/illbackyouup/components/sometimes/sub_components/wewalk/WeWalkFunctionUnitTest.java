package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import java.util.NoSuchElementException;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class WeWalkFunctionUnitTest {

    @Mock
    @Bind
    private Function<Element, Observable<WalkBean>> walkFunction;

    @Inject
    private WeWalkFunction weWalkFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new WeWalkModule(), new WeWalkTestModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Document document = mock(Document.class);

        Elements elements = new Elements();
        Element element = new Element("table");
        elements.add(element);

        Element tbody = new Element("tbody");
        element.appendChild(tbody);

        Element row = new Element("tr");
        tbody.appendChild(row);

        when(document.getElementsByAttributeValue("class", "newslogcell")).thenReturn(elements);

        YourStepsBean yourStepsBean = new YourStepsBean(document);

        WalkBean walkBean = mock(WalkBean.class);

        when(walkFunction.apply(any())).thenReturn(Observable.just(walkBean));

        TestObserver<WalkBean> testObserver = Observable.just(yourStepsBean)
                .flatMap(weWalkFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(walkBean);
        verify(walkFunction, times(1)).apply(any());
        verifyNoMoreInteractions(walkFunction);
    }

    @Test
    public void verifyRight2() throws Exception {
        Document document = mock(Document.class);

        Elements elements = new Elements();
        Element element = new Element("table");
        elements.add(element);

        Element tbody = new Element("tbody");
        element.appendChild(tbody);

        Element row1 = new Element("tr");
        Element row2 = new Element("tr").attr("class", "");
        Element row3 = new Element("tr");
        tbody.appendChild(row1);
        tbody.appendChild(row2);
        tbody.appendChild(row3);

        when(document.getElementsByAttributeValue("class", "newslogcell")).thenReturn(elements);

        YourStepsBean yourStepsBean = new YourStepsBean(document);

        WalkBean walkBean = mock(WalkBean.class);

        when(walkFunction.apply(any())).thenReturn(Observable.just(walkBean));

        TestObserver<WalkBean> testObserver = Observable.just(yourStepsBean)
                .flatMap(weWalkFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(walkBean, walkBean);
        verify(walkFunction, times(2)).apply(any());
        verifyNoMoreInteractions(walkFunction);
    }

    @Test
    public void verifyE1() throws Exception {
        Document document = mock(Document.class);

        Elements elements = new Elements();

        when(document.getElementsByAttributeValue("class", "newslogcell")).thenReturn(elements);

        YourStepsBean yourStepsBean = new YourStepsBean(document);

        WalkBean walkBean = mock(WalkBean.class);

        when(walkFunction.apply(any())).thenReturn(Observable.just(walkBean));

        TestObserver<WalkBean> testObserver = Observable.just(yourStepsBean)
                .flatMap(weWalkFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(NoSuchElementException.class);
        verify(walkFunction, times(0)).apply(any());
        verifyNoMoreInteractions(walkFunction);
    }

}

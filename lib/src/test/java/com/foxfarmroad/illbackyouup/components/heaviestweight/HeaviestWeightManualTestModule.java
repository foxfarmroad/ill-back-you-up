package com.foxfarmroad.illbackyouup.components.heaviestweight;

import com.foxfarmroad.illbackyouup.retrofit.CookieStore;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import okhttp3.CookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class HeaviestWeightManualTestModule extends AbstractModule {

    @Override
    protected void configure() {
        // TODO Config for these in prod
        bindConstant().annotatedWith(Heaviest.class).to("https://whsec1.davematthewsband.com");
        bindConstant().annotatedWith(Weight.class).to("login.asp");
        bindConstant().annotatedWith(Username.class).to("fateman");
        bindConstant().annotatedWith(Password.class).to("nintendo");
        bindConstant().annotatedWith(FormAction.class).to("login");

        bind(CookieJar.class).toInstance(new CookieStore());
    }

    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        // TODO Config for the level
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, CookieJar cookieJar) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .cookieJar(cookieJar)
                .build();
    }

}

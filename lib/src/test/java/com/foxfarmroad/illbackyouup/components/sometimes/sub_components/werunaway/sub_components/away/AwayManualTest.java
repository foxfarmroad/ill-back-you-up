package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.Fast;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.How;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastModule;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

/**
 * <p>
 * </p>
 */
@Category(ComponentTest.class)
public class AwayManualTest {

    @Bind
    @How
    private String how = "https://maps.googleapis.com";

    @Bind
    @Fast
    private String fast = "maps/api/timezone/json";

    @Inject
    private AwayBiFunction awayBiFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.override(new HowFastModule()).with(BoundFieldModule.of(this)))
                .injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(45.5047654f);
        latLngBean.setLng(-122.8225272f);

        TestObserver<Integer> testObserver = Observable.zip(
                        Observable.just(latLngBean),
                        Observable.just(new DateTime()),
                        awayBiFunction
                )
                .blockingSingle()
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);

        System.out.println(testObserver.values());
    }

    @Test
    public void verifyRight2() throws Exception {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(51.178882f);
        latLngBean.setLng(-1.8284037f);

        TestObserver<Integer> testObserver = Observable.zip(
                Observable.just(latLngBean),
                Observable.just(new DateTime()),
                awayBiFunction
        )
                .blockingSingle()
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);

        System.out.println(testObserver.values());
    }

    @Test
    public void verifyRight3() throws Exception {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(-33.8567844f);
        latLngBean.setLng(151.213108f);

        TestObserver<Integer> testObserver = Observable.zip(
                Observable.just(latLngBean),
                Observable.just(new DateTime()),
                awayBiFunction
        )
                .blockingSingle()
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);

        System.out.println(testObserver.values());
    }

}

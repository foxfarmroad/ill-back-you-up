package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.List;

@Category(UnitTest.class)
public class ButLikeThatTouchModuleUnitTest {

    @Inject
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Inject
    private Function<YourStepsBean, Observable<Boolean>> foreverOnlyKnowingFunction;

    @Inject
    @ButLike
    private Function<Node, Observable<List<String>>> butLikeFunction;

    @Inject
    @ThatTouch
    private Function<Node, Observable<Node>> thatTouchFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new ButLikeThatTouchModule(), new WouldYouLikeModule()).injectMembers(this);
    }

}

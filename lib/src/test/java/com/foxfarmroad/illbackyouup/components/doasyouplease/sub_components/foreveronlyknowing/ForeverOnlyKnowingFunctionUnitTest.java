package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class ForeverOnlyKnowingFunctionUnitTest {

    @Mock
    @Bind
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Mock
    @Bind
    private BiFunction<Boolean, List<Node>, Boolean> foreverBiFunction;

    @Inject
    private ForeverOnlyKnowingFunction foreverOnlyKnowingFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new ForeverOnlyKnowingModule(), new ForeverOnlyKnowingTestModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        YourStepsBean yourStepsBean = mock(YourStepsBean.class);
        Document document = mock(Document.class);

        when(yourStepsBean.getBody()).thenReturn(document);
        when(butIKnowFunction.apply(document)).thenReturn(Observable.just(new DataNode("", "")));
        when(foreverBiFunction.apply(any(), any())).thenReturn(Boolean.TRUE);

        TestObserver<Boolean> testObserver = Observable.just(yourStepsBean)
                .flatMap(foreverOnlyKnowingFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(Boolean.TRUE);
    }

    @Test
    public void verifyRight2() throws Exception {
        YourStepsBean yourStepsBean = mock(YourStepsBean.class);
        Document document = mock(Document.class);

        when(yourStepsBean.getBody()).thenReturn(document);
        when(butIKnowFunction.apply(document)).thenReturn(Observable.just(new DataNode("", "")));
        when(foreverBiFunction.apply(any(), any())).thenReturn(Boolean.FALSE);

        TestObserver<Boolean> testObserver = Observable.just(yourStepsBean)
                .flatMap(foreverOnlyKnowingFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(Boolean.FALSE);
    }

}

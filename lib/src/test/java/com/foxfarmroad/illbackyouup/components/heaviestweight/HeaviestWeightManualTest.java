package com.foxfarmroad.illbackyouup.components.heaviestweight;

import com.foxfarmroad.ManualTest;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import okhttp3.ResponseBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import retrofit2.Response;

import javax.inject.Inject;

@Category(ManualTest.class)
public class HeaviestWeightManualTest {

    @Inject
    private HeaviestWeightCallable heaviestWeightCallable;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new HeaviestWeightModule(), new HeaviestWeightManualTestModule())
                .injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<Response<ResponseBody>> testObserver = Observable.fromCallable(heaviestWeightCallable)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        System.out.println(testObserver.values().get(0));
    }
}

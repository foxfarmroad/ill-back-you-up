package com.foxfarmroad.illbackyouup.components.yoursteps;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import org.jsoup.nodes.Document;
import retrofit2.http.Url;
import retrofit2.mock.BehaviorDelegate;

public class YourStepsMockEndpoint implements YourStepsEndpoint {

    private final BehaviorDelegate<YourStepsEndpoint> delegate;
    private final ResponseBody responseBody;

    public YourStepsMockEndpoint(BehaviorDelegate<YourStepsEndpoint> service, ResponseBody responseBody) {
        this.delegate = service;
        this.responseBody = responseBody;
    }

    @Override
    public Observable<Document> document(@Url String url) {
        return delegate.returningResponse(responseBody).document(url);
    }

}

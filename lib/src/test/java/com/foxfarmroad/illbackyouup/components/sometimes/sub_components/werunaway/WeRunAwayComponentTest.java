package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.RetrofitTestModule;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(ComponentTest.class)
public class WeRunAwayComponentTest {

    @Inject
    private WeRunAwayFunction weRunAwayFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.override(new WeRunAwayModule()).with(new WeRunAwayTestModule(),
                new RetrofitTestModule())).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);

        when(walkBean.getLocation()).thenReturn("");
        when(walkBean.getDate()).thenReturn("01.01.17");

        TestObserver<Integer> testObserver = Observable.just(walkBean)
                .flatMap(weRunAwayFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(2);
    }

    @Test
    public void verifyE() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);

        when(walkBean.getLocation()).thenReturn("");
        when(walkBean.getDate()).thenReturn("");

        TestObserver<Integer> testObserver = Observable.just(walkBean)
                .flatMap(weRunAwayFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertError(IllegalArgumentException.class);
    }

}

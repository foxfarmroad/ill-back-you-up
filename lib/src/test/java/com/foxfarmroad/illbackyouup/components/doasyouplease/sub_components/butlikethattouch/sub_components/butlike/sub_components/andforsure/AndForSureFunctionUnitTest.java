package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.ButLikeThatTouchModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown.IveKnownModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class AndForSureFunctionUnitTest {

    @Mock
    @Bind
    private Function<Node, Observable<Integer>> positionFunction;

    @Mock
    @Bind
    @And
    private Function<Node, Observable<String>> andFunction;

    @Mock
    @Bind
    @For
    private Function<Node, Observable<String>> forFunction;

    @Inject
    private AndForSureFunction andForSureFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new AndForSureModule(), new ButLikeThatTouchModule(),
                new IveKnownModule(), new WouldYouLikeModule()).with(BoundFieldModule.of(this)))
                .injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        when(positionFunction.apply(any())).thenReturn(Observable.just(589));
        when(forFunction.apply(any())).thenReturn(Observable.just("Digging a Ditch"));

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:589;\">17.&nbsp;&nbsp;Digging a Ditch&nbsp;</div>").body().childNode(0))
                .flatMap(andForSureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals(new Integer(589), testObserver.values().get(0).getKey());
        assertEquals("Digging a Ditch", testObserver.values().get(0).getValue());
    }

    @Test
    public void verifyRight2() throws Exception {
        when(positionFunction.apply(any())).thenReturn(Observable.just(691));
        when(forFunction.apply(any())).thenReturn(Observable.just("Encore:"));

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:691;\">-------- ENCORE -------- &nbsp;</div>").body().childNode(0))
                .flatMap(andForSureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals(new Integer(691), testObserver.values().get(0).getKey());
        assertEquals("Encore:", testObserver.values().get(0).getValue());
    }

    @Test
    public void verifyRight3() throws Exception {
        when(positionFunction.apply(any())).thenReturn(Observable.just(606));
        when(andFunction.apply(any())).thenReturn(Observable.just("Don't Drink The Water"));

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:606;\">18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;\n<img src=\"images/setlists/NewArrow.png\" hspace=\"4\" /></div>").body().childNode(0))
                .flatMap(andForSureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals(new Integer(606), testObserver.values().get(0).getKey());
        assertEquals("Don't Drink The Water", testObserver.values().get(0).getValue());
    }

    @Test
    public void verifyB1() throws Exception {
        Node node = mock(Node.class);

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(node)
                .flatMap(andForSureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

    @Test
    public void verifyB2() throws Exception {
        Element element = mock(Element.class);
        when(element.attr("style")).thenReturn("");

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(element)
                .flatMap(andForSureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

    @Test
    public void verifyB3() throws Exception {
        when(positionFunction.apply(any())).thenReturn(Observable.just(691));
        when(andFunction.apply(any())).thenReturn(Observable.empty());

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:606;\">18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;\n<img src=\"images/setlists/NewArrow.png\" hspace=\"4\" /></div>").body().childNode(0))
                .flatMap(andForSureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

    @Test
    public void verifyB4() throws Exception {
        when(positionFunction.apply(any())).thenReturn(Observable.just(691));
        when(forFunction.apply(any())).thenReturn(Observable.empty());

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:691;\">-------- ENCORE -------- &nbsp;</div>").body().childNode(0))
                .flatMap(andForSureFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(0);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway;

import com.foxfarmroad.ManualTest;
import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(ManualTest.class)
public class WeRunAwayManualTest {

    @Inject
    private WeRunAwayFunction weRunAwayFunction;

    @Bind
    @TimeStart
    private Integer timeStart = 20;

    @Bind
    private DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("MM.dd.yy").withZoneUTC();

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new WeRunAwayModule(), BoundFieldModule.of(this)).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        WalkBean walkBean = mock(WalkBean.class);

        when(walkBean.getLocation()).thenReturn("Beaverton, OR");
        when(walkBean.getDate()).thenReturn("01.01.17");

        TestObserver<Integer> testObserver = Observable.just(walkBean)
                .flatMap(weRunAwayFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(-28800);
    }

}

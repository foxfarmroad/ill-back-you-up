package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing;

import com.foxfarmroad.illbackyouup.components.doasyouplease.StyleAttr;
import com.google.inject.AbstractModule;

public class ForeverOnlyKnowingTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(StyleAttr.class).to("style");
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease;

import com.foxfarmroad.illbackyouup.io.DoAsYouPlease;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class DoAsYouPleaseTestModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new DoAsYouPleaseModule());

        bind(new TypeLiteral<Function<YourStepsBean, Observable<DoAsYouPlease>>>() { })
                .to(DoAsYouPleaseFunction.class);
    }

}

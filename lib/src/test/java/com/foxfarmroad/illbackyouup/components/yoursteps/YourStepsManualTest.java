package com.foxfarmroad.illbackyouup.components.yoursteps;

import com.foxfarmroad.ManualTest;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(ManualTest.class)
public class YourStepsManualTest {

    @Bind
    @Your
    private String your = "http://www.dmbalmanac.com";

    @Bind
    @Steps
    private String steps = "TourShow.aspx?where=2018";

    @Inject
    private YourStepsCallable yourStepsCallable;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new YourStepsModule(), BoundFieldModule.of(this)).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<YourStepsBean> testObserver = Observable.fromCallable(yourStepsCallable)
                .blockingSingle()
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        System.out.println(testObserver.values().get(0));
    }
}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class IveKnownModuleUnitTest {

    @Inject
    @Knowledge
    private String knowledge;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new IveKnownModule()).injectMembers(this);
    }

}

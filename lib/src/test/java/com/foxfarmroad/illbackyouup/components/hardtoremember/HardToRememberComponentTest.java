package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.ComponentTest;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import org.apache.commons.configuration2.ImmutableConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import software.amazon.awssdk.services.ssm.SSMClient;
import software.amazon.awssdk.services.ssm.model.DescribeParametersRequest;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;
import software.amazon.awssdk.services.ssm.model.GetParametersRequest;
import software.amazon.awssdk.services.ssm.model.GetParametersResponse;
import software.amazon.awssdk.services.ssm.model.Parameter;
import software.amazon.awssdk.services.ssm.model.ParameterMetadata;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@Category(ComponentTest.class)
public class HardToRememberComponentTest {

    @Mock
    @Bind
    private SSMClient ssm;

    @Inject
    private ImmutableConfiguration immutableConfiguration;

    @Mock
    private DescribeParametersResponse describeParametersResponse;

    private List<ParameterMetadata> parameterMetadataList;

    @Mock
    private ParameterMetadata parameterMetadata;

    @Mock
    private GetParametersResponse getParametersResponse;

    private List<Parameter> parameterList;

    @Mock
    private Parameter parameter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void verifyRight() throws Exception {
        parameterMetadataList = new ArrayList<>(1);
        parameterMetadataList.add(parameterMetadata);

        parameterList = new ArrayList<>(1);
        parameterList.add(parameter);

        when(ssm.describeParameters(any(DescribeParametersRequest.class))).thenReturn(describeParametersResponse);
        when(describeParametersResponse.parameters()).thenReturn(parameterMetadataList);
        when(parameterMetadata.name()).thenReturn(UUID.randomUUID().toString());
        when(ssm.getParameters(any(GetParametersRequest.class))).thenReturn(getParametersResponse);
        when(getParametersResponse.parameters()).thenReturn(parameterList);
        when(parameter.name()).thenReturn(UUID.randomUUID().toString());
        when(parameter.value()).thenReturn(UUID.randomUUID().toString());

        Guice.createInjector(Modules.override(new HardToRememberModule()).with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyB() throws Exception {
        parameterMetadataList = new ArrayList<>(0);

        parameterList = new ArrayList<>(0);

        when(ssm.describeParameters(any(DescribeParametersRequest.class))).thenReturn(describeParametersResponse);
        when(describeParametersResponse.parameters()).thenReturn(parameterMetadataList);
        when(ssm.getParameters(any(GetParametersRequest.class))).thenReturn(getParametersResponse);
        when(getParametersResponse.parameters()).thenReturn(parameterList);

        Guice.createInjector(Modules.override(new HardToRememberModule()).with(BoundFieldModule.of(this))).injectMembers(this);
    }

}

package com.foxfarmroad.illbackyouup.components.heaviestweight;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.RetrofitTestModule;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import okhttp3.ResponseBody;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import retrofit2.Response;

import javax.inject.Inject;

@Category(ComponentTest.class)
public class HeaviestWeightComponentTest {

    @Inject
    private HeaviestWeightCallable heaviestWeightCallable;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.override(new HeaviestWeightModule()).with(new HeaviestWeightTestModule(),
                new RetrofitTestModule())).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<Response<ResponseBody>> testObserver = Observable.fromCallable(heaviestWeightCallable)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
    }
}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast;

import com.foxfarmroad.RetrofitTestModule;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.junit.Test;
import retrofit2.Retrofit;

import javax.inject.Inject;

public class HowFastModuleUnitTest {

    @Inject
    @How
    private Retrofit retrofit;

    @Inject
    private HowFastEndpoint howFastEndpoint;

    @Inject
    private HttpLoggingInterceptor httpLoggingInterceptor;

    @Inject
    private OkHttpClient okHttpClient;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new HowFastModule(), new RetrofitTestModule(),
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bindConstant().annotatedWith(How.class).to("http://localhost");
                    }
                })
                .injectMembers(this);
    }
}

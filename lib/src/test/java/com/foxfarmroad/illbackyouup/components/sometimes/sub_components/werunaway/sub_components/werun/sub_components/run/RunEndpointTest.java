package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run;

import com.foxfarmroad.RetrofitTestModule;
import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.io.RunBean;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

@Category(UnitTest.class)
public class RunEndpointTest {

    @Inject
    @Path
    private String path;

    @Inject
    private RunEndpoint runEndpoint;

    @Inject
    private RunBean runBean;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new RunModule()).with(new RunTestModule(), new RetrofitTestModule()))
                .injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<RunBean> testObserver = runEndpoint
                .getGeocodeJson(path, "test")
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        testObserver.assertResult(runBean);
    }

}

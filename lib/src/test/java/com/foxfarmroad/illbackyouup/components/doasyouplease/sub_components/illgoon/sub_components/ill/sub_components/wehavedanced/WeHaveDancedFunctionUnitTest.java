package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.inject.Inject;
import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class WeHaveDancedFunctionUnitTest {

    @Mock
    @Bind
    private Function<Node, Observable<Integer>> positionFunction;

    @Mock
    @Bind
    private BiConsumer<StringBuilder, Node> inTheRiskConsumer;

    @Inject
    private WeHaveDancedFunction weHaveDancedFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new WeHaveDancedModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        when(positionFunction.apply(any())).thenReturn(Observable.just(806));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                if (((Node) args[1]).nodeName().equals("#text")) {
                    ((StringBuilder) args[0]).append("* Dave Solo");
                }
                return "called with arguments: " + Arrays.toString(args);
            }
        }).when(inTheRiskConsumer).accept(any(), any());

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parse("<div style=\"Color:#3f94aa;Position:Absolute;Top:806;\"><font style=\"font-size:19px;\">*</font>&nbsp;Dave Solo</div>").body().child(0))
                .flatMap(weHaveDancedFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(ImmutablePair.of(806, "* Dave Solo"));
    }

    @Test
    public void verifyRight2() throws Exception {
        when(positionFunction.apply(any())).thenReturn(Observable.just(762));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                if (((Node) args[1]).nodeName().equals("#text") && !((TextNode) args[1]).text().contains("SHOW NOTES")
                        && !((TextNode) args[1]).text().equals(" ")) {
                    ((StringBuilder) args[0]).append("-> indicates a segue into next song");
                }
                return "called with arguments: " + Arrays.toString(args);
            }
        }).when(inTheRiskConsumer).accept(any(), any());

        TestObserver<Pair<Integer, String>> testObserver = Observable.just(Jsoup.parse("<div style=\"Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;\">SHOW NOTES:<br />&nbsp;<img src=\"images/setlists/NewArrow.png\" width=\"16\" height=\"9\" />&nbsp;indicates a segue into next song</div>").body().child(0))
                .flatMap(weHaveDancedFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(ImmutablePair.of(762, "-> indicates a segue into next song"));
    }

}

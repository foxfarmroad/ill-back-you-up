package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class WouldYouLikeFunctionUnitTest {

    @Inject
    private WouldYouLikeFunction wouldYouLikeFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new WouldYouLikeModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        TestObserver<String> testObserver = Observable.just(Jsoup.parse("&nbsp;Dave Solo� ").body().textNodes().get(0).text())
                .flatMap(wouldYouLikeFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue("Dave Solo'");
    }

    @Test
    public void verifyRight2() throws Exception {
        TestObserver<String> testObserver = Observable.just(Jsoup.parse(" &nbsp;ï¿½indicates a segue into next song").body().textNodes().get(0).text())
                .flatMap(wouldYouLikeFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue("'indicates a segue into next song");
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.Fast;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.How;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastEndpoint;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastMockEndpoint;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.Path;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.Run;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.RunEndpoint;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.RunMockEndpoint;
import com.foxfarmroad.illbackyouup.io.GeometryBean;
import com.foxfarmroad.illbackyouup.io.HowFastBean;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.foxfarmroad.illbackyouup.io.RunBean;
import com.foxfarmroad.illbackyouup.io.RunResultsBean;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

import java.util.Collections;

public class SometimesTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(How.class).to("http://localhost");
        bindConstant().annotatedWith(Fast.class).to("test");

        bindConstant().annotatedWith(Run.class).to("http://localhost");
        bindConstant().annotatedWith(Path.class).to("test");
    }

    @Provides
    @How
    MockRetrofit provideHowMockRetrofit(@How Retrofit retrofit, NetworkBehavior networkBehavior) {
        return new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();
    }

    @Provides
    HowFastEndpoint provideHowFastEndpoint(@How MockRetrofit mockRetrofit, HowFastBean howFastBean) {
        BehaviorDelegate<HowFastEndpoint> delegate = mockRetrofit.create(HowFastEndpoint.class);
        return new HowFastMockEndpoint(delegate, howFastBean);
    }

    @Provides
    HowFastBean provideHowFastBean() {
        HowFastBean howFastBean = new HowFastBean();
        howFastBean.setDstOffset(1);
        howFastBean.setRawOffset(1);

        return howFastBean;
    }

    @Provides
    @Run
    MockRetrofit provideRunMockRetrofit(@Run Retrofit retrofit, NetworkBehavior networkBehavior) {
        return new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();
    }

    @Provides
    RunEndpoint provideRunEndpoint(@Run MockRetrofit mockRetrofit, RunBean runBean) {
        BehaviorDelegate<RunEndpoint> delegate = mockRetrofit.create(RunEndpoint.class);
        return new RunMockEndpoint(delegate, runBean);
    }

    @Provides
    RunBean provideRunBean() {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(1.0f);
        latLngBean.setLng(1.0f);
        GeometryBean geometryBean = new GeometryBean();
        geometryBean.setLocation(latLngBean);
        RunResultsBean runResultsBean = new RunResultsBean();
        runResultsBean.setGeometry(geometryBean);
        RunBean runBean = new RunBean();
        runBean.setResults(Collections.singletonList(runResultsBean));

        return runBean;
    }

    @Provides
    @Sometimes
    DateTime provideNow() {
        return new DateTime()
                .withZone(DateTimeZone.UTC)
                .withHourOfDay(10)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
    }

}

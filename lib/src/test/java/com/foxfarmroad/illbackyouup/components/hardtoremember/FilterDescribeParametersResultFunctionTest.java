package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;
import software.amazon.awssdk.services.ssm.model.ParameterMetadata;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class FilterDescribeParametersResultFunctionTest {

    @Mock
    private DescribeParametersResponse describeParametersResponse;

    private List<ParameterMetadata> parameterMetadataList;

    @Inject
    private FilterDescribeParametersResultFunction filterDescribeParametersResultPredicate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(new HardToRememberModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        ParameterMetadata parameterMetadata = ParameterMetadata.builder()
                .name("name")
                .keyId("keyId")
                .build();
        parameterMetadataList = new ArrayList<>();
        parameterMetadataList.add(parameterMetadata);
        when(describeParametersResponse.parameters()).thenReturn(parameterMetadataList);

        TestObserver<DescribeParametersResponse> testObserver = Observable.just(describeParametersResponse)
                .filter(filterDescribeParametersResultPredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(describeParametersResponse);
    }

    @Test
    public void verifyRight2() throws Exception {
        parameterMetadataList = new ArrayList<>();
        when(describeParametersResponse.parameters()).thenReturn(parameterMetadataList);

        TestObserver<DescribeParametersResponse> testObserver = Observable.just(describeParametersResponse)
                .filter(filterDescribeParametersResultPredicate)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(0);
    }

}

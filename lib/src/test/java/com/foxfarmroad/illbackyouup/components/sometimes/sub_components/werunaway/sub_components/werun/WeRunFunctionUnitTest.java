package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.RunEndpoint;
import com.foxfarmroad.illbackyouup.io.GeometryBean;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.foxfarmroad.illbackyouup.io.RunBean;
import com.foxfarmroad.illbackyouup.io.RunResultsBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.Collections;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class WeRunFunctionUnitTest {

    @Mock
    @Bind
    @WeRun
    private Function<String, Observable<String>> weFunction;

    @Mock
    @Bind
    private RunEndpoint runEndpoint;

    @Inject
    private WeRunFunction weRunFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new WeRunModule()).with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(1.0f);
        latLngBean.setLng(1.0f);
        GeometryBean geometryBean = new GeometryBean();
        geometryBean.setLocation(latLngBean);
        RunResultsBean runResultsBean = new RunResultsBean();
        runResultsBean.setGeometry(geometryBean);
        RunBean runBean = new RunBean();
        runBean.setResults(Collections.singletonList(runResultsBean));

        when(weFunction.apply(anyString())).thenReturn(Observable.just("CA"));
        when(runEndpoint.getGeocodeJson(anyString(), anyString())).thenReturn(Observable.just(runBean));

        TestObserver<LatLngBean> testObserver = Observable.just("Toronto, ON, CAN")
                .flatMap(weRunFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(latLngBean);
        verify(weFunction, times(1)).apply(any());
        verifyNoMoreInteractions(weFunction);
        verify(runEndpoint, times(1)).getGeocodeJson(anyString(), anyString());
        verifyNoMoreInteractions(runEndpoint);
    }

    @Test
    public void verifyB() throws Exception {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(1.0f);
        latLngBean.setLng(1.0f);
        GeometryBean geometryBean = new GeometryBean();
        geometryBean.setLocation(latLngBean);
        RunResultsBean runResultsBean = new RunResultsBean();
        runResultsBean.setGeometry(geometryBean);
        RunBean runBean = new RunBean();
        runBean.setResults(Collections.singletonList(runResultsBean));

        when(runEndpoint.getGeocodeJson(anyString(), anyString())).thenReturn(Observable.just(runBean));

        TestObserver<LatLngBean> testObserver = Observable.just("Beaverton, OR")
                .flatMap(weRunFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(latLngBean);
        verify(weFunction, times(0)).apply(any());
        verifyNoMoreInteractions(weFunction);
        verify(runEndpoint, times(1)).getGeocodeJson(anyString(), anyString());
        verifyNoMoreInteractions(runEndpoint);
    }

}

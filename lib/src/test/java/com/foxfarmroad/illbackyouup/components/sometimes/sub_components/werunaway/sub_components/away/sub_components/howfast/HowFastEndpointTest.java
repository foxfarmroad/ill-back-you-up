package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast;

import com.foxfarmroad.RetrofitTestModule;
import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.io.HowFastBean;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

@Category(UnitTest.class)
public class HowFastEndpointTest {

    @Inject
    @Fast
    private String fast;

    @Inject
    private HowFastEndpoint howFastEndpoint;

    @Inject
    private HowFastBean howFastBean;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new HowFastModule()).with(new HowFastTestModule(),
                new RetrofitTestModule())).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<HowFastBean> testObserver = howFastEndpoint
                .getTimezoneJson(fast, "test", -1L)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        testObserver.assertResult(howFastBean);
    }

}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ImmutableConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static org.mockito.Mockito.mock;

@Category(UnitTest.class)
public class ConfigurationToImmutableConfigurationObservableFunctionTest {

    @Inject
    private ConfigurationToImmutableConfigurationObservableFunction configurationToImmutableConfigurationObservableFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.EMPTY_MODULE).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        Configuration configuration = mock(Configuration.class);

        TestObserver<ImmutableConfiguration> testObserver = Observable.just(configuration)
                .flatMap(configurationToImmutableConfigurationObservableFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
    }

}

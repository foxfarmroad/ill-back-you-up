package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.we;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class WeFunctionUnitTest {

    @Inject
    private WeFunction weFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new WeModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        TestObserver<String> testObserver = Observable.just("&nbsp;")
                .flatMap(weFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult("");
    }

    @Test
    public void verifyRight2() throws Exception {
        TestObserver<String> testObserver = Observable.just("&#32;")
                .flatMap(weFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult("");
    }

    @Test
    public void verifyRight3() throws Exception {
        TestObserver<String> testObserver = Observable.just("&Auml;")
                .flatMap(weFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult("Ä");
    }

    @Test
    public void verifyRight4() throws Exception {
        TestObserver<String> testObserver = Observable.just("&#196;")
                .flatMap(weFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult("Ä");
    }

    @Test
    public void verifyRight5() throws Exception {
        TestObserver<String> testObserver = Observable.just(" &#196; ")
                .flatMap(weFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult("Ä");
    }

    @Test
    public void verifyRight6() throws Exception {
        TestObserver<String> testObserver = Observable.just("   \n ")
                .flatMap(weFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult("");
    }

}

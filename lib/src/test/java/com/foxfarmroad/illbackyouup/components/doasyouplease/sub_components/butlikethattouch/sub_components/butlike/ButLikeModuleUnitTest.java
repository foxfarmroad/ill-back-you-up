package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.AndForSure;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Node;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class ButLikeModuleUnitTest {

    @Inject
    @AndForSure
    private Function<Node, Observable<Pair<Integer, String>>> andForSureFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new ButLikeModule(), new WouldYouLikeModule()).injectMembers(this);
    }

}

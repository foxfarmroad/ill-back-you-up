package com.foxfarmroad.illbackyouup.components.yoursteps;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class YourStepsTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(Your.class).to("http://localhost/");
        bindConstant().annotatedWith(Steps.class).to("test");
    }

    @Provides
    MockRetrofit provideMockRetrofit(@YourSteps Retrofit retrofit, NetworkBehavior networkBehavior) {
        return new MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build();
    }

    @Provides
    YourStepsEndpoint provideYourStepsEndpoint(MockRetrofit mockRetrofit, ResponseBody responseBody) {
        BehaviorDelegate<YourStepsEndpoint> delegate = mockRetrofit.create(YourStepsEndpoint.class);
        return new YourStepsMockEndpoint(delegate, responseBody);
    }

}

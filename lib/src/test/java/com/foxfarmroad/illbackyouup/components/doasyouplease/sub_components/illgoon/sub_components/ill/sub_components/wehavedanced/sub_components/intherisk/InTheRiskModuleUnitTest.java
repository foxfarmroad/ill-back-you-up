package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.sub_components.intherisk;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.IRememberThinking;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class InTheRiskModuleUnitTest {

    @Inject
    @IRememberThinking
    private Function<String, Observable<String>> iRememberThinkingFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new InTheRiskModule(), new WouldYouLikeModule()).injectMembers(this);
    }

}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import software.amazon.awssdk.services.ssm.model.DescribeParametersRequest;
import software.amazon.awssdk.services.ssm.model.ParametersFilter;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@Category(UnitTest.class)
public class ParametersFilterListToDescribeParametersRequestFunctionTest {

    private Collection<ParametersFilter> parametersFilterCollection;

    @Inject
    private ParametersFilterListToDescribeParametersRequestFunction parametersFilterListToDescribeParametersRequestFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new HardToRememberModule()).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        parametersFilterCollection = new ArrayList<>();
        parametersFilterCollection.add(ParametersFilter.builder().build());

        TestObserver<DescribeParametersRequest> testObserver = Observable.just(parametersFilterCollection)
                .map(parametersFilterListToDescribeParametersRequestFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        DescribeParametersRequest describeParametersRequest = testObserver.values().get(0);
        assertEquals(parametersFilterCollection, describeParametersRequest.filters());
    }

}

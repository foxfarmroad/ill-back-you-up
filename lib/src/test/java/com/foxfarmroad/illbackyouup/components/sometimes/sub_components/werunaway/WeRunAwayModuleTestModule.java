package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway;

import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * <p>
 * </p>
 */
public class WeRunAwayModuleTestModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(TimeStart.class).to(20);
    }

    @Provides
    DateTimeFormatter provideDateTimeFormatter() {
        return DateTimeFormat.forPattern("MM.dd.yy").withZoneUTC();
    }

}

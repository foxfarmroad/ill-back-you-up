package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Category(ComponentTest.class)
public class ButLikeComponentTest {

    @Inject
    private ButLikeFunction butLikeFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new ButLikeModule(), new WouldYouLikeModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        String divBlock = "<div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:470;\">\n" +
                "    10.&nbsp;&nbsp;Warehouse&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:426;\">\n" +
                "    -------- SET BREAK -------- &nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:674;\">\n" +
                "    22.&nbsp;&nbsp;Grey Street&nbsp;&Auml;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:358;\">\n" +
                "    &nbsp;5.&nbsp;&nbsp;Christmas Song&nbsp;~\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:623;\">\n" +
                "    19.&nbsp;&nbsp;Typical Situation&nbsp;&Auml;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:691;\">\n" +
                "    -------- ENCORE -------- &nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:640;\">\n" +
                "    20.&nbsp;&nbsp;Drunken Soldier&nbsp;&Auml;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#3f94aa;Position:Absolute;Top:806;\">\n" +
                "    <font style=\"font-size:19px;\">*</font>&nbsp;Dave Solo\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#3f94aa;Position:Absolute;Top:840;\">\n" +
                "    <font style=\"font-size:19px;\">~</font>&nbsp;Carter, Dave and Tim\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:392;\">\n" +
                "    &nbsp;7.&nbsp;&nbsp;Lie In Our Graves&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;\">\n" +
                "    SHOW NOTES:\n" +
                "    <br />&nbsp;\n" +
                "    <img src=\"images/setlists/NewArrow.png\" width=\"16\" height=\"9\" />&nbsp;indicates a segue into next song\n" +
                "   </div>\n" +
                "   <br />\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:290;\">\n" +
                "    &nbsp;1.&nbsp;&nbsp;Beach Ball&nbsp;*\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:555;\">\n" +
                "    15.&nbsp;&nbsp;Jimi Thing&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:735;\">\n" +
                "    24.&nbsp;&nbsp;Ants Marching&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:589;\">\n" +
                "    17.&nbsp;&nbsp;Digging a Ditch&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:504;\">\n" +
                "    12.&nbsp;&nbsp;Crush&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:487;\">\n" +
                "    11.&nbsp;&nbsp;Belly Belly Nice&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:538;\">\n" +
                "    14.&nbsp;&nbsp;If Only&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:375;\">\n" +
                "    &nbsp;6.&nbsp;&nbsp;Sweet&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:307;\">\n" +
                "    &nbsp;2.&nbsp;&nbsp;Minarets&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:718;\">\n" +
                "    23.&nbsp;&nbsp;Granny&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:453;\">\n" +
                "    &nbsp;9.&nbsp;&nbsp;Save Me&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:521;\">\n" +
                "    13.&nbsp;&nbsp;Seven&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#3f94aa;Position:Absolute;Top:823;\">\n" +
                "    <font style=\"font-size:19px;\">+</font>&nbsp;Carter and Dave\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:606;\">\n" +
                "    18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;\n" +
                "    <img src=\"images/setlists/NewArrow.png\" hspace=\"4\" />\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:409;\">\n" +
                "    &nbsp;8.&nbsp;&nbsp;Two Step&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:657;\">\n" +
                "    21.&nbsp;&nbsp;Corn Bread&nbsp;&Auml;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#3f94aa;Position:Absolute;Top:857;\">\n" +
                "    <font style=\"font-size:19px;\">&Auml;</font>&nbsp;Bela Fleck\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:341;\">\n" +
                "    &nbsp;4.&nbsp;&nbsp;Little Red Bird&nbsp;+\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "    Spoon\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:572;\">\n" +
                "    16.&nbsp;&nbsp;The Riff&nbsp;\n" +
                "    <img src=\"images/setlists/NewArrow.png\" hspace=\"4\" />\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:324;\">\n" +
                "    &nbsp;3.&nbsp;&nbsp;What Would You Say&nbsp;\n" +
                "   </div>\n" +
                "   <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "    So Much To Say\n" +
                "   </div>\n" +
                "  </div>";

        TestObserver<List<String>> testObserver = Observable.just((Node) Jsoup.parse(divBlock).body().child(0))
                .flatMap(butLikeFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        assertEquals(26, testObserver.values().get(0).size());
        assertEquals("Beach Ball*", testObserver.values().get(0).get(0));
        assertEquals("Ants Marching", testObserver.values().get(0).get(25));
    }

}

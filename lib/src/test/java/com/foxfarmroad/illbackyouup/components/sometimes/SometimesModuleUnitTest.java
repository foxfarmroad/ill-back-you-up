package com.foxfarmroad.illbackyouup.components.sometimes;

import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.functions.Predicate;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import javax.inject.Inject;

public class SometimesModuleUnitTest {

    @Inject
    private Function<YourStepsBean, Observable<WalkBean>> weWalkFunction;

    @Inject
    private Predicate<WalkBean> somePredicate;

    @Inject
    private Function<WalkBean, Observable<Integer>> weRunAwayFunction;

    @Inject
    private DateTimeFormatter dateTimeFormatter;

    @Inject
    @Sometimes
    private DateTime now;

    @Inject
    private Function3<DateTime, DateTime, Integer, Observable<Long>> timesFunction3;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new SometimesModule()).injectMembers(this);
    }
}

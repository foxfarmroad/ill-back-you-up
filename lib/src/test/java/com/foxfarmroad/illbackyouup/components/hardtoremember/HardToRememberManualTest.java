package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.ManualTest;
import com.google.inject.Guice;
import org.apache.commons.configuration2.ImmutableConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.Iterator;

@Category(ManualTest.class)
public class HardToRememberManualTest {

    @Inject
    private ImmutableConfiguration immutableConfiguration;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new HardToRememberModule()).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        // Print out the configuration that came from AWS
        Iterator<String> keyIterator = immutableConfiguration.getKeys();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            System.out.println(key + " : " + immutableConfiguration.getString(key));
        }
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.OldNoteStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.WeHaveDanced;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Node;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class IllModuleUnitTest {

    @Inject
    @OldNoteStyle
    private String noteStyle;

    @Inject
    @WeHaveDanced
    private Function<Node, Observable<Pair<Integer, String>>> weHaveDancedFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new IllModule(), new WouldYouLikeModule()).injectMembers(this);
    }
}

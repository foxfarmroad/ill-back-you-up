package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.RetrofitTestModule;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.RunTestModule;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

/**
 * <p>
 * </p>
 */
@Category(ComponentTest.class)
public class WeRunComponentTest {

    @Inject
    private WeRunFunction weRunFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.override(new WeRunModule()).with(new RunTestModule(), new RetrofitTestModule()))
                .injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        TestObserver<LatLngBean> testObserver = Observable.just("test")
                .flatMap(weRunFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
    }

}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;
import software.amazon.awssdk.services.ssm.model.ParameterMetadata;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class DescribeParametersResultToNameListObservableFunctionTest {

    @Mock
    private DescribeParametersResponse describeParametersResponse;

    private List<ParameterMetadata> parameterMetadataList;

    @Inject
    private DescribeParametersResultToNameListObservableFunction describeParametersResultObservableFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.EMPTY_MODULE).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        ParameterMetadata parameterMetadata = ParameterMetadata.builder()
                .name("name")
                .keyId("keyId")
                .build();
        parameterMetadataList = new ArrayList<>();
        parameterMetadataList.add(parameterMetadata);
        when(describeParametersResponse.parameters()).thenReturn(parameterMetadataList);

        List<String> names = new ArrayList<>();
        names.add("name");

        TestObserver<List<String>> testObserver = Observable.just(describeParametersResponse)
                .flatMap(describeParametersResultObservableFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(names);
    }

    @Test
    public void verifyRight2() throws Exception {
        ParameterMetadata parameterMetadata1 = ParameterMetadata.builder()
                .name("name1")
                .keyId("keyId1")
                .build();
        ParameterMetadata parameterMetadata2 = ParameterMetadata.builder()
                .name("name2")
                .keyId("keyId2")
                .build();
        parameterMetadataList = new ArrayList<>();
        parameterMetadataList.add(parameterMetadata1);
        parameterMetadataList.add(parameterMetadata2);
        when(describeParametersResponse.parameters()).thenReturn(parameterMetadataList);

        List<String> names = new ArrayList<>();
        names.add("name1");
        names.add("name2");

        TestObserver<List<String>> testObserver = Observable.just(describeParametersResponse)
                .flatMap(describeParametersResultObservableFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(names);
    }

    @Test
    public void verifyRight3() throws Exception {
        parameterMetadataList = new ArrayList<>();
        when(describeParametersResponse.parameters()).thenReturn(parameterMetadataList);

        List<String> names = new ArrayList<>();

        TestObserver<List<String>> testObserver = Observable.just(describeParametersResponse)
                .flatMap(describeParametersResultObservableFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(names);
    }

}

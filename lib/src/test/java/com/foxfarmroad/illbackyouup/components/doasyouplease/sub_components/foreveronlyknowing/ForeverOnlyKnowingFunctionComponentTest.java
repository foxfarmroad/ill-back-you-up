package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.jsoup.Jsoup;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(ComponentTest.class)
public class ForeverOnlyKnowingFunctionComponentTest {

    @Inject
    private ForeverOnlyKnowingFunction foreverOnlyKnowingFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new ForeverOnlyKnowingModule(), new ForeverOnlyKnowingTestModule())
                .injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        YourStepsBean yourStepsBean = new YourStepsBean(Jsoup.parse("<body bgcolor=\"#eeeeee\" background=\"images/background.jpg\">\n" +
                " <div id=\"pageWrapper\">\n" +
                "  <!-- Begin Header Area Including Top Nav -->\n" +
                "  <table bgcolor=\"#000000\" width=\"927\" height=\"113\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"Table1\">\n" +
                "   <tbody>\n" +
                "    <tr>\n" +
                "     <th rowspan=\"3\"> <img src=\"images/header_marginLeft.gif\" width=\"8\" height=\"113\" alt=\"\" /></th>\n" +
                "     <td><img src=\"images/header_marginTop.gif\" width=\"911\" height=\"6\" alt=\"\" /></td>\n" +
                "     <td rowspan=\"3\"><img src=\"images/header_marginRight.gif\" width=\"8\" height=\"113\" alt=\"\" /></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td>\n" +
                "      <table bgcolor=\"#000000\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"Table2\">\n" +
                "       <tbody>\n" +
                "        <tr>\n" +
                "         <td><img src=\"images/header_logo.gif\" width=\"357\" height=\"75\" alt=\"\" /></td>\n" +
                "         <td><img src=\"images/header_adSpace.png\" width=\"419\" height=\"75\" alt=\"\" /></td>\n" +
                "         <td id=\"loginArea\">\n" +
                "          <div id=\"loginText\">\n" +
                "           Welcome fateman\n" +
                "           <br /> exp: 04.30.2015\n" +
                "           <br />\n" +
                "           <a href=\"memberinfo.asp?CurrentPage=MyInfo\">My Info</a>\n" +
                "           <br />\n" +
                "           <a href=\"logoff.asp\">Logout</a>\n" +
                "          </div> </td>\n" +
                "        </tr>\n" +
                "       </tbody>\n" +
                "      </table> </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td bgcolor=\"#666666\">\n" +
                "      <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "       <tbody>\n" +
                "        <tr>\n" +
                "         <td><a href=\"index.asp\" onmouseover=\"imageSwap('home','images/NewNav/Home_on.gif');\" onmouseout=\"imageSwap('home','images/NewNav/Home_off.gif');\"><img src=\"images/NewNav/Home_off.gif\" name=\"home\" width=\"61\" height=\"32\" border=\"0\" alt=\"\" /></a></td>\n" +
                "         <td><a href=\"news.asp\" onmouseover=\"imageSwap('news','images/NewNav/News_on.gif');\" onmouseout=\"imageSwap('news','images/NewNav/News_off.gif');\"><img src=\"images/NewNav/News_off.gif\" name=\"news\" width=\"58\" height=\"32\" border=\"0\" alt=\"News\" /></a></td>\n" +
                "         <td><a href=\"ticketing.asp\" onmouseover=\"imageSwap('ticketing','images/NewNav/Ticketing_on.gif');\" onmouseout=\"imageSwap('ticketing','images/NewNav/Ticketing_off.gif');\"><img src=\"images/NewNav/Ticketing_off.gif\" name=\"ticketing\" width=\"90\" height=\"32\" border=\"0\" alt=\"Ticketing\" /></a></td>\n" +
                "         <td><a href=\"backstage.asp\"><img src=\"images/NewNav/Backstage_on.gif\" name=\"backstage\" width=\"104\" height=\"32\" border=\"0\" alt=\"Backstage\" /></a></td>\n" +
                "         <td><a href=\"/securejump.asp?jump_id=116\" onmouseover=\"imageSwap('audio','images/NewNav/Audio_on.gif');\" onmouseout=\"imageSwap('audio','images/NewNav/Audio_off.gif');\" target=\"_blank\"><img src=\"images/NewNav/Audio_off.gif\" name=\"audio\" width=\"66\" height=\"32\" border=\"0\" alt=\"Audio\" /></a></td>\n" +
                "         <td><a href=\"Connect.asp\" onmouseover=\"imageSwap('connect','images/NewNav/Connect_on.gif');\" onmouseout=\"imageSwap('connect','images/NewNav/Connect_off.gif');\"><img src=\"images/NewNav/Connect_off.gif\" name=\"connect\" width=\"90\" height=\"32\" border=\"0\" alt=\"Gallery\" /></a></td>\n" +
                "         <td><a href=\"fanworks.asp\" onmouseover=\"imageSwap('fanworks','images/NewNav/FanWorks_on.gif');\" onmouseout=\"imageSwap('fanworks','images/NewNav/FanWorks_off.gif');\"><img src=\"images/NewNav/FanWorks_off.gif\" name=\"fanworks\" width=\"101\" height=\"32\" border=\"0\" alt=\"Fan Works\" /></a></td>\n" +
                "         <td><a href=\"contests.asp\" onmouseover=\"imageSwap('contests','images/NewNav/Contests_on.gif');\" onmouseout=\"imageSwap('contests','images/NewNav/Contests_off.gif');\"><img src=\"images/NewNav/Contests_off.gif\" name=\"contests\" width=\"89\" height=\"32\" border=\"0\" alt=\"Contests\" /></a></td>\n" +
                "         <td><a href=\"securejump.asp?jump_id=2\" onmouseover=\"imageSwap('store','images/NewNav/Store_on.gif');\" onmouseout=\"imageSwap('store','images/NewNav/Store_off.gif');\" target=\"_blank\"><img src=\"images/NewNav/Store_off.gif\" name=\"store\" width=\"61\" height=\"32\" border=\"0\" alt=\"Store\" /></a></td>\n" +
                "         <td><a href=\"boards.asp\" onmouseover=\"imageSwap('boards','images/NewNav/Boards_on.gif');\" onmouseout=\"imageSwap('boards','images/NewNav/Boards_off.gif');\"><img src=\"images/NewNav/Boards_off.gif\" name=\"boards\" width=\"79\" height=\"32\" border=\"0\" alt=\"Boards\" /></a></td>\n" +
                "         <td><a href=\"memberinfo.asp\" onmouseover=\"imageSwap('memberinfo','images/NewNav/MemberInfo_on.gif');\" onmouseout=\"imageSwap('memberinfo','images/NewNav/MemberInfo_off.gif');\"><img src=\"images/NewNav/MemberInfo_off.gif\" name=\"memberinfo\" width=\"112\" height=\"32\" border=\"0\" alt=\"Member Info\" /></a></td>\n" +
                "        </tr>\n" +
                "       </tbody>\n" +
                "      </table></td>\n" +
                "    </tr>\n" +
                "   </tbody>\n" +
                "  </table>\n" +
                "  <!-- End Header Area -->\n" +
                "  <!-- Begin Main Content Area -->\n" +
                "  <div id=\"bodyTableBorder\">\n" +
                "   <div id=\"bodyTableContents\">\n" +
                "    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"925\" height=\"94\" style=\"background-image:url('images/backstage_head.jpg');\">\n" +
                "     <tbody>\n" +
                "      <tr>\n" +
                "       <td valign=\"top\"> &nbsp; </td>\n" +
                "      </tr>\n" +
                "     </tbody>\n" +
                "    </table>\n" +
                "    <a name=\"top\"></a>\n" +
                "    <div id=\"columnLeftGray\" style=\"width:250px;\">\n" +
                "     <img style=\"margin-left:49px;margin-top:69px;\" src=\"images/setlist_search.gif\" />\n" +
                "     <img src=\"images/spacer.gif\" border=\"0\" height=\"3\" width=\"1\" />\n" +
                "     <br />\n" +
                "     <table border=\"0\" cellpadding=\"14\" cellspacing=\"0\">\n" +
                "      <tbody>\n" +
                "       <tr>\n" +
                "        <td valign=\"top\" align=\"left\">\n" +
                "         <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "          <tbody>\n" +
                "           <tr>\n" +
                "            <form action=\"/backstage.asp\" method=\"post\"></form>\n" +
                "            <td align=\"right\" class=\"newsDate\" width=\"290\"><img src=\"images/spacer.gif\" width=\"1\" height=\"11\" border=\"0\" /><br />\n" +
                "             <div id=\"searchText\">\n" +
                "              SELECT A YEAR\n" +
                "             </div><br /> <img src=\"images/spacer.gif\" width=\"1\" height=\"5\" border=\"0\" /><br /> <select style=\"float:right;clear:both;margin-right:8px;\" name=\"year\"> <option value=\"\">----</option> <option value=\"1991\">1991</option> <option value=\"1992\">1992</option> <option value=\"1993\">1993</option> <option value=\"1994\">1994</option> <option value=\"1995\">1995</option> <option value=\"1996\">1996</option> <option value=\"1997\">1997</option> <option value=\"1998\">1998</option> <option value=\"1999\">1999</option> <option value=\"2000\">2000</option> <option value=\"2001\">2001</option> <option value=\"2002\">2002</option> <option value=\"2003\">2003</option> <option value=\"2004\">2004</option> <option value=\"2005\">2005</option> <option value=\"2006\">2006</option> <option value=\"2007\">2007</option> <option value=\"2008\">2008</option> <option value=\"2009\">2009</option> <option value=\"2010\">2010</option> <option value=\"2011\">2011</option> <option value=\"2012\">2012</option> <option value=\"2013\">2013</option> <option value=\"2014\" selected=\"\">2014</option> </select> <p> </p>\n" +
                "             <div id=\"searchText\">\n" +
                "              SELECT A MONTH\n" +
                "             </div><br /> <img src=\"images/spacer.gif\" width=\"1\" height=\"5\" border=\"0\" /><br /> <select style=\"float:right;clear:both;margin-right:8px;\" name=\"month\"> <option value=\"\">Show all months</option> <option value=\"1\">January</option> <option value=\"4\">April</option> <option value=\"5\">May</option> <option value=\"6\" selected=\"\">June</option> </select> <p></p> <p> <input type=\"image\" src=\"images/submit.gif\" border=\"0\" style=\"float:right;clear:both;margin-right:8px;margin-top:22px;margin-bottom:1px;\" /> </p> </td>\n" +
                "           </tr>\n" +
                "          </tbody>\n" +
                "         </table> </td>\n" +
                "       </tr>\n" +
                "      </tbody>\n" +
                "     </table>\n" +
                "     <a href=\"http://smarturl.it/DMBretweet\" target=\"_blank\"><img width=\"234\" height=\"87\" id=\"colLeftAd\" style=\"margin-right:8px;margin-top:5px;border:0px\" src=\"images/banners/TwitterDownload_270x100.jpg\" /></a>\n" +
                "     <a href=\"http://youtu.be/X8ljNFQNCUI\"><img width=\"234\" height=\"87\" id=\"colLeftAd\" style=\"margin-right:8px;margin-top:5px;border:0px\" src=\"images/banners/ScenesFromSA_Everyday_270x100.jpg\" /></a>\n" +
                "     <a href=\"securejump.asp?jump_id=280&amp;ReferringPage=71\" target=\"_blank\"><img width=\"234\" height=\"87\" id=\"colLeftAd\" style=\"margin-right:8px;margin-top:5px;border:0px\" src=\"images/banners/DMBLive_SaltLakeCity_270x100.png\" /></a>\n" +
                "     <br />\n" +
                "     <img src=\"images/spacer.gif\" height=\"150\" />\n" +
                "     <br />\n" +
                "     <a href=\"#top\"><img style=\"float:right;margin-right:10px;margin-top:20px;margin-bottom:12px;clear:both;\" src=\"images/backtotop.gif\" border=\"0\" /></a>\n" +
                "    </div>\n" +
                "    <div id=\"columnCenterWhiteBackstage\" style=\"width:600px;\">\n" +
                "     <title>Dave Matthews Band - Setlist</title>\n" +
                "     <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n" +
                "     <!--[if lt IE 7.]>\n" +
                "<script defer type=\"text/javascript\" src=\"pngfix.js\"></script>\n" +
                "<![endif]-->\n" +
                "     <!--  %> -->\n" +
                "     <div style=\"font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15px;\">\n" +
                "      <div style=\"padding-bottom:12px;padding-left:3px;color:#3995aa;\">\n" +
                "       Jun 28 2014\n" +
                "       <br />First Niagara Pavilion\n" +
                "       <br />Burgettstown, PA\n" +
                "      </div>\n" +
                "      <div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:470;\">\n" +
                "        10.&nbsp;&nbsp;Warehouse&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:426;\">\n" +
                "        -------- SET BREAK -------- &nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:674;\">\n" +
                "        22.&nbsp;&nbsp;Grey Street&nbsp;&Auml;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:358;\">\n" +
                "        &nbsp;5.&nbsp;&nbsp;Christmas Song&nbsp;~\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:623;\">\n" +
                "        19.&nbsp;&nbsp;Typical Situation&nbsp;&Auml;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:691;\">\n" +
                "        -------- ENCORE -------- &nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:640;\">\n" +
                "        20.&nbsp;&nbsp;Drunken Soldier&nbsp;&Auml;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#3f94aa;Position:Absolute;Top:806;\">\n" +
                "        <font style=\"font-size:19px;\">*</font>&nbsp;Dave Solo\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#3f94aa;Position:Absolute;Top:840;\">\n" +
                "        <font style=\"font-size:19px;\">~</font>&nbsp;Carter, Dave and Tim\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:392;\">\n" +
                "        &nbsp;7.&nbsp;&nbsp;Lie In Our Graves&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;\">\n" +
                "        SHOW NOTES:\n" +
                "        <br />&nbsp;\n" +
                "        <img src=\"images/setlists/NewArrow.png\" width=\"16\" height=\"9\" />&nbsp;indicates a segue into next song\n" +
                "       </div>\n" +
                "       <br />\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:290;\">\n" +
                "        &nbsp;1.&nbsp;&nbsp;Beach Ball&nbsp;*\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:555;\">\n" +
                "        15.&nbsp;&nbsp;Jimi Thing&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:735;\">\n" +
                "        24.&nbsp;&nbsp;Ants Marching&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:589;\">\n" +
                "        17.&nbsp;&nbsp;Digging a Ditch&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:504;\">\n" +
                "        12.&nbsp;&nbsp;Crush&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:487;\">\n" +
                "        11.&nbsp;&nbsp;Belly Belly Nice&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:538;\">\n" +
                "        14.&nbsp;&nbsp;If Only&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:375;\">\n" +
                "        &nbsp;6.&nbsp;&nbsp;Sweet&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:307;\">\n" +
                "        &nbsp;2.&nbsp;&nbsp;Minarets&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:718;\">\n" +
                "        23.&nbsp;&nbsp;Granny&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:453;\">\n" +
                "        &nbsp;9.&nbsp;&nbsp;Save Me&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:521;\">\n" +
                "        13.&nbsp;&nbsp;Seven&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#3f94aa;Position:Absolute;Top:823;\">\n" +
                "        <font style=\"font-size:19px;\">+</font>&nbsp;Carter and Dave\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:606;\">\n" +
                "        18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;\n" +
                "        <img src=\"images/setlists/NewArrow.png\" hspace=\"4\" />\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:409;\">\n" +
                "        &nbsp;8.&nbsp;&nbsp;Two Step&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:657;\">\n" +
                "        21.&nbsp;&nbsp;Corn Bread&nbsp;&Auml;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#3f94aa;Position:Absolute;Top:857;\">\n" +
                "        <font style=\"font-size:19px;\">&Auml;</font>&nbsp;Bela Fleck\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:341;\">\n" +
                "        &nbsp;4.&nbsp;&nbsp;Little Red Bird&nbsp;+\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">\n" +
                "        Spoon\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:572;\">\n" +
                "        16.&nbsp;&nbsp;The Riff&nbsp;\n" +
                "        <img src=\"images/setlists/NewArrow.png\" hspace=\"4\" />\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:324;\">\n" +
                "        &nbsp;3.&nbsp;&nbsp;What Would You Say&nbsp;\n" +
                "       </div>\n" +
                "       <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">\n" +
                "        So Much To Say\n" +
                "       </div>\n" +
                "      </div>\n" +
                "      <br />\n" +
                "      <p></p>\n" +
                "      <br />&nbsp;\n" +
                "      <br />\n" +
                "     </div>\n" +
                "     <div class=\"clear\"></div>\n" +
                "    </div>\n" +
                "    <div id=\"columnRight\" style=\"width:169px;\">\n" +
                "     <!-- <img id=\"colRightAd\" style=\"margin-left:0px;background-color\" src=\"images/160x600adSpace.gif\" />-->\n" +
                "    </div>\n" +
                "    <div id=\"black_solid\"></div>\n" +
                "    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "     <tbody>\n" +
                "      <tr>\n" +
                "       <td><a href=\"index.asp\" onmouseover=\"imageSwap('homeBottom','images/NewNav/Home_on.gif');\" onmouseout=\"imageSwap('homeBottom','images/NewNav/Home_off.gif');\"><img src=\"images/NewNav/Home_off.gif\" name=\"homeBottom\" width=\"61\" height=\"32\" border=\"0\" alt=\"\" /></a></td>\n" +
                "       <td><a href=\"news.asp\" onmouseover=\"imageSwap('newsBottom','images/NewNav/News_on.gif');\" onmouseout=\"imageSwap('newsBottom','images/NewNav/News_off.gif');\"><img src=\"images/NewNav/News_off.gif\" name=\"newsBottom\" width=\"58\" height=\"32\" border=\"0\" alt=\"News\" /></a></td>\n" +
                "       <td><a href=\"ticketing.asp\" onmouseover=\"imageSwap('ticketingBottom','images/NewNav/Ticketing_on.gif');\" onmouseout=\"imageSwap('ticketingBottom','images/NewNav/Ticketing_off.gif');\"><img src=\"images/NewNav/Ticketing_off.gif\" name=\"ticketingBottom\" width=\"90\" height=\"32\" border=\"0\" alt=\"Ticketing\" /></a></td>\n" +
                "       <td><a href=\"backstage.asp\"><img src=\"images/NewNav/Backstage_on.gif\" name=\"backstageBottom\" width=\"104\" height=\"32\" border=\"0\" alt=\"Backstage\" /></a></td>\n" +
                "       <td><a href=\"/securejump.asp?jump_id=116\" onmouseover=\"imageSwap('audioBottom','images/NewNav/Audio_on.gif');\" onmouseout=\"imageSwap('audioBottom','images/NewNav/Audio_off.gif');\" target=\"_blank\"><img src=\"images/NewNav/Audio_off.gif\" name=\"audioBottom\" width=\"66\" height=\"32\" border=\"0\" alt=\"Audio\" /></a></td>\n" +
                "       <td><a href=\"Connect.asp\" onmouseover=\"imageSwap('connectBottom','images/NewNav/Connect_on.gif');\" onmouseout=\"imageSwap('connectBottom','images/NewNav/Connect_off.gif');\"><img src=\"images/NewNav/Connect_off.gif\" name=\"connectBottom\" width=\"90\" height=\"32\" border=\"0\" alt=\"Gallery\" /></a></td>\n" +
                "       <td><a href=\"fanworks.asp\" onmouseover=\"imageSwap('fanworksBottom','images/NewNav/FanWorks_on.gif');\" onmouseout=\"imageSwap('fanworksBottom','images/NewNav/FanWorks_off.gif');\"><img src=\"images/NewNav/FanWorks_off.gif\" name=\"fanworksBottom\" width=\"101\" height=\"32\" border=\"0\" alt=\"Fan Works\" /></a></td>\n" +
                "       <td><a href=\"contests.asp\" onmouseover=\"imageSwap('contestsBottom','images/NewNav/Contests_on.gif');\" onmouseout=\"imageSwap('contestsBottom','images/NewNav/Contests_off.gif');\"><img src=\"images/NewNav/Contests_off.gif\" name=\"contestsBottom\" width=\"89\" height=\"32\" border=\"0\" alt=\"Contests\" /></a></td>\n" +
                "       <td><a href=\"securejump.asp?jump_id=2\" onmouseover=\"imageSwap('storeBottom','images/NewNav/Store_on.gif');\" onmouseout=\"imageSwap('storeBottom','images/NewNav/Store_off.gif');\" target=\"_blank\"><img src=\"images/NewNav/Store_off.gif\" name=\"storeBottom\" width=\"61\" height=\"32\" border=\"0\" alt=\"Store\" /></a></td>\n" +
                "       <td><a href=\"boards.asp\" onmouseover=\"imageSwap('boardsBottom','images/NewNav/Boards_on.gif');\" onmouseout=\"imageSwap('boardsBottom','images/NewNav/Boards_off.gif');\"><img src=\"images/NewNav/Boards_off.gif\" name=\"boardsBottom\" width=\"79\" height=\"32\" border=\"0\" alt=\"Boards\" /></a></td>\n" +
                "       <td><a href=\"memberinfo.asp\" onmouseover=\"imageSwap('memberinfoBottom','images/NewNav/MemberInfo_on.gif');\" onmouseout=\"imageSwap('memberinfoBottom','images/NewNav/MemberInfo_off.gif');\"><img src=\"images/NewNav/MemberInfo_off.gif\" name=\"memberinfoBottom\" width=\"112\" height=\"32\" border=\"0\" alt=\"Member Info\" /></a></td>\n" +
                "      </tr>\n" +
                "     </tbody>\n" +
                "    </table>\n" +
                "   </div>\n" +
                "  </div>\n" +
                "  <div id=\"bottomNavBar\">\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp#contact\">Contact Info</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp?CurrentPage=Legal\">Legal Policies</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp?CurrentPage=Agreement\">User Agreement</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp?CurrentPage=LegalPolicies#copy\">Copyright</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp?CurrentPage=PrivacyPolicies\">Privacy Policy</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"logoff.asp\">Log Off</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ï¿½Bama Rags 1991-2014 &nbsp;&nbsp; Powered by MusicTodayï¿½\n" +
                "  </div>\n" +
                " </div>\n" +
                "</body>"));

        TestObserver<Boolean> testObserver = Observable.just(yourStepsBean)
                .flatMap(foreverOnlyKnowingFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(Boolean.TRUE);
    }

    @Test
    public void verifyRight2() throws Exception {
        YourStepsBean yourStepsBean = new YourStepsBean(Jsoup.parse("<body bgcolor=\"#eeeeee\" background=\"images/background.jpg\">\n" +
                " <div id=\"pageWrapper\">\n" +
                "  <!-- Begin Header Area Including Top Nav -->\n" +
                "  <table bgcolor=\"#000000\" width=\"927\" height=\"113\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"Table1\">\n" +
                "   <tbody>\n" +
                "    <tr>\n" +
                "     <th rowspan=\"3\"> <img src=\"images/header_marginLeft.gif\" width=\"8\" height=\"113\" alt=\"\" /></th>\n" +
                "     <td><img src=\"images/header_marginTop.gif\" width=\"911\" height=\"6\" alt=\"\" /></td>\n" +
                "     <td rowspan=\"3\"><img src=\"images/header_marginRight.gif\" width=\"8\" height=\"113\" alt=\"\" /></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td>\n" +
                "      <table bgcolor=\"#000000\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"Table2\">\n" +
                "       <tbody>\n" +
                "        <tr>\n" +
                "         <td><img src=\"images/header_logo.gif\" width=\"357\" height=\"75\" alt=\"\" /></td>\n" +
                "         <td background=\"images/header_adSpace.png\" width=\"419\" height=\"75\" valign=\"top\"><a href=\"https://davematthewsband.desk.com\" target=\"_blank\"><img style=\"padding-left:200px;\" src=\"images/newnav/Top_SupportNav\n" +
                ".jpg\" border=\"0\" /></a></td>\n" +
                "         <td id=\"loginArea\">\n" +
                "          <div id=\"loginText\">\n" +
                "           Welcome fateman\n" +
                "           <br /> exp: 04.30.2018\n" +
                "           <br />\n" +
                "           <a href=\"memberinfo.asp?CurrentPage=MyInfo\">My Info</a>\n" +
                "           <br />\n" +
                "           <a href=\"logoff.asp\">Logout</a>\n" +
                "          </div> </td>\n" +
                "        </tr>\n" +
                "       </tbody>\n" +
                "      </table> </td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td bgcolor=\"#666666\">\n" +
                "      <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "       <tbody>\n" +
                "        <tr>\n" +
                "         <td><a href=\"index.asp\" onmouseover=\"imageSwap('home','images/NewNav/Home_on.gif');\" onmouseout=\"imageSwap('home','images/NewNav/Home_off.gif');\"><img src=\"images/NewNav/Home_off.gif\" name=\"home\" width=\"61\" height=\"32\" border=\"0\" alt=\"\" /></a></td>\n" +
                "         <td><a href=\"news.asp\" onmouseover=\"imageSwap('news','images/NewNav/News_on.gif');\" onmouseout=\"imageSwap('news','images/NewNav/News_off.gif');\"><img src=\"images/NewNav/News_off.gif\" name=\"news\" width=\"58\" height=\"32\" border=\"0\" alt=\"News\" /></a></td>\n" +
                "         <td><a href=\"ticketing.asp\" onmouseover=\"imageSwap('ticketing','images/NewNav/Ticketing_on.gif');\" onmouseout=\"imageSwap('ticketing','images/NewNav/Ticketing_off.gif');\"><img src=\"images/NewNav/Ticketing_off.gif\" name=\"ticketing\" width=\"90\" height=\"32\" border=\"0\" alt=\"Ticketing\" /></a></td>\n" +
                "         <td><a href=\"backstage.asp\"><img src=\"images/NewNav/Backstage_on.gif\" name=\"backstage\" width=\"104\" height=\"32\" border=\"0\" alt=\"Backstage\" /></a></td>\n" +
                "         <td><a href=\"/securejump.asp?jump_id=116\" onmouseover=\"imageSwap('audio','images/NewNav/Audio_on.gif');\" onmouseout=\"imageSwap('audio','images/NewNav/Audio_off.gif');\" target=\"_blank\"><img src=\"images/NewNav/Audio_off.gif\" name=\"audio\" width=\"66\" height=\"32\" border=\"0\" alt=\"Audio\" /></a></td>\n" +
                "         <td><a href=\"Connect.asp\" onmouseover=\"imageSwap('connect','images/NewNav/Connect_on.gif');\" onmouseout=\"imageSwap('connect','images/NewNav/Connect_off.gif');\"><img src=\"images/NewNav/Connect_off.gif\" name=\"connect\" width=\"90\" height=\"32\" border=\"0\" alt=\"Gallery\" /></a></td>\n" +
                "         <td><a href=\"Photos.asp\" onmouseover=\"imageSwap('Photos','images/NewNav/Photos_on.gif');\" onmouseout=\"imageSwap('Photos','images/NewNav/Photos_off.gif');\"><img src=\"images/NewNav/Photos_off.gif\" name=\"Photos\" width=\"101\" height=\"32\" border=\"0\" alt=\"Photo Download\" /></a></td>\n" +
                "         <td><a href=\"contests.asp\" onmouseover=\"imageSwap('contests','images/NewNav/Contests_on.gif');\" onmouseout=\"imageSwap('contests','images/NewNav/Contests_off.gif');\"><img src=\"images/NewNav/Contests_off.gif\" name=\"contests\" width=\"89\" height=\"32\" border=\"0\" alt=\"Contests\" /></a></td>\n" +
                "         <td><a href=\"securejump.asp?jump_id=2\" onmouseover=\"imageSwap('store','images/NewNav/Store_on.gif');\" onmouseout=\"imageSwap('store','images/NewNav/Store_off.gif');\" target=\"_blank\"><img src=\"images/NewNav/Store_off.gif\" name=\"store\" width=\"61\" height=\"32\" border=\"0\" alt=\"Store\" /></a></td>\n" +
                "         <td><a href=\"boards.asp\" onmouseover=\"imageSwap('boards','images/NewNav/Boards_on.gif');\" onmouseout=\"imageSwap('boards','images/NewNav/Boards_off.gif');\"><img src=\"images/NewNav/Boards_off.gif\" name=\"boards\" width=\"79\" height=\"32\" border=\"0\" alt=\"Boards\" /></a></td>\n" +
                "         <td><a href=\"memberinfo.asp\" onmouseover=\"imageSwap('memberinfo','images/NewNav/MemberInfo_on.gif');\" onmouseout=\"imageSwap('memberinfo','images/NewNav/MemberInfo_off.gif');\"><img src=\"images/NewNav/MemberInfo_off.gif\" name=\"memberinfo\" width=\"112\" height=\"32\" border=\"0\" alt=\"Member Info\" /></a></td>\n" +
                "        </tr>\n" +
                "       </tbody>\n" +
                "      </table></td>\n" +
                "    </tr>\n" +
                "   </tbody>\n" +
                "  </table>\n" +
                "  <!-- End Header Area -->\n" +
                "  <!-- Begin Main Content Area -->\n" +
                "  <div id=\"bodyTableBorder\">\n" +
                "   <div id=\"bodyTableContents\">\n" +
                "    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"925\" height=\"94\" style=\"background-image:url('images/backstage_head.jpg');\">\n" +
                "     <tbody>\n" +
                "      <tr>\n" +
                "       <td valign=\"top\"> &nbsp; </td>\n" +
                "      </tr>\n" +
                "     </tbody>\n" +
                "    </table>\n" +
                "    <a name=\"top\"></a>\n" +
                "    <div id=\"columnLeftGray\" style=\"width:250px;\">\n" +
                "     <img style=\"margin-left:49px;margin-top:69px;\" src=\"images/setlist_search.gif\" />\n" +
                "     <img src=\"images/spacer.gif\" border=\"0\" height=\"3\" width=\"1\" />\n" +
                "     <br />\n" +
                "     <table border=\"0\" cellpadding=\"14\" cellspacing=\"0\">\n" +
                "      <tbody>\n" +
                "       <tr>\n" +
                "        <td valign=\"top\" align=\"left\">\n" +
                "         <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
                "          <tbody>\n" +
                "           <tr>\n" +
                "            <form action=\"/backstage.asp\" method=\"post\"></form>\n" +
                "            <td align=\"right\" class=\"newsDate\" width=\"290\"><img src=\"images/spacer.gif\" width=\"1\" height=\"11\" border=\"0\" /><br />\n" +
                "             <div id=\"searchText\">\n" +
                "              SELECT A YEAR\n" +
                "             </div><br /> <img src=\"images/spacer.gif\" width=\"1\" height=\"5\" border=\"0\" /><br /> <select style=\"float:right;clear:both;margin-right:8px;\" name=\"year\"> <option value=\"\">----</option> <option value=\"199\n" +
                "1\">1991</option> <option value=\"1992\">1992</option> <option value=\"1993\">1993</option> <option value=\"1994\">1994</option> <option value=\"1995\">1995</option> <option value=\"1996\">1996</option> <option value=\"1997\">19\n" +
                "97</option> <option value=\"1998\">1998</option> <option value=\"1999\">1999</option> <option value=\"2000\">2000</option> <option value=\"2001\">2001</option> <option value=\"2002\">2002</option> <option value=\"2003\">2003</o\n" +
                "ption> <option value=\"2004\">2004</option> <option value=\"2005\">2005</option> <option value=\"2006\">2006</option> <option value=\"2007\">2007</option> <option value=\"2008\">2008</option> <option value=\"2009\">2009</option\n" +
                "> <option value=\"2010\">2010</option> <option value=\"2011\">2011</option> <option value=\"2012\">2012</option> <option value=\"2013\">2013</option> <option value=\"2014\">2014</option> <option value=\"2015\">2015</option> <op\n" +
                "tion value=\"2016\">2016</option> <option value=\"2017\" selected=\"\">2017</option> </select> <p> </p>\n" +
                "             <div id=\"searchText\">\n" +
                "              SELECT A MONTH\n" +
                "             </div><br /> <img src=\"images/spacer.gif\" width=\"1\" height=\"5\" border=\"0\" /><br /> <select style=\"float:right;clear:both;margin-right:8px;\" name=\"month\"> <option value=\"\">Show all months</option> <optio\n" +
                "n value=\"1\">January</option> <option value=\"2\">February</option> <option value=\"3\">March</option> <option value=\"4\">April</option> <option value=\"5\" selected=\"\">May</option> </select> <p></p> <p> <input type=\"image\"\n" +
                " src=\"images/submit.gif\" border=\"0\" style=\"float:right;clear:both;margin-right:8px;margin-top:22px;margin-bottom:1px;\" /> </p> </td>\n" +
                "           </tr>\n" +
                "          </tbody>\n" +
                "         </table> </td>\n" +
                "       </tr>\n" +
                "      </tbody>\n" +
                "     </table>\n" +
                "     <a href=\"securejump.asp?jump_id=381\" target=\"_blank\"><img width=\"234\" height=\"87\" id=\"colLeftAd\" style=\"margin-right:8px;margin-top:5px;border:0px\" src=\"images/banners/Vol41_270x100.jpg\" /></a>\n" +
                "     <a href=\"securejump.asp?jump_id=383\"><img width=\"234\" height=\"87\" id=\"colLeftAd\" style=\"margin-right:8px;margin-top:5px;border:0px\" src=\"images/banners/CincodeMayo-270x100.jpg\" /></a>\n" +
                "     <a href=\"ticketing_request.asp\" target=\"_blank\"><img width=\"234\" height=\"87\" id=\"colLeftAd\" style=\"margin-right:8px;margin-top:5px;border:0px\" src=\"images/banners/2017tickets270x100px.jpeg\" /></a>\n" +
                "     <br />\n" +
                "     <img src=\"images/spacer.gif\" height=\"150\" />\n" +
                "     <br />\n" +
                "     <a href=\"#top\"><img style=\"float:right;margin-right:10px;margin-top:20px;margin-bottom:12px;clear:both;\" src=\"images/backtotop.gif\" border=\"0\" /></a>\n" +
                "    </div>\n" +
                "    <div id=\"columnCenterWhiteBackstage\" style=\"width:600px;\">\n" +
                "     <title>Dave Matthews Band - Setlist</title>\n" +
                "     <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n" +
                "     <!--[if lt IE 7.]>\n" +
                "<script defer type=\"text/javascript\" src=\"pngfix.js\"></script>\n" +
                "<![endif]-->\n" +
                "     <!--  %> -->\n" +
                "     <div style=\"font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15px;\">\n" +
                "      <div style=\"padding-bottom:12px;padding-left:3px;color:#3995aa;\">\n" +
                "       Dave Matthews and Tim Reynolds\n" +
                "       <br />May 6 2017\n" +
                "       <br />Ascend Amphitheater\n" +
                "       <br />Nashville, TN\n" +
                "      </div> &nbsp;1.&nbsp;&nbsp;Take Me To Tomorrow&nbsp;\n" +
                "      <br />&nbsp;2.&nbsp;&nbsp;Bartender&nbsp;\n" +
                "      <br />&nbsp;3.&nbsp;&nbsp;When The World Ends&nbsp;\n" +
                "      <br />&nbsp;4.&nbsp;&nbsp;Satellite&nbsp;\n" +
                "      <br />\n" +
                "      <br />\n" +
                "      <p></p>\n" +
                "      <br />&nbsp;\n" +
                "      <br />\n" +
                "     </div>\n" +
                "     <div class=\"clear\"></div>\n" +
                "    </div>\n" +
                "    <div id=\"columnRight\" style=\"width:169px;\">\n" +
                "     <!-- <img id=\"colRightAd\" style=\"margin-left:0px;background-color\" src=\"images/160x600adSpace.gif\" />-->\n" +
                "    </div>\n" +
                "    <div id=\"black_solid\"></div>\n" +
                "    <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "     <tbody>\n" +
                "      <tr>\n" +
                "       <td><a href=\"index.asp\" onmouseover=\"imageSwap('homeBottom','images/NewNav/Home_on.gif');\" onmouseout=\"imageSwap('homeBottom','images/NewNav/Home_off.gif');\"><img src=\"images/NewNav/Home_off.gif\" name=\"homeBo\n" +
                "ttom\" width=\"61\" height=\"32\" border=\"0\" alt=\"\" /></a></td>\n" +
                "       <td><a href=\"news.asp\" onmouseover=\"imageSwap('newsBottom','images/NewNav/News_on.gif');\" onmouseout=\"imageSwap('newsBottom','images/NewNav/News_off.gif');\"><img src=\"images/NewNav/News_off.gif\" name=\"newsBot\n" +
                "tom\" width=\"58\" height=\"32\" border=\"0\" alt=\"News\" /></a></td>\n" +
                "       <td><a href=\"ticketing.asp\" onmouseover=\"imageSwap('ticketingBottom','images/NewNav/Ticketing_on.gif');\" onmouseout=\"imageSwap('ticketingBottom','images/NewNav/Ticketing_off.gif');\"><img src=\"images/NewNav/Ti\n" +
                "cketing_off.gif\" name=\"ticketingBottom\" width=\"90\" height=\"32\" border=\"0\" alt=\"Ticketing\" /></a></td>\n" +
                "       <td><a href=\"backstage.asp\"><img src=\"images/NewNav/Backstage_on.gif\" name=\"backstageBottom\" width=\"104\" height=\"32\" border=\"0\" alt=\"Backstage\" /></a></td>\n" +
                "       <td><a href=\"/securejump.asp?jump_id=116\" onmouseover=\"imageSwap('audioBottom','images/NewNav/Audio_on.gif');\" onmouseout=\"imageSwap('audioBottom','images/NewNav/Audio_off.gif');\" target=\"_blank\"><img src=\"im\n" +
                "ages/NewNav/Audio_off.gif\" name=\"audioBottom\" width=\"66\" height=\"32\" border=\"0\" alt=\"Audio\" /></a></td>\n" +
                "       <td><a href=\"Connect.asp\" onmouseover=\"imageSwap('connectBottom','images/NewNav/Connect_on.gif');\" onmouseout=\"imageSwap('connectBottom','images/NewNav/Connect_off.gif');\"><img src=\"images/NewNav/Connect_off.\n" +
                "gif\" name=\"connectBottom\" width=\"90\" height=\"32\" border=\"0\" alt=\"Gallery\" /></a></td>\n" +
                "       <td><a href=\"Photos.asp\" onmouseover=\"imageSwap('PhotosBottom','images/NewNav/Photos_on.gif');\" onmouseout=\"imageSwap('PhotosBottom','images/NewNav/Photos_off.gif');\"><img src=\"images/NewNav/Photos_off.gif\" n\n" +
                "ame=\"PhotosBottom\" width=\"101\" height=\"32\" border=\"0\" alt=\"Photo Download\" /></a></td>\n" +
                "       <td><a href=\"contests.asp\" onmouseover=\"imageSwap('contestsBottom','images/NewNav/Contests_on.gif');\" onmouseout=\"imageSwap('contestsBottom','images/NewNav/Contests_off.gif');\"><img src=\"images/NewNav/Contest\n" +
                "s_off.gif\" name=\"contestsBottom\" width=\"89\" height=\"32\" border=\"0\" alt=\"Contests\" /></a></td>\n" +
                "       <td><a href=\"securejump.asp?jump_id=2\" onmouseover=\"imageSwap('storeBottom','images/NewNav/Store_on.gif');\" onmouseout=\"imageSwap('storeBottom','images/NewNav/Store_off.gif');\" target=\"_blank\"><img src=\"image\n" +
                "s/NewNav/Store_off.gif\" name=\"storeBottom\" width=\"61\" height=\"32\" border=\"0\" alt=\"Store\" /></a></td>\n" +
                "       <td><a href=\"boards.asp\" onmouseover=\"imageSwap('boardsBottom','images/NewNav/Boards_on.gif');\" onmouseout=\"imageSwap('boardsBottom','images/NewNav/Boards_off.gif');\"><img src=\"images/NewNav/Boards_off.gif\" n\n" +
                "ame=\"boardsBottom\" width=\"79\" height=\"32\" border=\"0\" alt=\"Boards\" /></a></td>\n" +
                "       <td><a href=\"memberinfo.asp\" onmouseover=\"imageSwap('memberinfoBottom','images/NewNav/MemberInfo_on.gif');\" onmouseout=\"imageSwap('memberinfoBottom','images/NewNav/MemberInfo_off.gif');\"><img src=\"images/NewN\n" +
                "av/MemberInfo_off.gif\" name=\"memberinfoBottom\" width=\"112\" height=\"32\" border=\"0\" alt=\"Member Info\" /></a></td>\n" +
                "      </tr>\n" +
                "     </tbody>\n" +
                "    </table>\n" +
                "   </div>\n" +
                "  </div>\n" +
                "  <div id=\"bottomNavBar\">\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp#contact\">Contact Info</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp?CurrentPage=Legal\">Legal Policies</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp?CurrentPage=Agreement\">User Agreement</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"MemberInfo.asp?CurrentPage=DMBLegal#copy\">Copyright</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"memberinfo.asp?CurrentPage=PrivacyPolicies\">Privacy Policy</a>\n" +
                "   <span id=\"bottomNavBreak\">|</span>\n" +
                "   <a id=\"bottomNavLink\" href=\"logoff.asp\">Log Off</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbs\n" +
                "p;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &copy;Bama Rags 1991-2015 &nbsp;&nbsp; Powered by MusicToday&reg;\n" +
                "  </div>\n" +
                " </div>\n" +
                "</body>"));

        TestObserver<Boolean> testObserver = Observable.just(yourStepsBean)
                .flatMap(foreverOnlyKnowingFunction)
                .test();

        testObserver.assertNoErrors();
        testObserver.assertComplete();
        testObserver.assertValueCount(1);
        testObserver.assertValue(Boolean.FALSE);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.some;

import com.foxfarmroad.illbackyouup.components.sometimes.Sometimes;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * <p>
 * </p>
 */
public class SomeTestModule extends AbstractModule {

    @Override
    protected void configure() {
    }

    @Provides
    @Sometimes
    DateTime provideNow() {
        return new DateTime().withZone(DateTimeZone.UTC);
    }

}

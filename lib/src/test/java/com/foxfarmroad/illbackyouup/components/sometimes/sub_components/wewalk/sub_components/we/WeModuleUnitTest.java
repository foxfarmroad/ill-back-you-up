package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.we;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.NonBreakingSpace;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.WeWalkModule;
import com.google.inject.Guice;
import org.junit.Test;

import javax.inject.Inject;

public class WeModuleUnitTest {

    @Inject
    @NonBreakingSpace
    private char nonBreakingSpace;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new WeModule()).injectMembers(this);
    }
}

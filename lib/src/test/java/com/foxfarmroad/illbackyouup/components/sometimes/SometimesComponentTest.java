package com.foxfarmroad.illbackyouup.components.sometimes;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.RetrofitTestModule;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Category(ComponentTest.class)
public class SometimesComponentTest {

    @Inject
    private DateTimeFormatter dateTimeFormatter;

    @Inject
    @TimeStart
    private Integer showStartHour;

    @Inject
    private SometimesFunction sometimesFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.override(new SometimesModule()).with(new SometimesTestModule(),
                new RetrofitTestModule())).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        DateTime dateTime = new DateTime()
                .withZone(DateTimeZone.UTC)
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        String showDate = dateTimeFormatter.print(dateTime);

        Document document = mock(Document.class);

        Element row = new Element("tr");
        Element one = new Element("td").attr("align", "center");
        Element date = new Element("a").appendText(showDate);
        one.appendChild(date);
        Element two = new Element("td").attr("align", "center");
        Element three = new Element("td").attr("align", "center");
        Element four = new Element("td").attr("align", "center");
        Element five = new Element("td").attr("align", "center");
        Element six = new Element("td").attr("align", "center");
        Element seven = new Element("td").attr("align", "center");
        Element eight = new Element("td").attr("align", "left");
        Element table = new Element("table");
        Element tableBody = new Element("tbody");
        Element tableRow = new Element("tr");
        Element tableCell1 = new Element("td");
        Element a = new Element("a").appendText("venue");
        tableCell1.appendChild(a);
        tableRow.appendChild(tableCell1);
        Element tableCell2 = new Element("td").attr("align", "right");
        Element span = new Element("span").appendText("showDetail");
        tableCell2.appendChild(span);
        tableRow.appendChild(tableCell2);
        tableBody.appendChild(tableRow);
        table.appendChild(tableBody);
        eight.appendChild(table);
        Element nine = new Element("td").attr("align", "right");
        Element location = new Element("span").appendText("Beaverton, OR");
        nine.appendChild(location);
        Element ten = new Element("td").attr("align", "right");

        row.appendChild(one);
        row.appendChild(two);
        row.appendChild(three);
        row.appendChild(four);
        row.appendChild(five);
        row.appendChild(six);
        row.appendChild(seven);
        row.appendChild(eight);
        row.appendChild(nine);
        row.appendChild(ten);

        Element topTable = new Element("table");
        Element tbody = new Element("tbody");
        topTable.appendChild(tbody);
        tbody.appendChild(row);

        when(document.getElementsByAttributeValue("class", "newslogcell")).thenReturn(new Elements(topTable));

        YourStepsBean yourStepsBean = new YourStepsBean(document);

        TestObserver<Long> testObserver = Observable.just(yourStepsBean)
                .flatMap(sometimesFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(dateTime.withHourOfDay(showStartHour).minusSeconds(2).getMillis());
    }
}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure;

import com.foxfarmroad.ComponentTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.ButLikeThatTouchModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown.IveKnownModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(ComponentTest.class)
public class AndForSureComponentTest {

    @Inject
    private AndForSureFunction andForSureFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new AndForSureModule(), new ButLikeThatTouchModule(), new IveKnownModule(),
                new WouldYouLikeModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
//        TestObserver<Pair<Integer, AsBean>> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:589;\">17.&nbsp;&nbsp;Digging a Ditch&nbsp;</div>").body().childNode(0))
//                .flatMap(andForSureFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertEquals(new Integer(589), testObserver.values().get(0).getKey());
//        assertEquals("Digging a Ditch", testObserver.values().get(0).getValue().getSongText());
    }

    @Test
    public void verifyRight2() throws Exception {
//        TestObserver<Pair<Integer, AsBean>> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:691;\">-------- ENCORE -------- &nbsp;</div>").body().childNode(0))
//                .flatMap(andForSureFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertEquals(new Integer(691), testObserver.values().get(0).getKey());
//        assertEquals("Encore:", testObserver.values().get(0).getValue().getSongText());
    }

    @Test
    public void verifyRight3() throws Exception {
//        TestObserver<Pair<Integer, AsBean>> testObserver = Observable.just(Jsoup.parseBodyFragment("<div style=\"Color:#000000;Position:Absolute;Top:606;\">18.&nbsp;&nbsp;Donï¿½t Drink the Water&nbsp;&Auml;\n<img src=\"images/setlists/NewArrow.png\" hspace=\"4\" /></div>").body().childNode(0))
//                .flatMap(andForSureFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertEquals(new Integer(606), testObserver.values().get(0).getKey());
//        assertEquals("Don't Drink the WaterÄ ->", testObserver.values().get(0).getValue().getSongText());
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun;

import com.foxfarmroad.ManualTest;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

/**
 * <p>
 * </p>
 */
@Category(ManualTest.class)
public class WeRunManualTest {

    @Inject
    private WeRunFunction weRunFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new WeRunModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        TestObserver<LatLngBean> testObserver = Observable.just("Beaverton, OR")
                .flatMap(weRunFunction)
                .test();

        System.out.println(testObserver.values());
    }

    @Test
    public void verifyRight2() throws Exception {
        TestObserver<LatLngBean> testObserver = Observable.just("Toronto, ON, CAN")
                .flatMap(weRunFunction)
                .test();

        System.out.println(testObserver.values());
    }

    @Test
    public void verifyRight3() throws Exception {
        TestObserver<LatLngBean> testObserver = Observable.just("Forest, BEL")
                .flatMap(weRunFunction)
                .test();

        System.out.println(testObserver.values());
    }
}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.Fast;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.How;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastEndpoint;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastModule;
import com.foxfarmroad.illbackyouup.io.HowFastBean;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AwayBiFunctionUnitTest {

    @Bind
    @How
    private String how = "";

    @Bind
    @Fast
    private String fast = "";

    @Mock
    @Bind
    private HowFastEndpoint howFastEndpoint;

    @Inject
    private AwayBiFunction awayBiFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new HowFastModule()).with(BoundFieldModule.of(this)))
                .injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        LatLngBean latLngBean = new LatLngBean();
        latLngBean.setLat(1.0f);
        latLngBean.setLng(1.0f);
        DateTime dateTime = new DateTime();
        HowFastBean howFastBean = new HowFastBean();
        howFastBean.setDstOffset(1);
        howFastBean.setRawOffset(1);

        when(howFastEndpoint.getTimezoneJson(anyString(), anyString(), anyLong()))
                .thenReturn(Observable.just(howFastBean));

        TestObserver<Integer> testObserver = Observable.zip(
                        Observable.just(latLngBean),
                        Observable.just(dateTime),
                        awayBiFunction
                ).blockingSingle()
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(2);
        verify(howFastEndpoint, times(1)).getTimezoneJson(anyString(), anyString(), anyLong());
    }
}

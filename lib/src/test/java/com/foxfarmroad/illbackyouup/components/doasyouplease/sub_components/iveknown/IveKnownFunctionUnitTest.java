package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class IveKnownFunctionUnitTest {

    @Inject
    private IveKnownFunction iveKnownFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(new IveKnownModule()).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        String divBlock = "<div style=\"padding-bottom:12px;padding-left:3px;color:#3995aa;\">\n" +
                "   Jun 28 2014\n" +
                "   <br />First Niagara Pavilion\n" +
                "   <br />Burgettstown, PA\n" +
                "  </div>";

//        TestObserver<Do> testObserver = Observable.just(Jsoup.parse(divBlock).body().child(0))
//                .flatMap(iveKnownFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertEquals(null, testObserver.values().get(0).getPerformer());
//        assertEquals("Jun 28 2014", testObserver.values().get(0).getDate());
//        assertEquals("First Niagara Pavilion", testObserver.values().get(0).getVenue());
//        assertEquals("Burgettstown, PA", testObserver.values().get(0).getLocation());
    }

    @Test
    public void verifyRight2() throws Exception {
        String divBlock = "<div style=\"padding-bottom:12px;padding-left:3px;color:#3995aa;\">\n" +
                "     Dave Matthews and Tim Reynolds\n" +
                "     <br>\n" +
                "     May 31 2017\n" +
                "     <br>\n" +
                "     Verizon Amphitheatre\n" +
                "     <br>\n" +
                "     Alpharetta, GA\n" +
                "  </div>";

//        TestObserver<Do> testObserver = Observable.just(Jsoup.parse(divBlock).body().child(0))
//                .flatMap(iveKnownFunction)
//                .test();
//
//        testObserver.assertNoErrors();
//        testObserver.assertComplete();
//        testObserver.assertValueCount(1);
//        assertEquals("Dave Matthews and Tim Reynolds", testObserver.values().get(0).getPerformer());
//        assertEquals("May 31 2017", testObserver.values().get(0).getDate());
//        assertEquals("Verizon Amphitheatre", testObserver.values().get(0).getVenue());
//        assertEquals("Alpharetta, GA", testObserver.values().get(0).getLocation());
    }

}

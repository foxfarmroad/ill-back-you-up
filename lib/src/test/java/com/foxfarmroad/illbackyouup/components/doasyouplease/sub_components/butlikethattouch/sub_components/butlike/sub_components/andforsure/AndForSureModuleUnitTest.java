package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.ButLikeThatTouchModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.SongStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown.IveKnownModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Node;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class AndForSureModuleUnitTest {

    @Inject
    @SongStyle
    private String songStyle;

    @Inject
    private Function<Node, Observable<Integer>> positionFunction;

    @Inject
    @And
    private Function<Node, Observable<String>> andFunction;

    @Inject
    @For
    private Function<Node, Observable<String>> forFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new AndForSureModule(), new ButLikeThatTouchModule(), new IveKnownModule(),
                new WouldYouLikeModule()).injectMembers(this);
    }

}

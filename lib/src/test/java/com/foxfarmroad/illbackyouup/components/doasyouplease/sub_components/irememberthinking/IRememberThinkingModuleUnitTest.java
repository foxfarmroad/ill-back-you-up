package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.irememberthinking;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.google.inject.Guice;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

@Category(UnitTest.class)
public class IRememberThinkingModuleUnitTest {

    @Inject
    @ShowNotes
    private String showNotes;

    @Inject
    private Function<String, Observable<String>> wouldYouLikeFunction;

    @Test
    public void verifyRight() throws Exception {
        Guice.createInjector(new IRememberThinkingModule(), new WouldYouLikeModule(), new WouldYouLikeModule())
                .injectMembers(this);
    }

}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.foxfarmroad.UnitTest;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import software.amazon.awssdk.services.ssm.model.ParametersFilter;
import software.amazon.awssdk.services.ssm.model.ParametersFilterKey;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@Category(UnitTest.class)
public class CollectionToParametersFilterFunctionTest {

    @Inject
    private CollectionToParametersFilterFunction collectionParametersFilterFunction;

    @Before
    public void setUp() throws Exception {
        Guice.createInjector(Modules.EMPTY_MODULE).injectMembers(this);
    }

    @Test
    public void verifyRight() throws Exception {
        Collection<String> stringCollection = new ArrayList<>();
        stringCollection.add("test");

        TestObserver<ParametersFilter> testObserver = Observable.just(stringCollection)
                .map(collectionParametersFilterFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        assertEquals(ParametersFilterKey.NAME.toString(), testObserver.values().get(0).keyString());
        assertEquals(stringCollection, testObserver.values().get(0).values());
    }

    @Test
    public void verifyB() throws Exception {
        Collection<String> stringCollection = new ArrayList<>();

        TestObserver<ParametersFilter> testObserver = Observable.just(stringCollection)
                .map(collectionParametersFilterFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertValueCount(1);
        assertEquals(ParametersFilterKey.NAME.toString(), testObserver.values().get(0).keyString());
        assertEquals(stringCollection, testObserver.values().get(0).values());
    }

}

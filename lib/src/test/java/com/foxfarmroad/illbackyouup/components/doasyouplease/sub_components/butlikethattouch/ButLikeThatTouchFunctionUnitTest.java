package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch;

import com.foxfarmroad.UnitTest;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.Guice;
import com.google.inject.testing.fieldbinder.Bind;
import com.google.inject.testing.fieldbinder.BoundFieldModule;
import com.google.inject.util.Modules;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@Category(UnitTest.class)
public class ButLikeThatTouchFunctionUnitTest {

    @Mock
    @Bind
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Mock
    @Bind
    private Function<YourStepsBean, Observable<Boolean>> foreverOnlyKnowingFunction;

    @Mock
    @Bind
    @ButLike
    private Function<Node, Observable<List<String>>> butLikeFunction;

    @Mock
    @Bind
    @ThatTouch
    private Function<Node, Observable<Node>> thatTouchFunction;

    @Mock
    @Bind
    private BiFunction<List<String>, Node, List<String>> touchBiFunction;

    @Inject
    private ButLikeThatTouchFunction butLikeThatTouchFunction;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Guice.createInjector(Modules.override(new ButLikeThatTouchModule(), new WouldYouLikeModule())
                .with(BoundFieldModule.of(this))).injectMembers(this);
    }

    @Test
    public void verifyRight1() throws Exception {
        Document document = mock(Document.class);
        YourStepsBean yourStepsBean = new YourStepsBean(document);
        Node node = mock(Node.class);
        when(butIKnowFunction.apply(document)).thenReturn(Observable.just(node));
        when(foreverOnlyKnowingFunction.apply(yourStepsBean)).thenReturn(Observable.just(Boolean.TRUE));
        List<String> list = new ArrayList<>();
        when(butLikeFunction.apply(node)).thenReturn(Observable.just(list));

        TestObserver<List<String>> testObserver = Observable.just(yourStepsBean)
                .flatMap(butLikeThatTouchFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(list);
        verify(butIKnowFunction, times(1)).apply(document);
        verifyNoMoreInteractions(butIKnowFunction);
        verify(foreverOnlyKnowingFunction, times(1)).apply(yourStepsBean);
        verifyNoMoreInteractions(foreverOnlyKnowingFunction);
        verify(butLikeFunction, times(1)).apply(any());
        verifyNoMoreInteractions(butLikeFunction);
        verifyZeroInteractions(thatTouchFunction, touchBiFunction);
    }

    @Test
    public void verifyRight2() throws Exception {
        YourStepsBean yourStepsBean = mock(YourStepsBean.class);
        Document document = mock(Document.class);
        when(yourStepsBean.getBody()).thenReturn(document);
        Node node = mock(Node.class);
        when(butIKnowFunction.apply(document)).thenReturn(Observable.just(node));
        when(foreverOnlyKnowingFunction.apply(yourStepsBean)).thenReturn(Observable.just(Boolean.FALSE));
        when(thatTouchFunction.apply(any())).thenReturn(Observable.just(node));
        List<String> list = new ArrayList<>();
        when(touchBiFunction.apply(any(), any())).thenReturn(list);

        TestObserver<List<String>> testObserver = Observable.just(yourStepsBean)
                .flatMap(butLikeThatTouchFunction)
                .test();

        testObserver.awaitTerminalEvent();
        testObserver.assertResult(list);
        verify(butIKnowFunction, times(1)).apply(document);
        verifyNoMoreInteractions(butIKnowFunction);
        verify(foreverOnlyKnowingFunction, times(1)).apply(yourStepsBean);
        verifyNoMoreInteractions(foreverOnlyKnowingFunction);
        verify(butLikeFunction, times(0)).apply(any());
        verifyNoMoreInteractions(butLikeFunction);
        verify(thatTouchFunction, times(1)).apply(any());
        verifyNoMoreInteractions(thatTouchFunction);
        verify(touchBiFunction, times(1)).apply(any(), any());
        verifyNoMoreInteractions(touchBiFunction);
    }

}

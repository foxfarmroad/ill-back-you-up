package com.foxfarmroad;

import com.foxfarmroad.illbackyouup.components.hardtoremember.ConfigurationToImmutableConfigurationObservableFunction;
import com.foxfarmroad.illbackyouup.components.hardtoremember.FileBasedBuilderParametersToPropertiesConfigurationObservableFunction;
import com.foxfarmroad.illbackyouup.components.hardtoremember.FileNameToFileBasedBuilderParametersObservableFunction;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.configuration2.BaseConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ImmutableConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.FileBasedBuilderParameters;

public class ConfigOverridesModule extends AbstractModule {

    @Override
    public void configure() {
        requireBinding(FileNameToFileBasedBuilderParametersObservableFunction.class);
        requireBinding(FileBasedBuilderParametersToPropertiesConfigurationObservableFunction.class);
        requireBinding(ConfigurationToImmutableConfigurationObservableFunction.class);

        bind(new TypeLiteral<Function<String, Observable<FileBasedBuilderParameters>>>() {
        }).to(FileNameToFileBasedBuilderParametersObservableFunction.class);
        bind(new TypeLiteral<Function<FileBasedBuilderParameters, Observable<PropertiesConfiguration>>>() {
        }).to(FileBasedBuilderParametersToPropertiesConfigurationObservableFunction.class);
        bind(new TypeLiteral<Function<Configuration, Observable<ImmutableConfiguration>>>() {
        }).to(ConfigurationToImmutableConfigurationObservableFunction.class);
    }

    @Provides
    ImmutableConfiguration provideConfigOverridesConfiguration(
            FileNameToFileBasedBuilderParametersObservableFunction fileNameToFileBasedBuilderParametersObservableFunction,
            FileBasedBuilderParametersToPropertiesConfigurationObservableFunction fileBasedBuilderParametersToPropertiesConfigurationObservableFunction,
            ConfigurationToImmutableConfigurationObservableFunction configurationToImmutableConfigurationObservableFunction) {
        return Observable.just("config.properties")
                .flatMap(fileNameToFileBasedBuilderParametersObservableFunction)
                .flatMap(fileBasedBuilderParametersToPropertiesConfigurationObservableFunction)
                .flatMap(configurationToImmutableConfigurationObservableFunction)
                .blockingSingle(new BaseConfiguration());
    }
}

package com.foxfarmroad.illbackyouup.retrofit;

import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.internal.http.RealResponseBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

/**
 * From https://gist.github.com/autovalue/abe27912026ea5bf195187825fbd5ee2
 *
 * Heavily based off @NightlyNexus awesome work - https://github.com/square/retrofit/issues/2267#issuecomment-291915308
 * The converter must be the first entry in your {@link Retrofit.Builder#addCallAdapterFactory} call.
 */
public final class ConverterCallAdapterFactory extends CallAdapter.Factory {

    public interface ResponseBodyConverter<T> {
        T convert(Request request, ResponseBody body) throws IOException;

        interface Factory {
            ResponseBodyConverter<?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit);
        }
    }

    private final ResponseBodyConverter.Factory converterFactory;

    public ConverterCallAdapterFactory(ResponseBodyConverter.Factory converterFactory) {
        this.converterFactory = converterFactory;
    }

    @Override
    public CallAdapter<?, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        // noinspection unchecked
        final CallAdapter<Object, ?> delegate =
                (CallAdapter<Object, ?>) retrofit.nextCallAdapter(this, returnType, annotations);
        // noinspection unchecked
        final CallAdapter<Object, Call<ResponseBody>> bodyCallAdapter =
                (CallAdapter<Object, Call<ResponseBody>>) retrofit.nextCallAdapter(this,
                        new ParameterizedType() {
                            @Override
                            public Type[] getActualTypeArguments() {
                                return new Type[]{ResponseBody.class};
                            }

                            @Override
                            public Type getRawType() {
                                return Call.class;
                            }

                            @Override
                            public Type getOwnerType() {
                                return null;
                            }
                        }, annotations);
        final Type responseType = delegate.responseType();
        // noinspection unchecked
        final ResponseBodyConverter<Object> converter =
                (ResponseBodyConverter<Object>) converterFactory.responseBodyConverter(responseType, annotations,
                        retrofit);
        return new CallAdapter<Object, Object>() {
            @Override
            public Type responseType() {
                return responseType;
            }

            @Override
            public Object adapt(Call<Object> call) {
                return delegate.adapt(new ConverterCall<>(bodyCallAdapter.adapt(call), converter));
            }
        };
    }

    private static final class ConverterCall<T> implements Call<T> {
        private final Call<ResponseBody> delegate;
        final ResponseBodyConverter<T> converter;

        ConverterCall(Call<ResponseBody> delegate, ResponseBodyConverter<T> converter) {
            this.delegate = delegate;
            this.converter = converter;
        }

        @Override
        public Response<T> execute() throws IOException {
            Response<ResponseBody> response = delegate.execute();
            okhttp3.Response raw = response.raw();
            if (raw.isSuccessful()) {
                return Response.success(converter.convert(delegate.request(), response.body()), raw);
            }
            return Response.error(Optional.ofNullable(raw.body())
                    .orElse(new RealResponseBody(raw.headers(), new Buffer())), raw);
        }

        @Override
        public void enqueue(final Callback<T> callback) {
            delegate.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    okhttp3.Response raw = response.raw();
                    Response<T> converted;
                    if (raw.isSuccessful()) {
                        try {
                            converted = Response.success(converter.convert(call.request(), response.body()), raw);
                        } catch (IOException e) {
                            callback.onFailure(ConverterCall.this, e);
                            return;
                        }
                    } else {
                        converted = Response.error(Optional.ofNullable(raw.body())
                                .orElse(new RealResponseBody(raw.headers(), new Buffer())), raw);
                    }
                    callback.onResponse(ConverterCall.this, converted);
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    callback.onFailure(ConverterCall.this, t);
                }
            });
        }

        @Override
        public boolean isExecuted() {
            return delegate.isExecuted();
        }

        @Override
        public void cancel() {
            delegate.cancel();
        }

        @Override
        public boolean isCanceled() {
            return delegate.isCanceled();
        }

        @Override
        public Call<T> clone() {
            return new ConverterCall<>(delegate.clone(), converter);
        }

        @Override
        public Request request() {
            return delegate.request();
        }
    }
}
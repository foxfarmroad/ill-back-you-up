package com.foxfarmroad.illbackyouup.retrofit;

import org.jsoup.Jsoup;
import retrofit2.Retrofit;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class JsoupResponseBodyConverterFactory implements ConverterCallAdapterFactory.ResponseBodyConverter.Factory {

    @Override
    public ConverterCallAdapterFactory.ResponseBodyConverter<?> responseBodyConverter(
            final Type type, Annotation[] annotations, Retrofit retrofit) {
        return (request, body) -> Jsoup.parse(body.string(), request.url().toString());
    }

}
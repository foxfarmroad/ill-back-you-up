package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ConfigurationUtils;
import org.apache.commons.configuration2.ImmutableConfiguration;

public class ConfigurationToImmutableConfigurationObservableFunction implements Function<Configuration, Observable<ImmutableConfiguration>> {

    @Override
    public Observable<ImmutableConfiguration> apply(Configuration configuration) {
        return Observable.just(configuration)
                .map(ConfigurationUtils::unmodifiableConfiguration);
    }

}

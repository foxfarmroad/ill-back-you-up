package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.util.Optional;

/**
 * <p>
 *     Emits an {@link Observable observable} of the nodes inside the setlist block.
 * </p>
 *
 */
public class ButIKnowFunction implements Function<Document, Observable<Node>> {

    @Inject
    @SetlistStyle
    private String setlistStyle;

    @Override
    public Observable<Node> apply(Document document) throws Exception {
        return Observable.just(document)
                .map(doc -> doc.body().getElementsByAttributeValue("style", setlistStyle))
                .map(elements -> Optional.ofNullable(elements.first()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .flatMapIterable(Element::childNodes);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow;

import com.google.inject.AbstractModule;

public class ButIKnowModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(SetlistStyle.class)
                .to("font-family:sans-serif;font-size:14;font-weight:normal;margin-top:15px;margin-left:15px;");
    }

}

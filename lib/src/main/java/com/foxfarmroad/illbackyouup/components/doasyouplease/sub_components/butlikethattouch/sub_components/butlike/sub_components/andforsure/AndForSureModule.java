package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.SongStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.and.AndFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.and.AndModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.sure.SureFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.sure.SureModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.position.PositionFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.position.PositionModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Node;

public class AndForSureModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(PositionFunction.class);
        requireBinding(AndFunction.class);
        requireBinding(SureFunction.class);

        install(new PositionModule());
        install(new AndModule());
        install(new SureModule());
        install(new TheTouchOfYouModule());

        bindConstant().annotatedWith(SongStyle.class).to("Color:#000000;Position:Absolute;Top:");

        bind(new TypeLiteral<Function<Node, Observable<Integer>>>() { }).to(PositionFunction.class);
        bind(new TypeLiteral<Function<Node, Observable<String>>>() { }).annotatedWith(And.class).to(AndFunction.class);
        bind(new TypeLiteral<Function<Node, Observable<String>>>() { }).annotatedWith(For.class).to(SureFunction.class);
    }

}

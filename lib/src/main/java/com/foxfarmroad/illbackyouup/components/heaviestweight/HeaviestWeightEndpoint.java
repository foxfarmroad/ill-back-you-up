package com.foxfarmroad.illbackyouup.components.heaviestweight;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * <p>
 *     Used to login to a site which uses cookies to authenticate.
 * </p>
 */
public interface HeaviestWeightEndpoint {

    @POST
    @FormUrlEncoded
    Observable<Response<ResponseBody>> login(@Url String weight, @Field("Username") String username,
                                             @Field("Password") String password,
                                             @Field("form_action") String formAction);

}

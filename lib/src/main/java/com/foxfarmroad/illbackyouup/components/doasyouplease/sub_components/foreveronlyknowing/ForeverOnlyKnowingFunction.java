package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing;

import com.foxfarmroad.illbackyouup.components.doasyouplease.StyleAttr;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.util.List;

/**
 * <p>
 *     Checks which setlist version is present, returning true for v1 and false for v2.
 * </p>
 */
public class ForeverOnlyKnowingFunction implements Function<YourStepsBean, Observable<Boolean>> {

    @Inject
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Inject
    @StyleAttr
    private String attrName;

    @Inject
    @NewNoteStyle
    private String forever;

    @Inject
    private BiFunction<Boolean, List<Node>, Boolean> foreverBiFunction;

    @Override
    public Observable<Boolean> apply(YourStepsBean yourStepsBean) throws Exception {
        return Observable.just(yourStepsBean)
                .map(YourStepsBean::getBody)
                .flatMap(butIKnowFunction)
                .filter(node -> !node.attr(attrName).equals(forever))
                .toList().toObservable()
                .reduce(Boolean.FALSE, foreverBiFunction).toObservable();
    }

}

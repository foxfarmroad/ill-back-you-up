package com.foxfarmroad.illbackyouup.components.yoursteps;

import com.foxfarmroad.illbackyouup.retrofit.ConverterCallAdapterFactory;
import com.foxfarmroad.illbackyouup.retrofit.JsoupResponseBodyConverterFactory;
import com.foxfarmroad.illbackyouup.retrofit.ResponseBodyConverter;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/**
 * <p>
 *     Components utilizing this component should bind constants for <pre>baseUrl</pre> and <pre>path</pre> variables.
 * </p>
 */
public class YourStepsModule extends AbstractModule {

    @Override
    protected void configure() {
    }

    @Provides
    @YourSteps
    Retrofit provideRetrofit(@Your String baseUrl, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(new ConverterCallAdapterFactory(new JsoupResponseBodyConverterFactory()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(new ResponseBodyConverter())
                .build();
    }

    @Provides
    YourStepsEndpoint provideYourStepsEndpoint(@YourSteps Retrofit retrofit) {
        return retrofit.create(YourStepsEndpoint.class);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes;

import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.functions.Predicate;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import javax.inject.Inject;

/**
 * <p>
 *     Converts the {@link YourStepsBean DMBAlmanac source} into the next show start time in next 12 hours.
 * </p>
 */
public class SometimesFunction implements Function<YourStepsBean, Observable<Long>> {

    @Inject
    private Function<YourStepsBean, Observable<WalkBean>> weWalkFunction;

    @Inject
    private Predicate<WalkBean> somePredicate;

    @Inject
    private Function<WalkBean, Observable<Integer>> weRunAwayFunction;

    @Inject
    private DateTimeFormatter dateTimeFormatter;

    @Inject
    @Sometimes
    private DateTime now;

    @Inject
    private Function3<DateTime, DateTime, Integer, Observable<Long>> timesFunction3;

    @Inject
    @Times
    private Long times;

    @Override
    public Observable<Long> apply(YourStepsBean yourStepsBean) throws Exception {
        Observable<WalkBean> showObservable = Observable.just(yourStepsBean)
                .flatMap(weWalkFunction)
                .share();

        Observable<WalkBean> upcomingShowObservable = showObservable
                .filter(somePredicate)
                .share();

        Observable<Integer> upcomingOffsetObservable = upcomingShowObservable
                .flatMap(weRunAwayFunction)
                .share();

        Observable<DateTime> upcomingDateTimeObservable = upcomingShowObservable
                .map(WalkBean::getDate)
                .map(dateTimeFormatter::parseDateTime)
                .share();

        return Observable.zip(
                upcomingDateTimeObservable,
                Observable.just(now),
                upcomingOffsetObservable,
                timesFunction3
        ).blockingSingle(Observable.just(times));
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon;

import com.foxfarmroad.illbackyouup.components.doasyouplease.LocStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.StyleAttr;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.sub_components.go.Go;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *     Handles each of the song nodes in v1 format and adds songs and notes to {@link As} and {@link You}, respectively.
 * </p>
 */
public class GoOnFunction implements Function<Observable<Node>, Observable<List<String>>> {

    @Inject
    @StyleAttr
    private String attrName;

    @Inject
    @LocStyle
    private String locStyle;

    @Inject
    @Go
    private BiFunction<List<String>, Node, List<String>> goBiFunction;

    @Override
    public Observable<List<String>> apply(Observable<Node> nodeObservable) throws Exception {
        return nodeObservable
                .skipUntil(nodeObservable
                        .filter(n -> n.nodeName().equals("div") && !n.attr(attrName).equals(locStyle)))
                .skip(1)
                .scan(new ArrayList<>(), goBiFunction);
    }

}

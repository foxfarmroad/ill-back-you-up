package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown;

import com.foxfarmroad.illbackyouup.io.Do;
import hu.akarnokd.rxjava2.expr.StatementObservable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import javax.inject.Inject;

/**
 * <p>
 *     Chooses which piece of the location block the given {@link Node} contains and saves it to a {@link Do}.
 * </p>
 */
public class IveKnownFunction implements Function<Node, Observable<Do>> {

    @Inject
    @Knowledge
    private String knowledge;

    @Override
    public Observable<Do> apply(Node node) throws Exception {
        Observable<String> stringObservable = Observable.fromIterable(node.childNodes())
                .filter(childNode -> !(childNode instanceof Comment))
                .filter(childNode -> childNode instanceof TextNode)
                .map(childNode -> StringUtils.strip(((TextNode) childNode).text()))
                .share();

        return StatementObservable.ifThen(
                () -> stringObservable.count().blockingGet() == 3,
                stringObservable
                        .toList()
                        .map(list -> new Do("", list.get(0), list.get(1), list.get(2)))
                        .toObservable(),
                stringObservable
                        .toList()
                        .map(list -> new Do(list.get(0), list.get(1), list.get(2), list.get(3)))
                        .toObservable()
        );
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

/**
 * <p>
 *     Components utilizing this component should bind constants for <pre>baseUrl</pre> variable.
 * </p>
 */
public class RunModule extends AbstractModule {

    @Override
    protected void configure() {
    }

    @Provides
    @Run
    Retrofit provideRetrofit(@Run String baseUrl, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create())
                .build();
    }

    @Provides
    RunEndpoint provideHowFastEndpoint(@Run Retrofit retrofit) {
        return retrofit.create(RunEndpoint.class);
    }

}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import com.google.common.collect.ImmutableList;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import org.apache.commons.configuration2.BaseConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ImmutableConfiguration;
import org.apache.commons.configuration2.MapConfiguration;
import software.amazon.awssdk.core.regions.Region;
import software.amazon.awssdk.services.ssm.SSMClient;
import software.amazon.awssdk.services.ssm.model.DescribeParametersRequest;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;
import software.amazon.awssdk.services.ssm.model.GetParametersRequest;
import software.amazon.awssdk.services.ssm.model.GetParametersResponse;
import software.amazon.awssdk.services.ssm.model.ParametersFilter;

import java.util.Collection;
import java.util.List;

public class HardToRememberModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(SSMClient.class);

        requireBinding(CollectionToGetParametersRequestFunction.class);
        requireBinding(CollectionToParametersFilterFunction.class);
        requireBinding(DescribeParametersRequestToDescribeParametersResultFunction.class);
        requireBinding(DescribeParametersResultToNameListObservableFunction.class);
        requireBinding(FilterDescribeParametersResultFunction.class);
        requireBinding(GetParametersRequestToGetParametersResultFunction.class);
        requireBinding(GetParametersResultToMapConfigurationObservableFunction.class);
        requireBinding(ConfigurationToImmutableConfigurationObservableFunction.class);
        requireBinding(ParametersFilterListToDescribeParametersRequestFunction.class);

        bind(new TypeLiteral<Function<Collection<String>, GetParametersRequest>>() {
        }).to(CollectionToGetParametersRequestFunction.class);
        bind(new TypeLiteral<Function<Collection<String>, ParametersFilter>>() {
        }).to(CollectionToParametersFilterFunction.class);
        bind(new TypeLiteral<Function<DescribeParametersRequest, Observable<DescribeParametersResponse>>>() {
        }).to(DescribeParametersRequestToDescribeParametersResultFunction.class);
        bind(new TypeLiteral<Function<DescribeParametersResponse, Observable<List<String>>>>() {
        }).to(DescribeParametersResultToNameListObservableFunction.class);
        bind(new TypeLiteral<Predicate<DescribeParametersResponse>>() {
        }).to(FilterDescribeParametersResultFunction.class);
        bind(new TypeLiteral<Function<GetParametersRequest, Observable<GetParametersResponse>>>() {
        }).to(GetParametersRequestToGetParametersResultFunction.class);
        bind(new TypeLiteral<Function<GetParametersResponse, Observable<MapConfiguration>>>() {
        }).to(GetParametersResultToMapConfigurationObservableFunction.class);
        bind(new TypeLiteral<Function<Configuration, Observable<ImmutableConfiguration>>>() {
        }).to(ConfigurationToImmutableConfigurationObservableFunction.class);
        bind(new TypeLiteral<Function<Collection<ParametersFilter>, DescribeParametersRequest>>() {
        }).to(ParametersFilterListToDescribeParametersRequestFunction.class);
    }

    @Provides
    SSMClient provideSSMClient() {
        // TODO Use this user/role in CloudFormation for Lambda
        System.setProperty("aws.accessKeyId", "AKIAIITSKU2XHESDLBKQ");
        System.setProperty("aws.secretKey", "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");
        return SSMClient.builder()
                // TODO Use hardtoremember for this
                .region(Region.US_EAST_1)
                .build();
    }

    @Provides
    ImmutableConfiguration provideAwsParameterStoreConfiguration(
            Function<Collection<String>, ParametersFilter> collectionToParametersFilterFunction,
            Function<Collection<ParametersFilter>, DescribeParametersRequest> parametersFilterListToDescribeParametersRequestFunction,
            Function<DescribeParametersRequest, Observable<DescribeParametersResponse>> describeParametersRequestToDescribeParametersResultFunction,
            Predicate<DescribeParametersResponse> filterDescribeParametersResultFunction,
            Function<DescribeParametersResponse, Observable<List<String>>> describeParametersResultToNameListObservableFunction,
            Function<Collection<String>, GetParametersRequest> collectionToGetParametersRequestFunction,
            Function<GetParametersRequest, Observable<GetParametersResponse>> getParametersRequestToGetParametersResultFunction,
            Function<GetParametersResponse, Observable<MapConfiguration>> getParametersResultToMapConfigurationObservableFunction,
            Function<Configuration, Observable<ImmutableConfiguration>> configurationToImmutableConfigurationObservableFunction) {
        // TODO Use local hardtoremember for environment
        return Observable.just(new String[]{"prod."})
                .map(ImmutableList::copyOf)
                .map(collectionToParametersFilterFunction)
                .map(ImmutableList::of)
                .map(parametersFilterListToDescribeParametersRequestFunction)
                .flatMap(describeParametersRequestToDescribeParametersResultFunction)
                .filter(filterDescribeParametersResultFunction)
                .flatMap(describeParametersResultToNameListObservableFunction)
                .map(collectionToGetParametersRequestFunction)
                .flatMap(getParametersRequestToGetParametersResultFunction)
                .flatMap(getParametersResultToMapConfigurationObservableFunction)
                .flatMap(configurationToImmutableConfigurationObservableFunction)
                .blockingSingle(new BaseConfiguration());
    }

//    @Provides
//    Rollbar provideRollbar() {
//        Rollbar rollbar = new Rollbar("7badab0e577f451fa3229f6b066616ed", "test");
//        rollbar.handleUncaughtErrors();
//        return rollbar;
//    }

}

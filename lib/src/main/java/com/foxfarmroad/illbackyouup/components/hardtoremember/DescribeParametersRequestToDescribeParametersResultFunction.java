package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import software.amazon.awssdk.services.ssm.SSMClient;
import software.amazon.awssdk.services.ssm.model.DescribeParametersRequest;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;

import javax.inject.Inject;

public class DescribeParametersRequestToDescribeParametersResultFunction implements
        Function<DescribeParametersRequest, Observable<DescribeParametersResponse>> {

    @Inject
    private SSMClient ssm;

    @Override
    public Observable<DescribeParametersResponse> apply(DescribeParametersRequest describeParametersRequest) {
        return Observable.just(describeParametersRequest)
                .map(ssm::describeParameters);
    }

}

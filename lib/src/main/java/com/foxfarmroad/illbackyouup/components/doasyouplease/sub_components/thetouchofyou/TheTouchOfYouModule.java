package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeFunction;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class TheTouchOfYouModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(WouldYouLikeFunction.class);

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).to(WouldYouLikeFunction.class);
    }

}

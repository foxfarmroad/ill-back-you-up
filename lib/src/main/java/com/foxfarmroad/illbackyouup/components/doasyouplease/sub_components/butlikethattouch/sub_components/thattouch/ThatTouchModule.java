package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch;

import com.foxfarmroad.illbackyouup.components.doasyouplease.LocStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.StyleAttr;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.sub_components.that.ThatPredicate;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.sub_components.touch.TouchBiFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.sub_components.touch.TouchModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Predicate;
import org.jsoup.nodes.Node;

import java.util.List;

public class ThatTouchModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(ThatPredicate.class);
        requireBinding(TouchBiFunction.class);

        install(new TouchModule());

        bindConstant().annotatedWith(StyleAttr.class).to("style");
        bindConstant().annotatedWith(LocStyle.class).to("padding-bottom:12px;padding-left:3px;color:#3995aa;");

        bind(new TypeLiteral<Predicate<Node>>() { }).to(ThatPredicate.class);
        bind(new TypeLiteral<BiFunction<List<String>, Node, List<String>>>() { }).to(TouchBiFunction.class);
    }

}

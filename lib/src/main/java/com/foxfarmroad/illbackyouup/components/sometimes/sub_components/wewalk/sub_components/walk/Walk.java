package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.walk;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@BindingAnnotation
public @interface Walk {
}

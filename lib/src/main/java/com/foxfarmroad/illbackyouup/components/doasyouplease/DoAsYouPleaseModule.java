package com.foxfarmroad.illbackyouup.components.doasyouplease;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow.ButIKnowFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow.ButIKnowModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.ButLikeThatTouchFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.ButLikeThatTouchModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.IllGoOnFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.IllGoOnModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown.IveKnownFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown.IveKnownModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeModule;
import com.foxfarmroad.illbackyouup.io.Do;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import java.util.List;

public class DoAsYouPleaseModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(ButIKnowFunction.class);
        requireBinding(IveKnownFunction.class);
        requireBinding(ButLikeThatTouchFunction.class);
        requireBinding(IllGoOnFunction.class);

        install(new ButIKnowModule());
        install(new IveKnownModule());
        install(new ButLikeThatTouchModule());
        install(new IllGoOnModule());
        install(new WouldYouLikeModule());

        bindConstant().annotatedWith(StyleAttr.class).to("style");
        bindConstant().annotatedWith(LocStyle.class).to("padding-bottom:12px;padding-left:3px;color:#3995aa;");

        bind(new TypeLiteral<Function<Document, Observable<Node>>>() { }).to(ButIKnowFunction.class);
        bind(new TypeLiteral<Function<Node, Observable<Do>>>() { }).to(IveKnownFunction.class);
        bind(new TypeLiteral<Function<YourStepsBean, Observable<List<String>>>>() { })
                .annotatedWith(ButLikeThatTouch.class).to(ButLikeThatTouchFunction.class);
        bind(new TypeLiteral<Function<YourStepsBean, Observable<List<String>>>>() { }).annotatedWith(IllGoOn.class)
                .to(IllGoOnFunction.class);
    }

}

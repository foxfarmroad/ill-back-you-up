package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.position.PositionFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.position.PositionModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.sub_components.intherisk.InTheRiskConsumer;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.sub_components.intherisk.InTheRiskModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Node;

public class WeHaveDancedModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(PositionFunction.class);
        requireBinding(InTheRiskConsumer.class);

        install(new PositionModule());
        install(new InTheRiskModule());

        bind(new TypeLiteral<Function<Node, Observable<Integer>>>() { }).to(PositionFunction.class);
        bind(new TypeLiteral<BiConsumer<StringBuilder, Node>>() { }).to(InTheRiskConsumer.class);
    }

}

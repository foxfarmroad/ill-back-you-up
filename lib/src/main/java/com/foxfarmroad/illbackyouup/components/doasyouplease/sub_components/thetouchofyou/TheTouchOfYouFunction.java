package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

/**
 * <p>
 *     Cleans a {@link String} and prettifies it for a setlist.
 * </p>
 */
public class TheTouchOfYouFunction implements Function<String, Observable<String>> {

    @Inject
    private Function<String, Observable<String>> wouldYouLikeFunction;

    @Override
    public Observable<String> apply(String s) throws Exception {
        return Observable.just(s)
                .flatMap(wouldYouLikeFunction)
                .map(text -> {
                    if (text.matches("^[0-9]{1,2}\\..*")) {
                        return text.split("\\d+[\\.]")[1];
                    } else {
                        if (text.toLowerCase().contains("set break")) {
                            return "Set Break";
                        } else if (text.toLowerCase().contains("encore")) {
                            return "Encore:";
                        } else {
                            return "";
                        }
                    }
                })
                .filter(StringUtils::isNotBlank);
    }

}

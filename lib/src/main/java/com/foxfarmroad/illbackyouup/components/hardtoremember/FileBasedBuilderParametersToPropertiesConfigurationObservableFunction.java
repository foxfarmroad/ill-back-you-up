package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.FileBasedBuilderParameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

public class FileBasedBuilderParametersToPropertiesConfigurationObservableFunction implements Function<FileBasedBuilderParameters, Observable<PropertiesConfiguration>> {

    @Override
    public Observable<PropertiesConfiguration> apply(FileBasedBuilderParameters fileBasedBuilderParameters) {
        try {
            return Observable.just(new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class)
                    .configure(fileBasedBuilderParameters)
                    .getConfiguration());
        } catch (ConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

}

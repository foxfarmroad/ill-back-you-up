package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.Path;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.RunEndpoint;
import com.foxfarmroad.illbackyouup.io.GeometryBean;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.foxfarmroad.illbackyouup.io.RunBean;
import com.foxfarmroad.illbackyouup.io.RunResultsBean;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

/**
 * <p>
 *     Queries for the latitude and longitude of the given {@link String address}.
 * </p>
 */
public class WeRunFunction implements Function<String, Observable<LatLngBean>> {

    @Inject
    @WeRun
    private Function<String, Observable<String>> weFunction;

    @Inject
    @Path
    private String path;

    @Inject
    private RunEndpoint runEndpoint;

    @Override
    public Observable<LatLngBean> apply(String s) {
        Observable<String> countryObservable = Observable.fromArray(s.split(","))
                .takeLast(1)
                .map(StringUtils::strip)
                .share();

        Observable<String> shortCountryObservable = countryObservable
                .filter(country -> country.matches("[A-Z]{3}"))
                .flatMap(weFunction)
                .share();

        return Observable.just(
                        Observable.combineLatest(countryObservable, shortCountryObservable, s::replace)
                                .blockingSingle(s)
                )
                .flatMap(place -> runEndpoint.getGeocodeJson(path, place))
                .flatMapIterable(RunBean::getResults)
                .firstOrError().toObservable()
                .map(RunResultsBean::getGeometry)
                .map(GeometryBean::getLocation);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease;

import com.foxfarmroad.illbackyouup.io.Do;
import com.foxfarmroad.illbackyouup.io.DoAsYouPlease;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.util.List;

/**
 * <p>
 *     Converts the document body in {@link YourStepsBean} to a {@link DoAsYouPlease}, containing setlist info.
 * </p>
 */
public class DoAsYouPleaseFunction implements Function<YourStepsBean, Observable<DoAsYouPlease>> {

    @Inject
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Inject
    @StyleAttr
    private String attrName;

    @Inject
    @LocStyle
    private String locStyle;

    @Inject
    private Function<Node, Observable<Do>> iveKnownFunction;

    @Inject
    @ButLikeThatTouch
    private Function<YourStepsBean, Observable<List<String>>> butLikeThatTouchFunction;

    @Inject
    @IllGoOn
    private Function<YourStepsBean, Observable<List<String>>> illGoOnFunction;

    @Override
    public Observable<DoAsYouPlease> apply(YourStepsBean yourStepsBean) {
        return Observable.zip(
                Observable.just(yourStepsBean)
                        .map(YourStepsBean::getBody)
                        .flatMap(butIKnowFunction)
                        .filter(n -> n.nodeName().equals("div"))
                        .filter(n -> n.attr(attrName).equals(locStyle))
                        .flatMap(iveKnownFunction),
                Observable.just(yourStepsBean)
                        .flatMap(butLikeThatTouchFunction),
                Observable.just(yourStepsBean)
                        .flatMap(illGoOnFunction),
                DoAsYouPlease::new
        );
    }

}

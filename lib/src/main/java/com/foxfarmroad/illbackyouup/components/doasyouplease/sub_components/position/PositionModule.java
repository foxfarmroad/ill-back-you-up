package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.position;

import com.google.inject.AbstractModule;

public class PositionModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(PositionStyle.class).to("Position:Absolute;Top:");
    }

}

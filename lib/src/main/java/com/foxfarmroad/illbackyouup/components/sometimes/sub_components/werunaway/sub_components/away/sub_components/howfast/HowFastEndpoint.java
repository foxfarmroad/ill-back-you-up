package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast;

import com.foxfarmroad.illbackyouup.io.HowFastBean;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * <p>
 *     Fetches timezone information about the given {@link String location} and {@link Long timestamp}.
 * </p>
 */
public interface HowFastEndpoint {

    @GET
    Observable<HowFastBean> getTimezoneJson(@Url String fast, @Query("location") String location,
                                            @Query("timestamp") Long timestamp);

}

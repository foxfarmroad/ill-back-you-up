package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow.ButIKnowFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow.ButIKnowModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.ButLikeFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.ButLikeModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.ThatTouchFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.ThatTouchModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing.ForeverOnlyKnowingFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing.ForeverOnlyKnowingModule;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import java.util.List;

public class ButLikeThatTouchModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(ButIKnowFunction.class);
        requireBinding(ForeverOnlyKnowingFunction.class);
        requireBinding(ButLikeFunction.class);
        requireBinding(ThatTouchFunction.class);

        install(new ButIKnowModule());
        install(new ForeverOnlyKnowingModule());
        install(new ButLikeModule());
        install(new ThatTouchModule());

        bind(new TypeLiteral<Function<Document, Observable<Node>>>() { }).to(ButIKnowFunction.class);
        bind(new TypeLiteral<Function<YourStepsBean, Observable<Boolean>>>() { }).to(ForeverOnlyKnowingFunction.class);
        bind(new TypeLiteral<Function<Node, Observable<List<String>>>>() { }).annotatedWith(ButLike.class)
                .to(ButLikeFunction.class);
        bind(new TypeLiteral<Function<Node, Observable<Node>>>() { }).annotatedWith(ThatTouch.class)
                .to(ThatTouchFunction.class);
    }

}

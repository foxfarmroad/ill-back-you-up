package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.configuration2.builder.fluent.FileBasedBuilderParameters;
import org.apache.commons.configuration2.builder.fluent.Parameters;

public class FileNameToFileBasedBuilderParametersObservableFunction implements Function<String, Observable<FileBasedBuilderParameters>> {

    @Override
    public Observable<FileBasedBuilderParameters> apply(String fileName) {
        return Observable.zip(
                    Observable.just(new Parameters().fileBased()),
                    Observable.just(fileName),
                    FileBasedBuilderParameters::setFileName);
    }

}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.functions.Predicate;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;

public class FilterDescribeParametersResultFunction implements Predicate<DescribeParametersResponse> {

    @Override
    public boolean test(DescribeParametersResponse describeParametersResponse) {
        return !describeParametersResponse.parameters().isEmpty();
    }

}

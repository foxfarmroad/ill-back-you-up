package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.sub_components.go;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.IRememberThinking;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.irememberthinking.IRememberThinkingFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.irememberthinking.IRememberThinkingModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class GoModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(TheTouchOfYouFunction.class);
        requireBinding(IRememberThinkingFunction.class);

        install(new TheTouchOfYouModule());
        install(new IRememberThinkingModule());

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).annotatedWith(TheTouchOfYou.class)
                .to(TheTouchOfYouFunction.class);
        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).annotatedWith(IRememberThinking.class)
                .to(IRememberThinkingFunction.class);
    }

}

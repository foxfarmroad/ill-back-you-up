package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.times;

import com.google.inject.AbstractModule;

public class TimesModule extends AbstractModule {

    @Override
    protected void configure() {
        // TODO Use config
        bindConstant().annotatedWith(TimeOffset.class).to(13);
    }

}

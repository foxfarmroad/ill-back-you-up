package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.sub_components.intherisk;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.IRememberThinking;
import io.reactivex.Observable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import javax.inject.Inject;

/**
 * <p>
 *     Collects all show notes nodes into a {@link StringBuilder} {@link Observable}.
 * </p>
 */
public class InTheRiskConsumer implements BiConsumer<StringBuilder, Node> {

    @Inject
    @IRememberThinking
    private Function<String, Observable<String>> iRememberThinkingFunction;

    @Override
    public void accept(StringBuilder stringBuilder, Node node) throws Exception {
        if (node.nodeName().equals("img")) {
            stringBuilder.append("-> ");
        } else if (node.nodeName().equals("font")) {
            stringBuilder.append(StringUtils.strip(((Element) node).text()) + " ");
        } else if (node.nodeName().equals("#text")) {
            Observable.just(((TextNode) node).text())
                    .flatMap(iRememberThinkingFunction)
                    .subscribe(stringBuilder::append /* TODO Make a subscriber for errors */);
        }
    }

}

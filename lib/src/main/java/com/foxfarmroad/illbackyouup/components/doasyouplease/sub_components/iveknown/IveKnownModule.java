package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.iveknown;

import com.google.inject.AbstractModule;

public class IveKnownModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(Knowledge.class).to("[A-Z]{1}[a-z]{2}[ ]{1}\\d{1,2}[ ]{1}\\d{4}");
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.we;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.NonBreakingSpace;
import com.google.inject.AbstractModule;

public class WeModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(NonBreakingSpace.class).to('\u00A0');
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.sure;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import javax.inject.Inject;

/**
 * <p>
 *     Extracts song text from node without a hierarchy.
 * </p>
 */
public class SureFunction implements Function<Node, Observable<String>> {

    @Inject
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Override
    public Observable<String> apply(Node node) throws Exception {
        return Observable.just(node)
                .filter(n -> n instanceof Element)
                .map(element -> ((Element) element).text())
                .filter(text -> !StringUtils.isBlank(text))
                .map(text -> Observable.just(text)
                        .flatMap(theTouchOfYouFunction)
                        .blockingSingle("DUMMY")
                );
    }

}

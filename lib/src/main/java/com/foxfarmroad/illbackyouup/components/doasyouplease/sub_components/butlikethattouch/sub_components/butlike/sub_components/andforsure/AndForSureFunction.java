package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.SongStyle;
import hu.akarnokd.rxjava2.expr.StatementObservable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import javax.inject.Inject;

/**
 * <p>
 *     Extract and map the song from v1 of setlist HTML.
 * </p>
 */
public class AndForSureFunction implements Function<Node, Observable<Pair<Integer, String>>> {

    @Inject
    @SongStyle
    private String songStyle;

    @Inject
    private Function<Node, Observable<Integer>> positionFunction;

    @Inject
    @And
    private Function<Node, Observable<String>> andFunction;

    @Inject
    @For
    private Function<Node, Observable<String>> forFunction;

    @Override
    public Observable<Pair<Integer, String>> apply(Node node) throws Exception {
        return Observable.zip(
                Observable.just(node)
                        .filter(elementNode -> elementNode instanceof Element)
                        .filter(div -> div.attr("style").contains(songStyle))
                        .flatMap(positionFunction),
                StatementObservable
                        .ifThen(
                                () -> node.childNodeSize() > 1,
                                Observable.just(node)
                                        .flatMap(andFunction),
                                Observable.just(node)
                                        .flatMap(forFunction)
                        ),
                ImmutablePair::of
        );
    }

}

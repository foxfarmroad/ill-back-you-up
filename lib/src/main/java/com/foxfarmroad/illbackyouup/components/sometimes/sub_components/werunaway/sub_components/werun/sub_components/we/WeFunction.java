package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.we;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

import javax.inject.Inject;
import java.util.Map;

/**
 * <p>
 *     Converts three character country code to the correct two character country code, if found.
 * </p>
 */
public class WeFunction implements Function<String, Observable<String>> {

    @Inject
    private Map<String, String> countryCodeMap;

    @Override
    public Observable<String> apply(String s) throws Exception {
        return Observable.just(s)
                .filter(countryCodeMap::containsKey)
                .map(countryCodeMap::get);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.we;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import io.reactivex.Observable;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class WeModule extends AbstractModule {

    @Override
    protected void configure() {
    }

    @Provides
    Map<String, String> provideCountryCodeMap() {
        return Observable.fromArray(Locale.getISOCountries())
                .collectInto(new HashMap<String, String>(), (map, country) -> {
                        Observable.just(country)
                                .map(countryCode -> new Locale("", countryCode))
                                .map(Locale::getISO3Country)
                                .subscribe(v -> map.put(v, country));
                    }
                )
                .blockingGet();
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon;

import com.foxfarmroad.illbackyouup.components.doasyouplease.LocStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.StyleAttr;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.sub_components.go.Go;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.sub_components.go.GoBiFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.sub_components.go.GoModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.functions.BiFunction;
import org.jsoup.nodes.Node;

import java.util.List;

public class GoOnModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(GoBiFunction.class);

        install(new GoModule());

        bindConstant().annotatedWith(StyleAttr.class).to("style");
        bindConstant().annotatedWith(LocStyle.class).to("padding-bottom:12px;padding-left:3px;color:#3995aa;");

        bind(new TypeLiteral<BiFunction<List<String>, Node, List<String>>>() { }).annotatedWith(Go.class)
                .to(GoBiFunction.class);
    }

}

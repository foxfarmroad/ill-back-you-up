package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.GoOn;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.Ill;
import com.foxfarmroad.illbackyouup.io.DoAsYouPlease;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import hu.akarnokd.rxjava2.expr.StatementObservable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.util.List;

/**
 * <p>
 *     Collects the nodes from the new version of setlist DOM into a {@link DoAsYouPlease}.
 * </p>
 */
public class IllGoOnFunction implements Function<YourStepsBean, Observable<List<String>>> {

    @Inject
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Inject
    private Function<YourStepsBean, Observable<Boolean>> foreverOnlyKnowingFunction;

    @Inject
    @Ill
    private Function<Observable<Node>, Observable<List<String>>> illFunction;

    @Inject
    @GoOn
    private Function<Observable<Node>, Observable<List<String>>> goOnFunction;

    @Override
    public Observable<List<String>> apply(YourStepsBean yourStepsBean) throws Exception {
        Observable<Node> nodeObservable = Observable.just(yourStepsBean)
                .map(YourStepsBean::getBody)
                .flatMap(butIKnowFunction)
                .share();

        return StatementObservable.ifThen(
                () -> Observable.just(yourStepsBean)
                        .flatMap(foreverOnlyKnowingFunction)
                        .blockingSingle(Boolean.FALSE),
                Observable.just(nodeObservable)
                        .flatMap(illFunction),
                Observable.just(nodeObservable)
                        .flatMap(goOnFunction)
        );
    }

}

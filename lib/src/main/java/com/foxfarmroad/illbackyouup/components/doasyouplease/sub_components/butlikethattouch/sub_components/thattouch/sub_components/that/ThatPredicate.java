package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.sub_components.that;

import io.reactivex.functions.Predicate;
import org.jsoup.nodes.Node;

/**
 * <p>
 * </p>
 */
public class ThatPredicate implements Predicate<Node> {

    @Override
    public boolean test(Node node) throws Exception {
        return node.nodeName().equals("div");
    }

}

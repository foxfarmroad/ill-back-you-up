package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.functions.Function;
import software.amazon.awssdk.services.ssm.model.ParametersFilter;
import software.amazon.awssdk.services.ssm.model.ParametersFilterKey;

import java.util.Collection;

public class CollectionToParametersFilterFunction implements Function<Collection<String>, ParametersFilter> {

    @Override
    public ParametersFilter apply(Collection<String> stringCollection) {
        return ParametersFilter.builder()
                .key(ParametersFilterKey.NAME)
                .values(stringCollection)
                .build();
    }

}

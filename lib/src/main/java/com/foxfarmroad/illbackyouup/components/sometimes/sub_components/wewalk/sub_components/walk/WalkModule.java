package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.walk;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.we.WeFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.we.WeModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class WalkModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(WeFunction.class);

        install(new WeModule());

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).annotatedWith(Walk.class)
                .to(WeFunction.class);
    }

}

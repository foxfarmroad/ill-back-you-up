package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.position;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Node;

import javax.inject.Inject;

/**
 * <p>
 *     Extracts the position value from the given node's style.
 * </p>
 */
public class PositionFunction implements Function<Node, Observable<Integer>> {

    @Inject
    @PositionStyle
    private String positionStyle;

    @Override
    public Observable<Integer> apply(Node node) throws Exception {
        return Observable.just(node)
                .map(el -> el.attr("style"))
                .map(text -> text.split(positionStyle))
                .filter(textArray -> textArray.length > 1)
                .map(textArray -> textArray[1])
                .map(text -> text.split(";"))
                .filter(textArray -> textArray.length > 0)
                .map(textArray -> textArray[0])
                .map(Integer::parseInt);
    }

}

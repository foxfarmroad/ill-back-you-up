package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.AwayBiFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.Fast;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.How;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastEndpoint;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastModule;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.WeRunFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.WeRunModule;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.joda.time.DateTime;

public class WeRunAwayModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(HowFastEndpoint.class);
        requireBinding(WeRunFunction.class);
        requireBinding(AwayBiFunction.class);

        install(new HowFastModule());
        install(new WeRunModule());

        bindConstant().annotatedWith(How.class).to("https://maps.googleapis.com");
        bindConstant().annotatedWith(Fast.class).to("maps/api/timezone/json");

        bind(new TypeLiteral<Function<String, Observable<LatLngBean>>>() { }).to(WeRunFunction.class);
        bind(new TypeLiteral<BiFunction<LatLngBean, DateTime, Observable<Integer>>>() { }).to(AwayBiFunction.class);
    }

    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        // TODO Config for the level
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk;

import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Element;

import javax.inject.Inject;
import java.util.Optional;

/**
 * <p>
 *     Converts the DMBAlmanac DOM containing the upcoming tour dates to a stream of
 *     {@link com.foxfarmroad.illbackyouup.io.WalkBean}.
 * </p>
 */
public class WeWalkFunction implements Function<YourStepsBean, Observable<WalkBean>> {

    @Inject
    private Function<Element, Observable<WalkBean>> walkFunction;

    @Override
    public Observable<WalkBean> apply(YourStepsBean yourStepsBean) throws Exception {
        return Observable.just(yourStepsBean)
                .map(bean -> Optional.ofNullable(bean.getBody()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .flatMapIterable(document -> document.getElementsByAttributeValue("class", "newslogcell"))
                .singleOrError().toObservable()
                .flatMapIterable(Element::children)
                .filter(child -> child.nodeName().toLowerCase().equals("tbody"))
                .singleOrError().toObservable()
                .flatMapIterable(Element::children)
                .filter(child -> child.nodeName().toLowerCase().equals("tr"))
                .filter(child -> !child.hasAttr("class"))
                .flatMap(walkFunction);
    }

}

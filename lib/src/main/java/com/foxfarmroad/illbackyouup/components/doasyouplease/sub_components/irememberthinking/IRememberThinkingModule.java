package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.irememberthinking;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.WouldYouLikeFunction;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class IRememberThinkingModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(WouldYouLikeFunction.class);

        bindConstant().annotatedWith(ShowNotes.class).to("show notes");

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).to(WouldYouLikeFunction.class);
    }

}

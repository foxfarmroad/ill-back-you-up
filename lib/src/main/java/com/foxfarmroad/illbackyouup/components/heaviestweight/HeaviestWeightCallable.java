package com.foxfarmroad.illbackyouup.components.heaviestweight;

import okhttp3.ResponseBody;
import retrofit2.Response;

import javax.inject.Inject;
import java.util.concurrent.Callable;

/**
 * <p>
 *     Processes fetched source {@link org.jsoup.nodes.Document document}.
 * </p>
 */
public class HeaviestWeightCallable implements Callable<Response<ResponseBody>> {

    @Inject
    @Weight
    private String weight;

    @Inject
    @Username
    private String username;

    @Inject
    @Password
    private String password;

    @Inject
    @FormAction
    private String formAction;

    @Inject
    private HeaviestWeightEndpoint heaviestWeightEndpoint;

    @Override
    public Response<ResponseBody> call() throws Exception {
        return heaviestWeightEndpoint.login(weight, username, password, formAction)
                .blockingSingle();
    }

}

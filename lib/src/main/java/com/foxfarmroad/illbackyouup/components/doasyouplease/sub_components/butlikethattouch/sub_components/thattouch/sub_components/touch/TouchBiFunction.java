package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.sub_components.touch;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import javax.inject.Inject;
import java.util.List;

/**
 * <p>
 * </p>
 */
public class TouchBiFunction implements BiFunction<List<String>, Node, List<String>> {

    @Inject
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Override
    public List<String> apply(List<String> list, Node node) {
        switch (node.nodeName()) {
            case "img":
                if (!list.isEmpty()) {
                    list.set(list.size() - 1, list.get(list.size() - 1) + " ->");
                }
                break;
            case "#text":
                Observable.just(((TextNode) node).text())
                        .flatMap(theTouchOfYouFunction)
                        .blockingSubscribe(list::add/* TODO Error handler */);
                break;
            default:
                break;
        }

        return list;
    }

}

package com.foxfarmroad.illbackyouup.components.yoursteps;

import io.reactivex.Observable;
import org.jsoup.nodes.Document;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * <p>
 *     Used to fetch the source of a resource as a {@link org.jsoup.nodes.Document document}.
 * </p>
 */
public interface YourStepsEndpoint {

    @GET
    Observable<Document> document(@Url String url);

}

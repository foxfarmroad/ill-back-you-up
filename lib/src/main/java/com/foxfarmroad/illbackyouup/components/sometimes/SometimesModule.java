package com.foxfarmroad.illbackyouup.components.sometimes;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.some.SomePredicate;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.times.TimesFunction3;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.times.TimesModule;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.WeRunAwayFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.WeRunAwayModule;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.WeWalkFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.WeWalkModule;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.walk.WalkFunction;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Function3;
import io.reactivex.functions.Predicate;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class SometimesModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(SomePredicate.class);
        requireBinding(TimesFunction3.class);
        requireBinding(WeRunAwayFunction.class);
        requireBinding(WeWalkFunction.class);
        requireBinding(WalkFunction.class);

        install(new TimesModule());
        install(new WeRunAwayModule());
        install(new WeWalkModule());

        // TODO Use config
        bindConstant().annotatedWith(TimeStart.class).to(20);
        bindConstant().annotatedWith(Times.class).to(-1L);

        bind(new TypeLiteral<Predicate<WalkBean>>() { }).to(SomePredicate.class);
        bind(new TypeLiteral<Function3<DateTime, DateTime, Integer, Observable<Long>>>() { }).to(TimesFunction3.class);
        bind(new TypeLiteral<Function<WalkBean, Observable<Integer>>>() { }).to(WeRunAwayFunction.class);
        bind(new TypeLiteral<Function<YourStepsBean, Observable<WalkBean>>>() { }).to(WeWalkFunction.class);
    }

    @Provides
    DateTimeFormatter provideDateTimeFormatter() {
        return DateTimeFormat.forPattern("MM.dd.yy").withZoneUTC();
    }

    @Provides
    @Sometimes
    DateTime provideNow() {
        return new DateTime().withZone(DateTimeZone.UTC);
    }

}

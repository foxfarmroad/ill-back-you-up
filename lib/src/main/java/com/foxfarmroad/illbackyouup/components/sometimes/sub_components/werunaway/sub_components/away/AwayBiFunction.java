package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.Fast;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastEndpoint;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import org.joda.time.DateTime;

import javax.inject.Inject;

/**
 * <p>
 *     Queries for the timezone offset of the combination of the given {@link LatLngBean coordinates} and
 *     {@link DateTime show time}.
 * </p>
 */
public class AwayBiFunction implements BiFunction<LatLngBean, DateTime, Observable<Integer>> {

    @Inject
    @Fast
    private String fast;

    @Inject
    private HowFastEndpoint howFastEndpoint;

    @Override
    public Observable<Integer> apply(LatLngBean latLngBean, DateTime showTime) {
        return Observable.zip(
                        Observable.just(fast),
                        Observable.just(latLngBean)
                                .map(bean -> bean.getLat() + "," + bean.getLng()),
                        Observable.just(showTime)
                                .map(DateTime::getMillis)
                                .map(mills -> mills / 1000),
                        howFastEndpoint::getTimezoneJson
                ).blockingSingle()
                .map(bean -> bean.getDstOffset() + bean.getRawOffset());
    }
}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.configuration2.MapConfiguration;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import software.amazon.awssdk.services.ssm.model.GetParametersResponse;

public class GetParametersResultToMapConfigurationObservableFunction implements
        Function<GetParametersResponse, Observable<MapConfiguration>> {

    @Override
    public Observable<MapConfiguration> apply(GetParametersResponse getParametersResponse) {
        return Observable.just(getParametersResponse)
                .flatMapIterable(GetParametersResponse::parameters)
                .map(parameter -> ImmutablePair.of(parameter.name(), parameter.value()))
                .toMap(Pair::getKey, Pair::getValue)
                .toObservable()
                .map(MapConfiguration::new);
    }

}

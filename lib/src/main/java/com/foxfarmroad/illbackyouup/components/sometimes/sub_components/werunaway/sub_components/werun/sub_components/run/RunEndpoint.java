package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run;

import com.foxfarmroad.illbackyouup.io.RunBean;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * <p>
 *     Fetches geocode information about the given {@link String address}.
 * </p>
 */
public interface RunEndpoint {

    @GET
    Observable<RunBean> getGeocodeJson(@Url String path, @Query("address") String address);

}

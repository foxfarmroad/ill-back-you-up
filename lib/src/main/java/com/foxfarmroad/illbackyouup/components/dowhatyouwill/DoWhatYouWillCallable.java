package com.foxfarmroad.illbackyouup.components.dowhatyouwill;

import com.foxfarmroad.illbackyouup.io.DoAsYouPlease;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import retrofit2.Response;

import javax.inject.Inject;
import java.util.concurrent.Callable;

/**
 * <p>
 * </p>
 */
public class DoWhatYouWillCallable implements Callable<Observable<DoAsYouPlease>> {

    @Inject
    private Callable<Response<ResponseBody>> heaviestWeightCallable;

    @Inject
    private Callable<Observable<YourStepsBean>> yourStepsCallable;

    @Inject
    private Function<YourStepsBean, DoAsYouPlease> doAsYouPleaseFunction;

    @Override
    public Observable<DoAsYouPlease> call() throws Exception {
        return Observable.fromCallable(heaviestWeightCallable)
                .doOnNext(response -> {
                    if (!response.isSuccessful()) {
                        throw new RuntimeException(response.message());
                    }
                })
                .flatMap(response -> Observable.fromCallable(yourStepsCallable).blockingSingle())
                .map(doAsYouPleaseFunction);
    }

}

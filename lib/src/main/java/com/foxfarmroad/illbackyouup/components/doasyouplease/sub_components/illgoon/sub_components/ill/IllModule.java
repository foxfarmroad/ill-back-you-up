package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.OldNoteStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.WeHaveDanced;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.WeHaveDancedFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.WeHaveDancedModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Node;

public class IllModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(WeHaveDancedFunction.class);

        install(new WeHaveDancedModule());

        bindConstant().annotatedWith(OldNoteStyle.class).to("olor:#3f94aa");

        bind(new TypeLiteral<Function<Node, Observable<Pair<Integer, String>>>>() { }).annotatedWith(WeHaveDanced.class)
                .to(WeHaveDancedFunction.class);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.sub_components.go;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.IRememberThinking;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import javax.inject.Inject;
import java.util.List;

/**
 * <p>
 *     Extracts the position value from the given note node's style.
 * </p>
 */
public class GoBiFunction implements BiFunction<List<String>, Node, List<String>> {

    @Inject
    @IRememberThinking
    private Function<String, Observable<String>> iRememberThinkingFunction;

    @Override
    public List<String> apply(List<String> list, Node node) {
        switch (node.nodeName()) {
            case "img":
                list.add("-> ");
                if (!list.isEmpty()) {
                    list.set(list.size() - 1, list.get(list.size() - 1) + " ->");
                }
                break;
            case "font":
                list.add(((Element) node).ownText() + " ");
                break;
            case "#text":
                Observable.just(((TextNode) node).text())
                        .flatMap(iRememberThinkingFunction)
                        .blockingSubscribe(string -> {
                            if (!list.isEmpty()) {
                                list.set(list.size() - 1, list.get(list.size() - 1) + string);
                            }
                        }/* TODO Error handler */);
                break;
            default:
                break;
        }

        return list;
    }

}

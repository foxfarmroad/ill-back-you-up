package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.times;

import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.google.inject.Inject;
import io.reactivex.Observable;
import io.reactivex.functions.Function3;
import org.joda.time.DateTime;

/**
 * <p>
 *     Calculates the epoch milliseconds of the next show time within the {@link Integer offset}, if there is one.
 * </p>
 */
public class TimesFunction3 implements Function3<DateTime, DateTime, Integer, Observable<Long>> {

    @Inject
    @TimeStart
    private Integer showStartHour;

    @Inject
    @TimeOffset
    private Integer offsetHour;

    @Override
    public Observable<Long> apply(DateTime showDate, DateTime now, Integer offset) throws Exception {
        return Observable.just(showDate)
                .map(start -> start.withHourOfDay(showStartHour).minusSeconds(offset))
                .filter(start -> start.isBefore(now.plusHours(offsetHour)))
                .map(DateTime::getMillis);
    }

}

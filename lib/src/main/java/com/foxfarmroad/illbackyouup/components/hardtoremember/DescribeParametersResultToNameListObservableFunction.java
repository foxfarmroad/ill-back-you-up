package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import software.amazon.awssdk.services.ssm.model.DescribeParametersResponse;
import software.amazon.awssdk.services.ssm.model.ParameterMetadata;

import java.util.List;

public class DescribeParametersResultToNameListObservableFunction implements
        Function<DescribeParametersResponse, Observable<List<String>>> {

    @Override
    public Observable<List<String>> apply(DescribeParametersResponse describeParametersResponse) {
        return Observable.fromIterable(describeParametersResponse.parameters())
                .map(ParameterMetadata::name)
                .toList()
                .toObservable();
    }

}

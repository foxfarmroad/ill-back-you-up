package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.irememberthinking;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

import javax.inject.Inject;

/**
 * <p>
 *     Prettifies show note strings.
 * </p>
 */
public class IRememberThinkingFunction implements Function<String, Observable<String>> {

    @Inject
    @ShowNotes
    private String showNotes;

    @Inject
    private Function<String, Observable<String>> wouldYouLikeFunction;

    @Override
    public Observable<String> apply(String s) throws Exception {
        return Observable.just(s)
                .filter(text -> !text.toLowerCase().contains(showNotes))
                .flatMap(wouldYouLikeFunction);
    }

}

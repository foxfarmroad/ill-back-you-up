package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow.ButIKnowFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow.ButIKnowModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing.ForeverOnlyKnowingFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing.ForeverOnlyKnowingModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.GoOn;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.GoOnFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.goon.GoOnModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.Ill;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.IllFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.IllModule;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import java.util.List;

public class IllGoOnModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(ButIKnowFunction.class);
        requireBinding(ForeverOnlyKnowingFunction.class);
        requireBinding(IllFunction.class);
        requireBinding(GoOnFunction.class);

        install(new ButIKnowModule());
        install(new ForeverOnlyKnowingModule());
        install(new IllModule());
        install(new GoOnModule());

        bind(new TypeLiteral<Function<Document, Observable<Node>>>() { }).to(ButIKnowFunction.class);
        bind(new TypeLiteral<Function<YourStepsBean, Observable<Boolean>>>() { }).to(ForeverOnlyKnowingFunction.class);
        bind(new TypeLiteral<Function<Observable<Node>, Observable<List<String>>>>() { }).annotatedWith(Ill.class)
                .to(IllFunction.class);
        bind(new TypeLiteral<Function<Observable<Node>, Observable<List<String>>>>() { }).annotatedWith(GoOn.class)
                .to(GoOnFunction.class);
    }

}

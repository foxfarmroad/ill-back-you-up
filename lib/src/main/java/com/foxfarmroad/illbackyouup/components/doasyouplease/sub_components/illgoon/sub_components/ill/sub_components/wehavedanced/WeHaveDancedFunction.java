package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced;

import io.reactivex.Observable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Node;

import javax.inject.Inject;

/**
 * <p>
 *     Extracts the position value from the given note node's style.
 * </p>
 */
public class WeHaveDancedFunction implements Function<Node, Observable<Pair<Integer, String>>> {

    @Inject
    private Function<Node, Observable<Integer>> positionFunction;

    @Inject
    private BiConsumer<StringBuilder, Node> inTheRiskConsumer;

    @Override
    public Observable<Pair<Integer, String>> apply(Node node) throws Exception {
        return Observable.zip(
                Observable.just(node)
                        .flatMap(positionFunction),
                Observable.just(node)
                        .flatMapIterable(Node::childNodes)
                        .collectInto(new StringBuilder(), inTheRiskConsumer)
                        .toObservable()
                        .map(StringBuilder::toString),
                ImmutablePair::of
        );
    }

}

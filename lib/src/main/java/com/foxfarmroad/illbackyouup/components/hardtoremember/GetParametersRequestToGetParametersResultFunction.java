package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import software.amazon.awssdk.services.ssm.SSMClient;
import software.amazon.awssdk.services.ssm.model.GetParametersRequest;
import software.amazon.awssdk.services.ssm.model.GetParametersResponse;

import javax.inject.Inject;

public class GetParametersRequestToGetParametersResultFunction implements
        Function<GetParametersRequest, Observable<GetParametersResponse>> {

    @Inject
    private SSMClient ssm;

    @Override
    public Observable<GetParametersResponse> apply(GetParametersRequest getParametersRequest) {
        return Observable.just(getParametersRequest)
                .map(ssm::getParameters);
    }

}

package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.functions.Function;
import software.amazon.awssdk.services.ssm.model.GetParametersRequest;

import java.util.Collection;

public class CollectionToGetParametersRequestFunction implements Function<Collection<String>, GetParametersRequest> {

    @Override
    public GetParametersRequest apply(Collection<String> stringCollection) {
        return GetParametersRequest.builder()
                .withDecryption(true)
                .names(stringCollection)
                .build();
    }

}

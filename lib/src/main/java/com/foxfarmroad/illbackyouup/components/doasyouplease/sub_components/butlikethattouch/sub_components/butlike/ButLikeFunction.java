package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.AndForSure;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * <p>
 *     Creates a list of songs for v1 setlist.
 * </p>
 */
public class ButLikeFunction implements Function<Node, Observable<List<String>>> {

    @Inject
    @AndForSure
    private Function<Node, Observable<Pair<Integer, String>>> andForSureFunction;

    @Override
    public Observable<List<String>> apply(Node node) throws Exception {
        return Observable.just(node)
                .filter(n -> n.nodeName().equals("div"))
                .flatMapIterable(Node::childNodes)
                .filter(innerNode -> innerNode.nodeName().equals("div"))
                .filter(div -> div.hasAttr("style"))
                .flatMap(andForSureFunction)
                .filter(pair -> !pair.getRight().equals("DUMMY"))
                .collectInto(new TreeMap<Integer, String>(), (treeMap, pair) -> treeMap.put(pair.getLeft(), pair.getRight()))
                .toObservable()
                .map(Map::values)
                .filter(values -> !values.isEmpty())
                .map(ArrayList::new);
    }

}

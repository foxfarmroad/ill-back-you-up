package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike;

import com.google.inject.AbstractModule;

public class WouldYouLikeModule extends AbstractModule {

    @Override
    protected void configure() {
        bindConstant().annotatedWith(NonBreakingSpace.class).to('\u00A0');
        bindConstant().annotatedWith(ReplacementCharacter.class).to('�');
        bindConstant().annotatedWith(Apostrophe.class).to('\'');
        bindConstant().annotatedWith(BadTranslation.class).to("ï¿½");
    }

}

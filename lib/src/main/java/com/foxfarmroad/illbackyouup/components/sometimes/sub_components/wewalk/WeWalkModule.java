package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.walk.WalkFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.walk.WalkModule;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Element;

public class WeWalkModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(WalkFunction.class);

        install(new WalkModule());

        bind(new TypeLiteral<Function<Element, Observable<WalkBean>>>() {}).to(WalkFunction.class);
    }

}

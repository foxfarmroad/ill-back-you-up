package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun;

import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.Path;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.Run;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.run.RunModule;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.we.WeFunction;
import com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.werun.sub_components.we.WeModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class WeRunModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(WeFunction.class);

        install(new RunModule());
        install(new WeModule());

        bindConstant().annotatedWith(Run.class).to("http://maps.googleapis.com");
        bindConstant().annotatedWith(Path.class).to("maps/api/geocode/json");

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).annotatedWith(WeRun.class)
                .to(WeFunction.class);
    }

}

package com.foxfarmroad.illbackyouup.components.heaviestweight;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * <p>
 *     Components utilizing this component should bind constants for <pre>baseUrl</pre> and <pre>path</pre> variables.
 * </p>
 */
public class HeaviestWeightModule extends AbstractModule {

    @Override
    protected void configure() {
    }

    @Provides
    @Heaviest
    Retrofit provideRetrofit(@Heaviest String baseUrl, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    @Provides
    HeaviestWeightEndpoint provideHeaviestWeightEndpoint(@Heaviest Retrofit retrofit) {
        return retrofit.create(HeaviestWeightEndpoint.class);
    }

}

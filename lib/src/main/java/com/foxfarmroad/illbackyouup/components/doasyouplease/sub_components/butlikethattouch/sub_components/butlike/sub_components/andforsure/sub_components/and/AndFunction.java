package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.and;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

import javax.inject.Inject;

/**
 * <p>
 *     Extracts song text from node with a hierarchy.
 * </p>
 */
public class AndFunction implements Function<Node, Observable<String>> {

    @Inject
    @TheTouchOfYou
    private Function<String, Observable<String>> theTouchOfYouFunction;

    @Override
    public Observable<String> apply(Node node) throws Exception {
        return Observable.fromIterable(node.childNodes())
                .filter(childNode -> childNode instanceof TextNode)
                .map(textNode -> ((TextNode) textNode).text())
                .filter(text -> !StringUtils.isBlank(text))
                .flatMap(theTouchOfYouFunction)
                .map(text -> text + " ->");
    }

}

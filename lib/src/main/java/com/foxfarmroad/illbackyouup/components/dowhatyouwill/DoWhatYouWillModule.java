package com.foxfarmroad.illbackyouup.components.dowhatyouwill;

import com.foxfarmroad.illbackyouup.components.doasyouplease.DoAsYouPleaseFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.DoAsYouPleaseModule;
import com.foxfarmroad.illbackyouup.components.heaviestweight.HeaviestWeightCallable;
import com.foxfarmroad.illbackyouup.components.heaviestweight.HeaviestWeightModule;
import com.foxfarmroad.illbackyouup.components.yoursteps.YourStepsCallable;
import com.foxfarmroad.illbackyouup.components.yoursteps.YourStepsModule;
import com.foxfarmroad.illbackyouup.io.DoAsYouPlease;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import com.foxfarmroad.illbackyouup.retrofit.CookieStore;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import okhttp3.CookieJar;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;

import java.util.concurrent.Callable;

public class DoWhatYouWillModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(HeaviestWeightCallable.class);
        requireBinding(YourStepsCallable.class);
        requireBinding(DoAsYouPleaseFunction.class);

        install(new HeaviestWeightModule());
        install(new YourStepsModule());
        install(new DoAsYouPleaseModule());

        bind(new TypeLiteral<Callable<Response<ResponseBody>>>() { }).to(HeaviestWeightCallable.class);
        bind(new TypeLiteral<Callable<Observable<YourStepsBean>>>() { }).to(YourStepsCallable.class);
        bind(new TypeLiteral<Function<YourStepsBean, Observable<DoAsYouPlease>>>() { }).to(DoAsYouPleaseFunction.class);

        bind(CookieJar.class).toInstance(new CookieStore());
    }

    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        // TODO Config for the level
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, CookieJar cookieJar) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .cookieJar(cookieJar)
                .build();
    }

}

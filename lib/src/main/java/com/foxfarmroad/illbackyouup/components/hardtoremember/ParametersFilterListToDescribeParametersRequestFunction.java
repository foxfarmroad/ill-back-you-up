package com.foxfarmroad.illbackyouup.components.hardtoremember;

import io.reactivex.functions.Function;
import software.amazon.awssdk.services.ssm.model.DescribeParametersRequest;
import software.amazon.awssdk.services.ssm.model.ParametersFilter;

import java.util.Collection;

public class ParametersFilterListToDescribeParametersRequestFunction implements Function<Collection<ParametersFilter>, DescribeParametersRequest> {

    @Override
    public DescribeParametersRequest apply(Collection<ParametersFilter> parametersFilterCollection) {
        return DescribeParametersRequest.builder()
                .filters(parametersFilterCollection)
                .build();
    }

}

package com.foxfarmroad.illbackyouup.components.yoursteps;

import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import io.reactivex.Observable;

import javax.inject.Inject;
import java.util.concurrent.Callable;

/**
 * <p>
 *     Processes fetched source {@link org.jsoup.nodes.Document document}.
 * </p>
 */
public class YourStepsCallable implements Callable<Observable<YourStepsBean>> {

    @Inject
    @Steps
    private String steps;

    @Inject
    private YourStepsEndpoint yourStepsEndpoint;

    @Override
    public Observable<YourStepsBean> call() throws Exception {
        return yourStepsEndpoint.document(steps)
                .map(YourStepsBean::new);
    }

}

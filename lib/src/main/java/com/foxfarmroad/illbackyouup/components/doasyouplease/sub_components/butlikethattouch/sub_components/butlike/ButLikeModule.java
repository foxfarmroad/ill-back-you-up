package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.AndForSure;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.AndForSureFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.AndForSureModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Node;

public class ButLikeModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(AndForSureFunction.class);

        install(new AndForSureModule());

        bind(new TypeLiteral<Function<Node, Observable<Pair<Integer, String>>>>() { }).annotatedWith(AndForSure.class)
                .to(AndForSureFunction.class);
    }

}

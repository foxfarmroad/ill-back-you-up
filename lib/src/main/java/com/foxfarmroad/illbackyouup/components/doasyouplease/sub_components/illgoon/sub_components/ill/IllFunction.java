package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.OldNoteStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.WeHaveDanced;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.tuple.Pair;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * <p>
 *     Handles each of the song nodes in v1 format and adds songs and notes to {@link As} and {@link You}, respectively.
 * </p>
 */
public class IllFunction implements Function<Observable<Node>, Observable<List<String>>> {

    @Inject
    @OldNoteStyle
    private String noteStyle;

    @Inject
    @WeHaveDanced
    private Function<Node, Observable<Pair<Integer, String>>> weHaveDancedFunction;

    @Override
    public Observable<List<String>> apply(Observable<Node> nodeObservable) throws Exception {
        return nodeObservable
                .filter(n -> n.nodeName().equals("div"))
                .flatMapIterable(Node::childNodes)
                .filter(innerNode -> innerNode.nodeName().equals("div"))
                .filter(div -> div.hasAttr("style"))
                .filter(div -> div.attr("style").contains(noteStyle))
                .flatMap(weHaveDancedFunction)
                .collectInto(new TreeMap<Integer, String>(), (treeMap, pair) -> treeMap.put(pair.getLeft(), pair.getRight()))
                .toObservable()
                .map(Map::values)
                .map(ArrayList::new);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.walk;

import com.foxfarmroad.illbackyouup.io.WalkBean;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import javax.inject.Inject;

/**
 * <p>
 *     Converts a {@link Element} to a {@link WalkBean}.
 * </p>
 */
public class WalkFunction implements Function<Element, Observable<WalkBean>> {

    @Inject
    @Walk
    private Function<String, Observable<String>> weFunction;

    @Override
    public Observable<WalkBean> apply(Element element) {
        Observable<Node> cellObservable = Observable.just(element)
                .filter(e -> e.childNodeSize() == 10)
                .flatMapIterable(Node::childNodes)
                .filter(e -> e.nodeName().equals("td"))
                .share();

        Observable<Node> venueShowDetailObservable = cellObservable
                .filter(e -> e.attr("align").toLowerCase().equals("left"))
                .take(1)
                .filter(e -> e.childNodeSize() == 1)
                .flatMapIterable(Node::childNodes)
                .take(1)
                .filter(e -> e.nodeName().toLowerCase().equals("table"))
                .take(1)
                .filter(e -> e.childNodeSize() == 1)
                .flatMapIterable(Node::childNodes)
                .take(1)
                .filter(e -> e.nodeName().toLowerCase().equals("tbody"))
                .take(1)
                .filter(e -> e.childNodeSize() == 1)
                .flatMapIterable(Node::childNodes)
                .take(1)
                .filter(e -> e.nodeName().toLowerCase().equals("tr"))
                .take(1)
                .filter(e -> e.childNodeSize() == 2)
                .flatMapIterable(Node::childNodes)
                .filter(e -> e.nodeName().toLowerCase().equals("td"))
                .share();

        return Observable.merge(
                cellObservable
                        .filter(e -> e.attr("align").toLowerCase().equals("center"))
                        .take(1)
                        .map(dateCell -> ((Element) dateCell).text())
                        .flatMap(weFunction),
                venueShowDetailObservable
                        .filter(e -> e.childNodeSize() == 1)
                        .flatMapIterable(Node::childNodes)
                        .filter(e -> e.nodeName().toLowerCase().equals("a"))
                        .map(venue -> ((Element) venue).text())
                        .flatMap(weFunction),
                venueShowDetailObservable
                        .filter(e -> e.hasAttr("align"))
                        .map(showDetails -> ((Element) showDetails).text())
                        .flatMap(weFunction),
                cellObservable
                        .filter(e -> e.attr("align").toLowerCase().equals("right"))
                        .take(1)
                        .map(locationCell -> ((Element) locationCell).text())
                        .flatMap(weFunction)
                )
                .toList().toObservable()
                .filter(list -> list.size() == 4)
                .map(list -> new WalkBean(list.get(0), list.get(1), list.get(2), list.get(3)));
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch.sub_components.touch;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class TouchModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(TheTouchOfYouFunction.class);

        install(new TheTouchOfYouModule());

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).annotatedWith(TheTouchOfYou.class)
                .to(TheTouchOfYouFunction.class);
    }

}

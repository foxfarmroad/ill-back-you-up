package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.thattouch;

import com.foxfarmroad.illbackyouup.components.doasyouplease.LocStyle;
import com.foxfarmroad.illbackyouup.components.doasyouplease.StyleAttr;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import org.jsoup.nodes.Node;

import javax.inject.Inject;

/**
 * <p>
 *     Creates a list of songs for v2 setlist.
 * </p>
 */
public class ThatTouchFunction implements Function<Node, Observable<Node>> {

    @Inject
    @StyleAttr
    private String attrName;

    @Inject
    @LocStyle
    private String locStyle;

    @Inject
    private Predicate<Node> thatPredicate;

    @Override
    public Observable<Node> apply(Node node) throws Exception {
        return Observable.just(node)
                .filter(n -> !n.attr(attrName).equals(locStyle))
                .takeUntil(thatPredicate);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch;

import com.foxfarmroad.illbackyouup.io.DoAsYouPlease;
import com.foxfarmroad.illbackyouup.io.YourStepsBean;
import hu.akarnokd.rxjava2.expr.StatementObservable;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *     Processes the given {@link Node} and adds it to the given {@link DoAsYouPlease}.
 * </p>
 */
public class ButLikeThatTouchFunction implements Function<YourStepsBean, Observable<List<String>>> {

    @Inject
    private Function<Document, Observable<Node>> butIKnowFunction;

    @Inject
    private Function<YourStepsBean, Observable<Boolean>> foreverOnlyKnowingFunction;

    @Inject
    @ButLike
    private Function<Node, Observable<List<String>>> butLikeFunction;

    @Inject
    @ThatTouch
    private Function<Node, Observable<Node>> thatTouchFunction;

    @Inject
    private BiFunction<List<String>, Node, List<String>> touchBiFunction;

    @Override
    public Observable<List<String>> apply(YourStepsBean yourStepsBean) throws Exception {
        Observable<Node> nodeObservable = Observable.just(yourStepsBean)
                .map(YourStepsBean::getBody)
                .flatMap(butIKnowFunction)
                .share();

        return StatementObservable.ifThen(
                () -> Observable.just(yourStepsBean)
                        .flatMap(foreverOnlyKnowingFunction)
                        .blockingSingle(Boolean.FALSE),
                nodeObservable
                        .flatMap(butLikeFunction),
                nodeObservable
                        .flatMap(thatTouchFunction)
                        .scan(new ArrayList<>(), touchBiFunction)
                        .takeLast(1)
        );
    }
}

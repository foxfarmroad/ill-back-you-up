package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.sub_components.ill.sub_components.wehavedanced.sub_components.intherisk;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.IRememberThinking;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.irememberthinking.IRememberThinkingFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.irememberthinking.IRememberThinkingModule;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class InTheRiskModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(IRememberThinkingFunction.class);

        install(new IRememberThinkingModule());

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).annotatedWith(IRememberThinking.class)
                .to(IRememberThinkingFunction.class);
    }

}

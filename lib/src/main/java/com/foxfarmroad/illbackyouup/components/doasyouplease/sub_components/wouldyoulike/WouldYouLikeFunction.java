package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike;


import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.parser.Parser;

import javax.inject.Inject;

/**
 * <p>
 *     Prettifies strings.
 * </p>
 */
public class WouldYouLikeFunction implements Function<String, Observable<String>> {

    @Inject
    @NonBreakingSpace
    private char nonBreakingSpace;

    @Inject
    @ReplacementCharacter
    private char replacementCharacter;

    @Inject
    @Apostrophe
    private char apostrophe;

    @Inject
    @BadTranslation
    private String badTranslation;

    @Override
    public Observable<String> apply(String s) throws Exception {
        return Observable.just(s)
                .map(text -> Parser.unescapeEntities(text, false))
                .map(text -> StringUtils.remove(text, nonBreakingSpace))
                .map(text -> StringUtils.replaceChars(text, replacementCharacter, apostrophe))
                .map(text -> StringUtils.replaceChars(text, badTranslation, "'"))
                .map(StringUtils::strip)
                .filter(StringUtils::isNotBlank);
    }

}

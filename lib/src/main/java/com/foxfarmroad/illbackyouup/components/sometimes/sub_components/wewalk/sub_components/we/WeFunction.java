package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.wewalk.sub_components.we;


import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.wouldyoulike.NonBreakingSpace;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.parser.Parser;

import javax.inject.Inject;

/**
 * <p>
 *     Prettifies strings.
 * </p>
 */
public class WeFunction implements Function<String, Observable<String>> {

    @Inject
    @NonBreakingSpace
    private char nonBreakingSpace;

    @Override
    public Observable<String> apply(String s) throws Exception {
        return Observable.just(s)
                .map(text -> Parser.unescapeEntities(text, false))
                .map(text -> StringUtils.remove(text, nonBreakingSpace))
                .map(StringUtils::strip);
    }

}

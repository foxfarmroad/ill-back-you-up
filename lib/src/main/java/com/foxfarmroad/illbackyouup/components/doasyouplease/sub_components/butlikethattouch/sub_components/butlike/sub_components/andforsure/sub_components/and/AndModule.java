package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butlikethattouch.sub_components.butlike.sub_components.andforsure.sub_components.and;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.illgoon.TheTouchOfYou;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.thetouchofyou.TheTouchOfYouFunction;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class AndModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(TheTouchOfYouFunction.class);

        bind(new TypeLiteral<Function<String, Observable<String>>>() { }).annotatedWith(TheTouchOfYou.class)
                .to(TheTouchOfYouFunction.class);
    }

}

package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing.sub_components.forever;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import org.jsoup.nodes.Node;

import java.util.List;

/**
 * <p>
 *     Reduces the list of nodes to a {@link Boolean}; true if 2 divs are present and the seed value otherwise.
 * </p>
 */
public class ForeverBiFunction implements BiFunction<Boolean, List<Node>, Boolean> {

    @Override
    public Boolean apply(Boolean seed, List<Node> nodeList) {
        return Observable.fromIterable(nodeList)
                .filter(node -> node.nodeName().equals("div"))
                .count().toObservable()
                .map(count -> count == 2)
                .blockingSingle(seed);
    }

}

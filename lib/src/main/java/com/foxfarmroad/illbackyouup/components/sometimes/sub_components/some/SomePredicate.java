package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.some;

import com.foxfarmroad.illbackyouup.components.sometimes.Sometimes;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import io.reactivex.functions.Predicate;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import javax.inject.Inject;

/**
 * <p>
 *     Filters out any {@link com.foxfarmroad.illbackyouup.io.WalkBean} not within a day of current date.
 * </p>
 */
public class SomePredicate implements Predicate<WalkBean> {

    @Inject
    @Sometimes
    private DateTime now;

    @Inject
    private DateTimeFormatter dateTimeFormatter;

    @Override
    public boolean test(WalkBean walkBean) throws Exception {
        DateTime then = dateTimeFormatter.parseDateTime(walkBean.getDate());
        return then.isAfter(now.minusDays(1)) && then.isBefore(now.plusDays(1));
    }

}

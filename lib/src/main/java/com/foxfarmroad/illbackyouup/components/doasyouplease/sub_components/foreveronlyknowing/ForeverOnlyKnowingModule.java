package com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing;

import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow.ButIKnowFunction;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.butiknow.ButIKnowModule;
import com.foxfarmroad.illbackyouup.components.doasyouplease.sub_components.foreveronlyknowing.sub_components.forever.ForeverBiFunction;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;

import java.util.List;

public class ForeverOnlyKnowingModule extends AbstractModule {

    @Override
    protected void configure() {
        requireBinding(ButIKnowFunction.class);
        requireBinding(ForeverBiFunction.class);

        install(new ButIKnowModule());

        bindConstant().annotatedWith(NewNoteStyle.class).to("font-size:16px;font-weight:bold;color:#5fb4ca;margin-top:25px;padding-bottom:1px;");

        bind(new TypeLiteral<Function<Document, Observable<Node>>>() { }).to(ButIKnowFunction.class);
        bind(new TypeLiteral<BiFunction<Boolean, List<Node>, Boolean>>() { }).to(ForeverBiFunction.class);
    }

}

package com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway;

import com.foxfarmroad.illbackyouup.components.sometimes.TimeStart;
import com.foxfarmroad.illbackyouup.io.LatLngBean;
import com.foxfarmroad.illbackyouup.io.WalkBean;
import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import javax.inject.Inject;

/**
 * <p>
 *     Determines the timezone offset based on the location in a {@link WalkBean DMBAlmanac row}.
 * </p>
 */
public class WeRunAwayFunction implements Function<WalkBean, Observable<Integer>> {

    @Inject
    private Function<String, Observable<LatLngBean>> weRunFunction;

    @Inject
    private DateTimeFormatter dateTimeFormatter;

    @Inject
    @TimeStart
    private Integer timeStart;

    @Inject
    private BiFunction<LatLngBean, DateTime, Observable<Integer>> awayBiFunction;

    @Override
    public Observable<Integer> apply(WalkBean walkBean) throws Exception {
        return Observable.zip(
                Observable.just(walkBean)
                        .map(WalkBean::getLocation)
                        .flatMap(weRunFunction),
                Observable.just(walkBean)
                        .map(WalkBean::getDate)
                        .map(dateTimeFormatter::parseDateTime)
                        .map(dateTime -> dateTime.withHourOfDay(timeStart)),
                awayBiFunction
        ).blockingSingle();
    }

}

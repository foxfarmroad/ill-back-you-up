package com.foxfarmroad.illbackyouup.dynamo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.foxfarmroad.illbackyouup.dynamo.components.journal.Journal;
import com.foxfarmroad.illbackyouup.dynamo.components.journal.JournalGenerateEntryFunction;
import com.foxfarmroad.illbackyouup.dynamo.components.journal.JournalItemFunction;
import com.foxfarmroad.illbackyouup.dynamo.components.journal.JournalPutFunction;
import com.foxfarmroad.illbackyouup.dynamo.components.snapshot.Snapshot;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import io.reactivex.functions.Function;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class DynamoModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(new TypeLiteral<Function<Pair<String, Long>, Quartet<String, String, Long, String>>>() { }).to(JournalGenerateEntryFunction.class);
        bind(new TypeLiteral<Function<Quartet<String, String, Long, String>, Item>>() { }).to(JournalItemFunction.class);
        bind(new TypeLiteral<Function<Item, PutItemOutcome>>() { }).to(JournalPutFunction.class);
    }

    @Provides
    @Singleton
    AmazonDynamoDB provideDynamoClient() {
        return AmazonDynamoDBClientBuilder
                .standard()
                .build();
    }

    @Provides
    @Singleton
    DynamoDB provideDynamo(AmazonDynamoDB client) {
        return new DynamoDB(client);
    }

    @Provides
    @Singleton
    @Journal
    Table provideJournalTable(DynamoDB dynamo) {
        return dynamo.getTable(System.getenv().get("JOURNAL_TABLE"));
    }

    @Provides
    @Singleton
    @Snapshot
    Table provideSnapshotTable(DynamoDB dynamo) {
        return dynamo.getTable(System.getenv().get("SNAPSHOT_TABLE"));
    }

    @Provides
    @Singleton
    @DynamoDateTime
    DateTimeFormatter provideDateTimeFormatter() {
        return ISODateTimeFormat.dateHourMinuteSecondMillis().withZoneUTC();
    }
}

package com.foxfarmroad.illbackyouup.dynamo.components.journal;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import io.reactivex.functions.Function;

import javax.inject.Inject;

public class JournalPutFunction implements Function<Item, PutItemOutcome> {
    @Inject
    @Journal
    private Table journalTable;

    @Override
    public PutItemOutcome apply(Item item) throws Exception {
        return journalTable.putItem(item);
    }
}

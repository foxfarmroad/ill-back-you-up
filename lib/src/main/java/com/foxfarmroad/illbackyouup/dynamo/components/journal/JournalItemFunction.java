package com.foxfarmroad.illbackyouup.dynamo.components.journal;

import com.amazonaws.services.dynamodbv2.document.Item;
import io.reactivex.functions.Function;
import org.javatuples.Quartet;

public class JournalItemFunction implements Function<Quartet<String, String, Long, String>, Item> {
    @Override
    public Item apply(Quartet<String, String, Long, String> quartet) throws Exception {
        return new Item()
                .withPrimaryKey("correlationId", quartet.getValue0(), "uuid", quartet.getValue1())
                .withLong("timestamp", quartet.getValue2())
                .withString("createdAt", quartet.getValue3());
    }
}

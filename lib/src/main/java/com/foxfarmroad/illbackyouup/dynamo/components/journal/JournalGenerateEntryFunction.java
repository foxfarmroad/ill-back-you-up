package com.foxfarmroad.illbackyouup.dynamo.components.journal;

import com.foxfarmroad.illbackyouup.dynamo.DynamoDateTime;
import io.reactivex.functions.Function;
import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import javax.inject.Inject;
import java.util.UUID;

public class JournalGenerateEntryFunction implements Function<Pair<String, Long>, Quartet<String, String, Long, String>> {
    @Inject
    @DynamoDateTime
    private DateTimeFormatter dateTimeFormatter;

    @Override
    public Quartet<String, String, Long, String> apply(Pair<String, Long> journalPair) throws Exception {
        return Quartet.with(
                journalPair.getValue0(),
                UUID.randomUUID().toString(),
                journalPair.getValue1(),
                dateTimeFormatter.print(new DateTime()));
    }
}

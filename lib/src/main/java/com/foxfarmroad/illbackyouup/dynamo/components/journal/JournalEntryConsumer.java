package com.foxfarmroad.illbackyouup.dynamo.components.journal;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import org.javatuples.Pair;
import org.javatuples.Quartet;

import javax.inject.Inject;

public class JournalEntryConsumer implements Consumer<Pair<String, Long>> {
    @Inject
    private Function<Pair<String, Long>, Quartet<String, String, Long, String>> journalGenerateEntryFunction;
    @Inject
    private Function<Quartet<String, String, Long, String>, Item> journalItemFunction;
    @Inject
    private Function<Item, PutItemOutcome> journalPutFunction;

    @Override
    public void accept(Pair<String, Long> journalPair) throws Exception {
        Observable.just(journalPair)
                .map(journalGenerateEntryFunction)
                .map(journalItemFunction)
                .map(journalPutFunction)
                .blockingSingle();
    }
}

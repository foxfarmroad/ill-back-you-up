package com.foxfarmroad.illbackyouup.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.List;

/**
 * <p>
 * </p>
 */
public class AddressComponentBean {

    private String long_name;

    private String short_name;

    private List<String> types;

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(long_name, short_name, types);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final AddressComponentBean other = (AddressComponentBean) obj;
        return Objects.equal(this.long_name, other.long_name)
                && Objects.equal(this.short_name, other.short_name)
                && Objects.equal(this.types, other.types);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("long_name", long_name)
                .add("short_name", short_name)
                .add("types", types)
                .toString();
    }

}

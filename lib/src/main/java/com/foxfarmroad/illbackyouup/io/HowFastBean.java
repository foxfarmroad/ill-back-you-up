package com.foxfarmroad.illbackyouup.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class HowFastBean {

    private Integer dstOffset;

    private Integer rawOffset;

    private String status;

    private String timeZoneId;

    private String timeZoneName;

    public Integer getDstOffset() {
        return dstOffset;
    }

    public void setDstOffset(Integer dstOffset) {
        this.dstOffset = dstOffset;
    }

    public Integer getRawOffset() {
        return rawOffset;
    }

    public void setRawOffset(Integer rawOffset) {
        this.rawOffset = rawOffset;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeZoneId() {
        return timeZoneId;
    }

    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public void setTimeZoneName(String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(dstOffset, rawOffset, status, timeZoneId, timeZoneName);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final HowFastBean other = (HowFastBean) obj;
        return Objects.equal(this.dstOffset, other.dstOffset)
                && Objects.equal(this.rawOffset, other.rawOffset)
                && Objects.equal(this.status, other.status)
                && Objects.equal(this.timeZoneId, other.timeZoneId)
                && Objects.equal(this.timeZoneName, other.timeZoneName);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("dstOffset", dstOffset)
                .add("rawOffset", rawOffset)
                .add("status", status)
                .add("timeZoneId", timeZoneId)
                .add("timeZoneName", timeZoneName)
                .toString();
    }

}

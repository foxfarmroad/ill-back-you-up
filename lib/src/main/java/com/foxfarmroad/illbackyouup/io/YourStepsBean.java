package com.foxfarmroad.illbackyouup.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import io.reactivex.annotations.NonNull;
import org.jsoup.nodes.Document;

/**
 * <p>
 *     Contains the parsed {@link org.jsoup.nodes.Document document}.
 * </p>
 */
public class YourStepsBean {

    @NonNull
    private Document body;

    public YourStepsBean(Document body) {
        this.body = body;
    }

    public Document getBody() {
        return body;
    }

    public void setBody(Document body) {
        this.body = body;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(body);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final YourStepsBean other = (YourStepsBean) obj;
        return Objects.equal(this.body, other.body);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("body", body)
                .toString();
    }

}

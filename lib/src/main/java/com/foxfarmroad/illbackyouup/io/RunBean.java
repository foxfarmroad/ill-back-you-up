package com.foxfarmroad.illbackyouup.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.List;

/**
 * <p>
 *     Represents results from
 *     {@link com.foxfarmroad.illbackyouup.components.sometimes.sub_components.werunaway.sub_components.away.sub_components.howfast.HowFastEndpoint}.
 * </p>
 */
public class RunBean {

    private List<RunResultsBean> results;

    private String status;

    public List<RunResultsBean> getResults() {
        return results;
    }

    public void setResults(List<RunResultsBean> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(results, status);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final RunBean other = (RunBean) obj;
        return Objects.equal(this.results, other.results)
                && Objects.equal(this.status, other.status);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("results", results)
                .add("status", status)
                .toString();
    }
}

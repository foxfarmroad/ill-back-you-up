package com.foxfarmroad.illbackyouup.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.joda.time.DateTime;

/**
 * <p>
 * </p>
 */
public class AwayBean {

    private DateTime showTime;

    public DateTime getShowTime() {
        return showTime;
    }

    public void setShowTime(DateTime showTime) {
        this.showTime = showTime;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(showTime);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final AwayBean other = (AwayBean) obj;
        return Objects.equal(this.showTime, other.showTime);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("showTime", showTime)
                .toString();
    }
}

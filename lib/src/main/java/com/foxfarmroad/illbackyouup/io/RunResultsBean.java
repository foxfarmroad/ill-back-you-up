package com.foxfarmroad.illbackyouup.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.util.List;

/**
 * <p>
 * </p>
 */
public class RunResultsBean {

    private List<AddressComponentBean> address_components;

    private String formatted_address;

    private GeometryBean geometry;

    private String place_id;

    private List<String> types;

    public List<AddressComponentBean> getAddress_components() {
        return address_components;
    }

    public void setAddress_components(List<AddressComponentBean> address_components) {
        this.address_components = address_components;
    }

    public String getFormatted_address() {
        return formatted_address;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public GeometryBean getGeometry() {
        return geometry;
    }

    public void setGeometry(GeometryBean geometry) {
        this.geometry = geometry;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(address_components, formatted_address, geometry, place_id, types);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final RunResultsBean other = (RunResultsBean) obj;
        return Objects.equal(this.address_components, other.address_components)
                && Objects.equal(this.formatted_address, other.formatted_address)
                && Objects.equal(this.geometry, other.geometry)
                && Objects.equal(this.place_id, other.place_id)
                && Objects.equal(this.types, other.types);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("address_components", address_components)
                .add("formatted_address", formatted_address)
                .add("geometry", geometry)
                .add("place_id", place_id)
                .add("types", types)
                .toString();
    }

}

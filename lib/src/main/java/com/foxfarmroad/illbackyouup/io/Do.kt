package com.foxfarmroad.illbackyouup.io

/**
 * <p>
 *     The location information of a setlist.
 * </p>
 */
data class Do (
    val performer: String,
    val date: String,
    val venue: String,
    val location: String
)
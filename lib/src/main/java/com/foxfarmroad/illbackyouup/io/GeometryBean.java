package com.foxfarmroad.illbackyouup.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * <p>
 * </p>
 */
public class GeometryBean {

    private BoundsBean bounds;

    private LatLngBean location;

    private String location_type;

    private ViewportBean viewport;

    public BoundsBean getBounds() {
        return bounds;
    }

    public void setBounds(BoundsBean bounds) {
        this.bounds = bounds;
    }

    public LatLngBean getLocation() {
        return location;
    }

    public void setLocation(LatLngBean location) {
        this.location = location;
    }

    public String getLocation_type() {
        return location_type;
    }

    public void setLocation_type(String location_type) {
        this.location_type = location_type;
    }

    public ViewportBean getViewport() {
        return viewport;
    }

    public void setViewport(ViewportBean viewport) {
        this.viewport = viewport;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(bounds, location, location_type, viewport);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final GeometryBean other = (GeometryBean) obj;
        return Objects.equal(this.bounds, other.bounds)
                && Objects.equal(this.location, other.location)
                && Objects.equal(this.location_type, other.location_type)
                && Objects.equal(this.viewport, other.viewport);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("bounds", bounds)
                .add("location", location)
                .add("location_type", location_type)
                .add("viewport", viewport)
                .toString();
    }

}

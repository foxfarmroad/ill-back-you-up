package com.foxfarmroad.illbackyouup.io

/**
 * <p>
 *     The three parts of a live setlist:
 * </p>
 * <p>
 *     {@link Do}, the location information
 * </p>
 * <p>
 *     {@link As}, the song information
 * </p>
 * <p>
 *     {@link You}, the notes information
 * </p>
 */
data class DoAsYouPlease (
    val meta: Do,
    val song: List<String>,
    val note: List<String>
)
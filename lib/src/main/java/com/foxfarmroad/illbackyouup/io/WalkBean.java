package com.foxfarmroad.illbackyouup.io;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * <p>
 *     Contains a single DMBAlmanac listings for a show in a tour year.
 * </p>
 */
public class WalkBean {

    private String date;

    private String venue;

    private String showDetail;

    private String location;

    public WalkBean(String date, String venue, String showDetail, String location) {
        this.date = date;
        this.venue = venue;
        this.showDetail = showDetail;
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getShowDetail() {
        return showDetail;
    }

    public void setShowDetail(String showDetail) {
        this.showDetail = showDetail;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(date, venue, showDetail, location);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final WalkBean other = (WalkBean) obj;
        return Objects.equal(this.date, other.date)
                && Objects.equal(this.venue, other.venue)
                && Objects.equal(this.showDetail, other.showDetail)
                && Objects.equal(this.location, other.location);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("date", date)
                .add("venue", venue)
                .add("showDetail", showDetail)
                .add("location", location)
                .toString();
    }

}

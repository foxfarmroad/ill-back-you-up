divStyle = node.attr("style");
// Location node
if (divStyle.equals(LOC_STYLE)) {
    locList = parseLocationList(node);
}
// If the song nodes are divs
else {
    // All song divs
    Elements divs = ((Element) node)
            .getElementsByTag("div");
    for (Element div : divs) {
        if (div.hasAttr("style")) {
            divStyle = div.attr("style");
            if (divStyle.contains("Top:")) {
                divTemp = divStyle.substring(
                        divStyle.indexOf("Top:"));
                divStyleLocation = Integer.parseInt(
                        divTemp.substring(4,
                                divTemp.indexOf(";")));
            }
            if (divStyle.startsWith(SONG_STYLE)) {
                String[] locations = divStyle.split(
                        SONG_STYLE);
                currentLoc = Integer.parseInt(
                        locations[1].split(";")[0]);
                divText = div.ownText();
                divText = StringUtils.remove(divText,
                        endChar);
                String[] songs = divText.split(
                        "\\d+[\\.]");
                if (songs.length > 1) {
                    currSong = StringUtils.replaceChars(
                            songs[1], badChar, apos);
                    currSong = StringUtils.replaceChars(
                            currSong, badTranslation, "'");
                    Elements imgs =
                            div.getElementsByTag("img");
                    if (!imgs.isEmpty()) {
                        currSong = currSong.concat(" ->");
                        hasSegue = true;
                    }
                    songMap.put(currentLoc, currSong);
                }
                else if (divText.toLowerCase().contains(
                        "encore")) {
                    songMap.put(currentLoc, "Encore:");
                }
                else if (divText.toLowerCase().contains(
                        "set break")) {
                    songMap.put(currentLoc, "Set Break");
                }
            }
            else {
                boolean segue = false;
                divText = div.ownText();
                if (!StringUtils.isBlank(divText)) {
                    for (Node child : div.childNodes()) {
                        oldNote = noteMap.get(
                                divStyleLocation);
                        if (oldNote == null)
                            oldNote = "";
                        if (child instanceof TextNode) {
                            String nodeText = StringUtils
                                    .remove(((TextNode)child)
                                            .text(), endChar);
                            if (!StringUtils.isBlank(
                                    nodeText)) {
                                if (segue) {
                                    logger.info(
                                            "segue: " +
                                                    divStyleLocation);
                                    if (divStyleLocation > -1)
                                        noteMap.put(
                                                divStyleLocation,
                                                oldNote.concat(
                                                        StringUtils.strip(
                                                                nodeText)));
                                    noteList.set(
                                            noteList.size()-1,
                                            noteList.get(
                                                    noteList.size()-1)
                                                    .concat(
                                                            StringUtils.strip(nodeText)));
                                }
                                else {
                                    String noteText =
                                            StringUtils.strip(
                                                    nodeText);
                                    if (noteText
                                            .toLowerCase()
                                            .contains(
                                                    "show notes")) {
                                        logger.info(
                                                "show notes: " +
                                                        divStyleLocation);
                                        if (divStyleLocation > -1)
                                            noteMap.put(
                                                    divStyleLocation,
                                                    oldNote.concat("Notes:"));
                                        logger.info(
                                                "Notes:");
                                        noteList.add(0,
                                                "Notes:");
                                        breaks = 0;
                                    }
                                    else {
                                        if (hasGuest) {
                                            logger.info(
                                                    "hasGuest: " +
                                                            divStyleLocation);
                                            if (divStyleLocation > -1)
                                                noteMap.put(
                                                        divStyleLocation,
                                                        oldNote.concat(
                                                                StringUtils.strip(nodeText)));
                                            noteList.set(
                                                    noteList.size()-1,
                                                    noteList.get(
                                                            noteList.size()-1).concat(
                                                            StringUtils.strip(nodeText)));
                                        }
                                        else if (firstPartial ||
                                                lastPartial) {
                                            logger.info(
                                                    "partial: " +
                                                            divStyleLocation);
                                            if (divStyleLocation > -1)
                                                noteMap.put(
                                                        divStyleLocation,
                                                        oldNote.concat(
                                                                StringUtils.strip(nodeText)));
                                            noteList.set(
                                                    noteList.size()-1,
                                                    noteList.get(
                                                            noteList.size()-1).concat(
                                                            StringUtils.strip(nodeText)));
                                        }
                                        else {
                                            logger.info(
                                                    "other: " +
                                                            divStyleLocation);
                                            if (divStyleLocation > -1)
                                                noteMap.put(
                                                        divStyleLocation,
                                                        oldNote.concat(
                                                                StringUtils.strip(nodeText)));
                                            noteList.add(
                                                    StringUtils.strip(
                                                            nodeText));
                                            logger.info(
                                                    StringUtils.strip(
                                                            nodeText));
                                        }
                                    }
                                }
                                segue = false;
                                hasGuest = false;
                            }
                        }
                        else if (child.nodeName().equals("img")) {
                            logger.info("img: " +
                                    divStyleLocation);
                            if (divStyleLocation > -1)
                                noteMap.put(divStyleLocation,
                                        oldNote.concat("\n")
                                                .concat("-> "));
                            noteList.add("-> ");
                            logger.info("-> ");
                            segue = true;
                        }
                        else if (child.nodeName().equals(
                                "font")) {
                            List<Node> children =
                                    child.childNodes();
                            if (!children.isEmpty()) {
                                Node leaf = children.get(0);
                                if (leaf instanceof TextNode) {
                                    fontText = ((TextNode) leaf)
                                            .text();
                                    if (fontText.contains("(")) {
                                        firstPartial = true;
                                        logger.info(
                                                "partial: " +
                                                        divStyleLocation);
                                        if (divStyleLocation > -1)
                                            noteMap.put(
                                                    divStyleLocation,
                                                    oldNote.concat("\n")
                                                            .concat(
                                                                    StringUtils.strip(fontText)));
                                        noteList.add(
                                                fontText);
                                        logger.info(fontText);
                                    } else if (fontText.contains(")")) {
                                        lastPartial = true;
                                        logger.info(
                                                "partial: " +
                                                        divStyleLocation);
                                        if (divStyleLocation > -1)
                                            noteMap.put(
                                                    divStyleLocation,
                                                    oldNote.concat(
                                                            StringUtils.strip(fontText)
                                                                    .concat(" ")));
                                        noteList.set(
                                                noteList.size()-1,
                                                noteList.get(
                                                        noteList.size()-1).concat(
                                                        StringUtils.strip(fontText)
                                                                .concat(" ")));
                                    } else {
                                        hasGuest = true;
                                        logger.info(
                                                "guest: " +
                                                        divStyleLocation);
                                        if (divStyleLocation > -1)
                                            noteMap.put(
                                                    divStyleLocation,
                                                    oldNote.concat(
                                                            StringUtils.strip(fontText)
                                                                    .concat(" ")));
                                        noteList.add(
                                                fontText.concat(" "));
                                        logger.info(
                                                fontText.concat(" "));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
I remember thinking 
I'll go on forever only knowing 
I'll see you again 
But I know 
The touch of you is hard to remember 
But like that touch I've known no other 

And for sure we have danced in the risk of each other 
Would you like to dance around the world with me 

I'll be falling all about my own thing 
And I know you're the heaviest weight, 
When you're not here that's hung around my head 

And your lips burn wild 
Thrown from the face of a child 
And in your eyes 
The seeing of the greatest few 
Do what you will, always 
walk where you like, your steps 
Do as you please, I'll back you up 

I remember thinking 
Sometimes we walk 
Sometimes we run away 
But I know 
No matter how fast we are running 
Some how we keep 
Somehow we keep up with each other 

I'll be falling all about my own thing 
And I know you're the heaviest weight 
When you're not here that's hung around my head 

And your lips burn wild 
Thrown from the face of a child 
And in your eyes 
The seeing of the greatest few 
Do what you will, always 
Walk where you like, your steps 
Do as you please, I'll back you up



String nonDivBlock = "&nbsp;1.&nbsp;&nbsp;Little Thing&nbsp;\n" +
                "  <br>\n" +
                "  &nbsp;2.&nbsp;&nbsp;Tripping Billies&nbsp;\n" +
                "  <br>\n" +
                "  &nbsp;3.&nbsp;&nbsp;Snow Outside&nbsp;\n" +
                "  <br>\n" +
                "  &nbsp;4.&nbsp;&nbsp;What Would You Say&nbsp;\n" +
                "  <br>\n" +
                "  &nbsp;5.&nbsp;&nbsp;Samurai Cop&nbsp;\n" +
                "  <br>\n" +
                "  &nbsp;6.&nbsp;&nbsp;One Sweet World&nbsp;\n" +
                "  <br>\n" +
                "  &nbsp;7.&nbsp;&nbsp;Worried Man Blues&nbsp;\n" +
                "  <br>\n" +
                "  &nbsp;8.&nbsp;&nbsp;Grey Street&nbsp;\n" +
                "  <br>\n" +
                "  &nbsp;9.&nbsp;&nbsp;Black And Blue Bird&nbsp;\n" +
                "  <br>\n" +
                "  10.&nbsp;&nbsp;Two Step&nbsp;\n" +
                "  <br>\n" +
                "  11.&nbsp;&nbsp;Melissa&nbsp;\n" +
                "  <br>\n" +
                "  12.&nbsp;&nbsp;Don’t Drink the Water&nbsp;\n" +
                "  <br>\n" +
                "  13.&nbsp;&nbsp;Lie In Our Graves&nbsp;\n" +
                "  <br>\n" +
                "  14.&nbsp;&nbsp;Grab The Horns By The Bull&nbsp;&#42;\n" +
                "  <br>\n" +
                "  15.&nbsp;&nbsp;Death On The High Seas&nbsp;\n" +
                "  <br>\n" +
                "  16.&nbsp;&nbsp;Warehouse&nbsp;\n" +
                "  <br>\n" +
                "  17.&nbsp;&nbsp;Stay Or Leave&nbsp;\n" +
                "  <br>\n" +
                "  18.&nbsp;&nbsp;So Damn Lucky&nbsp;\n" +
                "  <br>\n" +
                "  19.&nbsp;&nbsp;Crash Into Me&nbsp;\n" +
                "  <br>\n" +
                "  20.&nbsp;&nbsp;Pantala Naga Pampa&nbsp;\n" +
                "  <img src=\"images/setlists/NewArrow.png\" hspace=\"4\">\n" +
                "  <br>\n" +
                "  21.&nbsp;&nbsp;Rapunzel&nbsp;\n" +
                "  <br>\n" +
                "  22.&nbsp;&nbsp;If The Ocean Had Its Way&nbsp;&#42;\n" +
                "  <br>\n" +
                "  23.&nbsp;&nbsp;Grace Is Gone&nbsp;\n" +
                "  <br>\n" +
                "  24.&nbsp;&nbsp;Satellite&nbsp;\n" +
                "  <br>\n" +
                "  25.&nbsp;&nbsp;Jimi Thing&nbsp;\n" +
                "  <br>\n" +
                "  26.&nbsp;&nbsp;Crush&nbsp;\n" +
                "  <br>\n" +
                "  <br>\n" +
                "  -------- ENCORE --------\n" +
                "  <br>\n" +
                "  &nbsp;\n" +
                "  <br>\n" +
                "  27.&nbsp;&nbsp;#41&nbsp;\n" +
                "  <br>\n" +
                "  28.&nbsp;&nbsp;Dancing Nancies&nbsp;\n" +
                "  <br>\n" +
                "  <br>\n" +
                "  -------- ENCORE --------\n" +
                "  <br>\n" +
                "  &nbsp;\n" +
                "  <br>\n" +
                "  29.&nbsp;&nbsp;Lie In Our Graves (reprise)&nbsp;\n" +
                "  <br>\n" +
                "  30.&nbsp;&nbsp;The Space Between&nbsp;\n" +
                "  <br>\n" +
                "  <div style=\"font-size:16px;font-weight:bold;color:#5fb4ca;margin-top:25px;padding-bottom:1px;\">\n" +
                "     SHOW NOTES:\n" +
                "  </div>\n" +
                "  <br>\n" +
                "  <font style=\"font-size:19px;\">&#42;</font>&nbsp;Tim Solo<br>\n" +
                "  <br>\n" +
                "  &nbsp;\n" +
                "  <img src=\"images/setlists/NewArrow.png\" width=\"16\" height=\"9\">\n" +
                "  &nbsp;indicates a segue into next song\n" +
                "  </p>\n" +
                "  </p>\n" +
                "  <br />\n" +
                "  &nbsp;\n" +
                "  <br />";
                
@Provides
    @Named("V2Node")
    List<Node> provideButLikeThatTouchV2Node() {
        return Jsoup.parseBodyFragment("<div style=\"padding-bottom:12px;padding-left:3px;color:#3995aa;\">Dave Matthews and Tim Reynolds<br>May  6 2017 <br>Ascend Amphitheater<br>Nashville, TN</div>\n" +
                "\n" +
                "\t     \n" +
                "\t\t\t&nbsp;1.&nbsp;&nbsp;Take Me To Tomorrow&nbsp;<br>&nbsp;2.&nbsp;&nbsp;Bartender&nbsp;<br>&nbsp;3.&nbsp;&nbsp;When The World Ends&nbsp;<br>&nbsp;4.&nbsp;&nbsp;Satellite&nbsp;<br>&nbsp;5.&nbsp;&nbsp;So Damn Lucky&nbsp;<br>&nbsp;6.&nbsp;&nbsp;Grace Is Gone&nbsp;<br>&nbsp;7.&nbsp;&nbsp;Save Me&nbsp;<br>&nbsp;8.&nbsp;&nbsp;Black And Blue Bird&nbsp;<br>&nbsp;9.&nbsp;&nbsp;Warehouse&nbsp;<br>10.&nbsp;&nbsp;Old Dirt Hill (Bring That Beat Back)&nbsp;<br>11.&nbsp;&nbsp;Grab The Horns By The Bull&nbsp;&#42;<br>12.&nbsp;&nbsp;Virginia In The Rain&nbsp;<br>13.&nbsp;&nbsp;#41&nbsp;&#43;<br>14.&nbsp;&nbsp;Funny The Way It Is&nbsp;<br>15.&nbsp;&nbsp;Lie In Our Graves&nbsp;<br>16.&nbsp;&nbsp;Stay Or Leave&nbsp;<br>17.&nbsp;&nbsp;Gravedigger&nbsp;<br>18.&nbsp;&nbsp;Manfood&nbsp;&#42;<br>19.&nbsp;&nbsp;Death On The High Seas&nbsp;<br>20.&nbsp;&nbsp;Jimi Thing&nbsp;<br>21.&nbsp;&nbsp;The Space Between&nbsp;<br>22.&nbsp;&nbsp;Grey Street&nbsp;<br>23.&nbsp;&nbsp;Two Step&nbsp;<br><br>-------- ENCORE -------- <br>&nbsp;<br>24.&nbsp;&nbsp;Corn Bread&nbsp;<br>25.&nbsp;&nbsp;Dancing Nancies&nbsp;<br>\n" +
                "\t\t\t\t\t\t\t\t\t\t\n" +
                "\t\t\t\t\t\t<div style=\"font-size:16px;font-weight:bold;color:#5fb4ca;margin-top:25px;padding-bottom:1px;\">SHOW NOTES:</div><br>\n" +
                "\t\t\t\t\t\t<font style=\"font-size:19px;\">&#42;</font>&nbsp;Tim Solo<br><font style=\"font-size:19px;\">&#43;</font>&nbsp;Ben Golder-Novick<br>\n" +
                "\t\t<br>\n" +
                "\t\t</p><br />&nbsp;<br />")
                .body()
                .childNodes();
    }
    
    
@Provides
    @Named("LocationNode")
    Node provideForeverOnlyKnowingLocationNode() {
        return Jsoup.parseBodyFragment(
                "<div style=\"padding-bottom:12px;padding-left:3px;color:#3995aa;\">\n" +
                        "   Jun 28 2014 \n" +
                        "   <br />First Niagara Pavilion\n" +
                        "   <br />Burgettstown, PA\n" +
                        "  </div>")
                .body()
                .childNode(0);
    }

    @Provides
    @Named("SetlistNode")
    Node provideForeverOnlyKnowingSetlistNode() {
        return Jsoup.parseBodyFragment(
                "<div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:470;\">10.&amp;nbsp;&amp;nbsp;Warehouse&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:426;\">-------- SET BREAK -------- &amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:674;\">22.&amp;nbsp;&amp;nbsp;Grey Street&amp;nbsp;&amp;Auml;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:358;\">&amp;nbsp;5.&amp;nbsp;&amp;nbsp;Christmas Song&amp;nbsp;~</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:623;\">19.&amp;nbsp;&amp;nbsp;Typical Situation&amp;nbsp;&amp;Auml;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:691;\">-------- ENCORE -------- &amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:640;\">20.&amp;nbsp;&amp;nbsp;Drunken Soldier&amp;nbsp;&amp;Auml;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#3f94aa;Position:Absolute;Top:806;\">\n" +
                        "         <font style=\"font-size:19px;\">*</font>\n" +
                        "         &amp;nbsp;Dave Solo\n" +
                        "      </div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#3f94aa;Position:Absolute;Top:840;\">\n" +
                        "         <font style=\"font-size:19px;\">~</font>\n" +
                        "         &amp;nbsp;Carter, Dave and Tim\n" +
                        "      </div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:392;\">&amp;nbsp;7.&amp;nbsp;&amp;nbsp;Lie In Our Graves&amp;nbsp;</div>\n" +
                        "      <div style=\"Position:Absolute;Top:762;font-size:16px;font-weight:bold;color:#3f94aa;margin-top:0px;padding-bottom:1px;\">\n" +
                        "         SHOW NOTES:\n" +
                        "         <br />\n" +
                        "         &amp;nbsp;\n" +
                        "         <img src=\"images/setlists/NewArrow.png\" width=\"16\" height=\"9\" />\n" +
                        "         &amp;nbsp;indicates a segue into next song\n" +
                        "      </div>\n" +
                        "      <br />\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:290;\">&amp;nbsp;1.&amp;nbsp;&amp;nbsp;Beach Ball&amp;nbsp;*</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:555;\">15.&amp;nbsp;&amp;nbsp;Jimi Thing&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:735;\">24.&amp;nbsp;&amp;nbsp;Ants Marching&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:589;\">17.&amp;nbsp;&amp;nbsp;Digging a Ditch&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:504;\">12.&amp;nbsp;&amp;nbsp;Crush&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:487;\">11.&amp;nbsp;&amp;nbsp;Belly Belly Nice&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:538;\">14.&amp;nbsp;&amp;nbsp;If Only&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:375;\">&amp;nbsp;6.&amp;nbsp;&amp;nbsp;Sweet&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:307;\">&amp;nbsp;2.&amp;nbsp;&amp;nbsp;Minarets&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:718;\">23.&amp;nbsp;&amp;nbsp;Granny&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:453;\">&amp;nbsp;9.&amp;nbsp;&amp;nbsp;Save Me&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:521;\">13.&amp;nbsp;&amp;nbsp;Seven&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#3f94aa;Position:Absolute;Top:823;\">\n" +
                        "         <font style=\"font-size:19px;\">+</font>\n" +
                        "         &amp;nbsp;Carter and Dave\n" +
                        "      </div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:606;\">\n" +
                        "         18.&amp;nbsp;&amp;nbsp;Donï¿½t Drink the Water&amp;nbsp;&amp;Auml;\n" +
                        "         <img src=\"images/setlists/NewArrow.png\" hspace=\"4\" />\n" +
                        "      </div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:409;\">&amp;nbsp;8.&amp;nbsp;&amp;nbsp;Two Step&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:657;\">21.&amp;nbsp;&amp;nbsp;Corn Bread&amp;nbsp;&amp;Auml;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#3f94aa;Position:Absolute;Top:857;\">\n" +
                        "         <font style=\"font-size:19px;\">&amp;Auml;</font>\n" +
                        "         &amp;nbsp;Bela Fleck\n" +
                        "      </div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:341;\">&amp;nbsp;4.&amp;nbsp;&amp;nbsp;Little Red Bird&amp;nbsp;+</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:5863;font-size:1px;\">Spoon</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:572;\">\n" +
                        "         16.&amp;nbsp;&amp;nbsp;The Riff&amp;nbsp;\n" +
                        "         <img src=\"images/setlists/NewArrow.png\" hspace=\"4\" />\n" +
                        "      </div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:324;\">&amp;nbsp;3.&amp;nbsp;&amp;nbsp;What Would You Say&amp;nbsp;</div>\n" +
                        "      <div style=\"Color:#000000;Position:Absolute;Top:6014;font-size:1px;\">So Much To Say</div>\n" +
                        "   </div>")
                .body()
                .childNode(0);
    }

    @Provides
    @Named("LocationAndSetlistNodes")
    Observable<Node> provideLocationAndSetlistNodes(@Named("LocationNode") Node locationNode,
                                                    @Named("SetlistNode") Node setlistNode) {
        return Observable.just(locationNode, setlistNode);
    }

    @Provides
    @Named("V2Node")
    Node provideButLikeThatTouchV2Node() {
        return Jsoup.parseBodyFragment("<div style=\"padding-bottom:12px;padding-left:3px;color:#3995aa;\">Dave Matthews and Tim Reynolds<br>May  6 2017 <br>Ascend Amphitheater<br>Nashville, TN</div>\n" +
                "\n" +
                "\t     \n" +
                "\t\t\t&nbsp;1.&nbsp;&nbsp;Take Me To Tomorrow&nbsp;<br>&nbsp;2.&nbsp;&nbsp;Bartender&nbsp;<br>&nbsp;3.&nbsp;&nbsp;When The World Ends&nbsp;<br>&nbsp;4.&nbsp;&nbsp;Satellite&nbsp;<br>&nbsp;5.&nbsp;&nbsp;So Damn Lucky&nbsp;<br>&nbsp;6.&nbsp;&nbsp;Grace Is Gone&nbsp;<br>&nbsp;7.&nbsp;&nbsp;Save Me&nbsp;<br>&nbsp;8.&nbsp;&nbsp;Black And Blue Bird&nbsp;<br>&nbsp;9.&nbsp;&nbsp;Warehouse&nbsp;<br>10.&nbsp;&nbsp;Old Dirt Hill (Bring That Beat Back)&nbsp;<br>11.&nbsp;&nbsp;Grab The Horns By The Bull&nbsp;&#42;<br>12.&nbsp;&nbsp;Virginia In The Rain&nbsp;<br>13.&nbsp;&nbsp;#41&nbsp;&#43;<br>14.&nbsp;&nbsp;Funny The Way It Is&nbsp;<br>15.&nbsp;&nbsp;Lie In Our Graves&nbsp;<br>16.&nbsp;&nbsp;Stay Or Leave&nbsp;<br>17.&nbsp;&nbsp;Gravedigger&nbsp;<br>18.&nbsp;&nbsp;Manfood&nbsp;&#42;<br>19.&nbsp;&nbsp;Death On The High Seas&nbsp;<br>20.&nbsp;&nbsp;Jimi Thing&nbsp;<br>21.&nbsp;&nbsp;The Space Between&nbsp;<br>22.&nbsp;&nbsp;Grey Street&nbsp;<br>23.&nbsp;&nbsp;Two Step&nbsp;<br><br>-------- ENCORE -------- <br>&nbsp;<br>24.&nbsp;&nbsp;Corn Bread&nbsp;<br>25.&nbsp;&nbsp;Dancing Nancies&nbsp;<br>\n" +
                "\t\t\t\t\t\t\t\t\t\t\n" +
                "\t\t\t\t\t\t<div style=\"font-size:16px;font-weight:bold;color:#5fb4ca;margin-top:25px;padding-bottom:1px;\">SHOW NOTES:</div><br>\n" +
                "\t\t\t\t\t\t<font style=\"font-size:19px;\">&#42;</font>&nbsp;Tim Solo<br><font style=\"font-size:19px;\">&#43;</font>&nbsp;Ben Golder-Novick<br>\n" +
                "\t\t<br>\n" +
                "\t\t</p><br />&nbsp;<br />")
                .body()
                .childNode(0);
    }